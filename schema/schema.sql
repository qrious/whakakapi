-- Language `plpythonu` is needed for the hashing and random-byte generation.
-- Note: This needs to be done as DB admin.
CREATE LANGUAGE IF NOT EXISTS plpythonu;

-- Create schema for tokeniser.
CREATE SCHEMA IF NOT EXISTS tokeniser;

-- Create imsi-token mapping table.
CREATE TABLE IF NOT EXISTS tokeniser.imsi_tokens (
    idx         serial PRIMARY KEY,
    imsi        bytea NOT NULL,
    token       bytea NOT NULL,
    hashcode    bytea,
    ts          integer,

    UNIQUE(imsi),
    UNIQUE(token)
);

-- Create index on imsi column.
CREATE INDEX ON tokeniser.imsi_tokens (imsi);

-- Add a dummy index 0 value to cater for sequencing starting with 1.
INSERT INTO tokeniser.imsi_tokens (idx, imsi, token, hashcode, ts)
    VALUES (0,
            E'\\x0000000000000000',
            E'\\x0000000000000000',
            E'\\xde47c9b27eb8d300dbb5f2c353e632c393262cf06340c4fa7f1b40c4cbd36f90',
            0);

-- Add the token creation function.
-- Note: This needs to be done as DB admin.
CREATE OR REPLACE FUNCTION qr_get_or_create_token(_imsi bytea)
    RETURNS TABLE (idx integer,
                   imsi bytea,
                   token bytea,
                   hashcode bytea,
                   ts integer)
AS $$
import hashlib
import nacl.utils
import struct
import time

# Prepare the plans for the required set of SQL queries.
PLANS = [('_individual_imsi',
          'SELECT idx, imsi, token, hashcode, ts'
          '  FROM tokeniser.imsi_tokens'
          '  WHERE imsi = $1',
          ['bytea']),
         ('_insert_base',
          'INSERT INTO tokeniser.imsi_tokens'
          '  (imsi, token, ts) VALUES ($1, $2, $3)'
          '  RETURNING idx',
          ['bytea', 'bytea', 'integer']),
         ('_get_prev_hash',
          'SELECT hashcode'
          '  FROM tokeniser.imsi_tokens t_prev'
          '  WHERE idx = $1',
          ['integer']),
         ('_update_hash',
          'UPDATE tokeniser.imsi_tokens'
          '  SET hashcode = $1'
          '  WHERE idx = $2',
          ['bytea', 'integer'])
]
for plan_handle, query, argtypes in PLANS:
    if plan_handle not in SD:
        plan = plpy.prepare(query, argtypes)
        SD[plan_handle] = plan
# Only create entry if not already exists.
value = plpy.execute(SD['_individual_imsi'], [_imsi], 1)
if not value:
    # Create new entry.
    ts = int(time.time())
    token = nacl.utils.random(8)
    result = plpy.execute(SD['_insert_base'], [_imsi, token, ts])
    idx = result[0]['idx']
    # Get last index, previous hashcode, and compute new hashcode.
    result = plpy.execute(SD['_get_prev_hash'], [idx - 1])
    last_hashcode = result[0]['hashcode']
    to_hash = b''.join([bytes(last_hashcode),
                        struct.pack('>I', idx),
                        _imsi,
                        token])
    hashcode = hashlib.sha256(to_hash).digest()
    # Update record.
    plpy.execute(SD['_update_hash'], [hashcode, idx])
    # Set expected record to return.
    value = [{'idx': idx,
              'imsi': _imsi,
              'token': token,
              'hashcode': hashcode,
              'ts': ts}]
return value
$$ LANGUAGE plpythonu;
