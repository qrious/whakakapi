=======================
Whakakapi Documentation
=======================

This documentation consists of explanatory content on the Whakakapi
tokeniser for data anonymisation to be used on IMSIs in Spark's mobile
network location records.


Building
--------

The documentation is written using the Sphinx documentation tool. You
may run `make html` in a shell (UNIX) in this directory to generate
it, or alternatively any of the targets listed in `make
help`.

