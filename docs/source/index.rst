=================================
Whakakapi Tokeniser Documentation
=================================

This documentation consists of explanatory content on the Whakakapi
tokeniser for data anonymisation to be used on IMSIs in Spark's mobile
network location records.


**Project Name**

*Whakakapi* is the Māori word for "make full" or "occupy", but also in
the sense of the verb/noun "substitute" or "replace"/"replacement".

whakakapi:

1. (verb) (-a) to fill up (a space), occupy, replace, stand as a
   substitute.
2. (verb) (-a, -ngia) to conclude, complete, close, finish (speeches,
   writing, lectures) - especially in the sense of tying together the
   ideas that have been said or written earlier or by other speakers.
3. (noun) cover, substitute, successor, replacement.


**Licence**

Whakakapi is Copyright (c) by Qrious Limited, Auckland, New
Zealand. It is licensed under the `Apache License Version 2.0
<https://opensource.org/licenses/Apache-2.0>`_.


.. only:: html

   **Contents:**

.. toctree::
   :maxdepth: 2
   :numbered:

   quick_start_deploy
   install_mdr
   dos_and_donts
   verifying_log


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
