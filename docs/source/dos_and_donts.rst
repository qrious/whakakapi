Operational and Management Guidelines
=====================================

This section contains the documentation for a number of "dos and
don'ts" to consider when operating tokenisation services.


Don't:

- Use the Flask development service when exposing the RESTful Token
  Base Web Service outside a protected network.

- Do not communicate any contents of the tokenisation log table file
  (e.g. ``imsi_token_vlog.gz``) via insecure means. This means the
  file or any of its contents **must not** be sent via email, Slack,
  etc.

- Do not back up the contents of the tokenisation log table file
  (e.g. ``imsi_token_vlog.gz``) via insecure means. Use a secure
  backup mechanism or encrypt the file before backup (e.g. using
  GnuPG).

- **DO not** oversee certain columns within a table to tokenise, that
  may carry the data to tokenise redundantly in another column
  (e.g. the IMSI value in a binary value from the salted
  hashing). That will immediately leak a large amount of one-to-one
  mapping information to the table's recipient. Make sure such columns
  are first removed!
  
  
Dos:

- Ensure only a single Token Base worker is running.

- Use a reverse proxy for security when serving the RESTful Token Base
  Web Service outside a protected network. In that case, make sure to
  only use a single Token Base Service worker! This is especially true
  if the Token Base Web Service binds to the interface all interfaces
  ('``0.0.0.0``').

- Encrypt the tokenisation log table file
  (e.g. ``imsi_token_vlog.gz``) before backup or transfer (e.g. using
  GnuPG).


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
