Procedures for Whakakapi Deployment
===================================

Note: The following instructions are **not** suitable for a productive
deployment!


The Environment and Prerequisites
---------------------------------

The code requires Python >= 3.4. It has been developed on Python 3.5
on a Linux system.


Python Virtual Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^

The code will be run with a compartmentalised Python virtual
environment, sand boxing local code dependencies without influencing
the overall system. The following will instruct how to create and
initialise a new virtual environment called ``whakakapi``.

* Requirements:

  - Python 3.x (preferably >= 3.5)
  - ``pip`` 8.x or later for Python 3.x
  - ``virtualenv`` 15.x.x (alternatively ``pyvenv``)

* Chose a location where to install the virtual environment,
  e.g. ``${HOME}``

.. code-block:: sh
    
   mkdir -p ${HOME}/.virtualenvs/
   cd ${HOME}/.virtualenvs/
   # Alternatively, you can also create a local virtual environment
   # within the project directory, then commonly called ``env``.

* Create the virtual environment

.. code-block:: sh

   # Using the tool `virtualenv` (preferred)
   virtualenv whakakapi --system-site-packages --python=/usr/bin/python3
   # Using the tool `pyvenv` (alternate, executable may vary)
   pyvenv-3.5 whakakapi
   
* Activate the virtual environment

.. code-block:: sh

   # For a user global virtual environment:
   source ${HOME}/.virtualenvs/whakakapi/bin/activate
   # For a project local virtual environment
   source env/bin/activate

* Your terminal will now be running in the virtual Python environment,
  indicated by a changed command prompt. To deactivate the virtual
  environment at any time, enter the following into your terminal:

.. code-block:: sh
    
   deactivate
   
* Install Python dependencies (into the activated virtualenv).

.. code-block:: sh

   pip install -e .

* Install packages required for a particular token base and working
  style:

.. code-block:: sh

    pip install -e .[ws]

Working styles are as follows:

  - ``ws`` -- Synchronous operation using a Flask RESTful Web Service
    as a token base.
  - ``zmq`` -- Asynchronous pipeline operation using ZeroMQ messages
    for all communication.
  - ``postgresql`` -- Synchronous operation using a PostgreSQL DB as a
    token base. The PostgreSQL DB *does not* support concurrent or
    multi-threaded operation.
   
* For installing the additional development, testing or documentation
  dependencies, add a qualifier with one or more of these commands:

.. code-block:: sh

   pip install -e .[dev]           # Development dependencies
   pip install -e .[test]          # Testing dependencies
   pip install -e .[doc]           # Documentation dependencies
   pip install -e .[dev,test,doc]  # All dependencies together

**Instructions for lacking network access for the target machine:**

In case of missing access via the Internet to PyPI, all the required
Python packages (wheels, tar-balls) can be downloaded on another
machine with network access:

* Download packages

.. code-block:: sh

   # Make a download directory and "pull" the packages
   mkdir venv_packages
   python3 -m pip download -d venv_packages -r requirements.txt
   # Tar up the packages to transfer them to the target machine
   tar cfvz venv_packages.tar.gz venv_packages/

* Unpack and install the packages on the target machine

.. code-block:: sh

   # Unpack tar ball
   tar xfvz venv_packages.tar.gz
   # Activate the environment and install the packages
   source whakakapi/bin/activate
   pip install venv_packages/*


Base System Install without a Virtual Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The install can also be performed on the base system, avoiding the use
of a virtual environment. Due to the nature of this, possible troubles
and hicc-ups may be harder to resolve, as a virtual environment can be
easily discarded and re-built.

* To avoid restrictive file permission policies, it may be wise to
  check whether the currently used ``umask`` is not to
  restrictive. Make sure the ``umask`` is set to a value of ``0022``
  for general usage of the installed packages.

* Install the required dependencies into the system. For this,
  navigate (``cd``) to the checked out code repository's base
  directory. Then install the code and dependencies to the system's
  Python packages. Obviously this install has to be performed using
  ``root`` privileges. Due to the potential ``umask`` problem (see
  step above) it may be advisable to perform the following steps as
  user ``root`` without the use of ``sudo``.

.. code-block:: sh

   $ sudo su -
   # umask 022
   # cd /path/to/project/whakakapi
   # python3 -m pip install .

* Install dependency packages required for a particular token base and
  working style:

.. code-block:: sh

   # python3 -m pip install .[ws]


Configure Services
------------------

The default configuration is located in
``whakakapi.config._Settings``. Every directive can be overridden via
a YAML configuration file. For these configuration file a system wide
file as well as a per user configuration file are parsed and
applied. Under Linux, these are commonly ``/etc/xdg/whakakapi``
(system-wide file) and ``${HOME}/.config/whakakapi`` (user specific
file).

Such a file could for example look like this (reconfiguring to a UNIX
socket end point on the worker-service communication, and changing the
reporting interval):
   
.. code-block:: yaml
   
   # Which file to store the token table in.
   token_log_filename: /home/whakakapi/imsi_token_vlog.gz
   # Reporting to the logs every 100k records, but at latest every 10 min.
   reporting_interval_records: 100000
   reporting_interval_time: 600
   
   # Token base Web Service configuration.
   # The interface and port the service binds to.
   # (0.0.0.0: binds to all interfaces, might be dangerous!)
   ws_base_bind_host: 127.0.1.1
   ws_base_port: 5000
   # Host that serves the token base service (used by the worker).
   ws_base_host: tokeniser.example.com

   # Email reporting configuration.
   mail_notifications_to:
     - devops-team@example.com
     - maintainer@example.com
   smtp_server: smtp.example.com


Run Services
------------

Manual Start/Stop
^^^^^^^^^^^^^^^^^

The tokeniser contains of a number of components to execute in the
``whakakapi/scripts`` directory. Components are using a number of suffixes indicating their implementation concept:

- ``_ws`` - For a the RESTful Web Service communication between Token
  Base and Worker.
- ``_zmq`` - For the fully asynchronous streaming communication using
  ZeroMQ (within the pipeline as well as with a ZeroMQ Token Base
  service).
- ``_db`` - For a PostgreSQL database to use as a Token Base service.

The implementation concepts are denoted below via ``XX``:
  
- ``tokenise_base_XX.py`` - The token base service. It tracks all
  IMSI to token mappings, persists these in a file, and is the ground
  truth for retrieving and assigning tokens. Only one token base
  service must ever run for a particular scenario.
- ``tokenise_worker_XX.py`` - The token worker is the 'work horse' in
  the scenario.  It keeps a running cache of all IMSI to token
  mappings, receives records containing IMSIs, and produces records
  with the IMSI replaced by a randomised token. For scalability
  several workers can run concurrently to share the load.
- ``tokenise_producer_XX.py`` - The token producer is intended for
  testing or as a reference implementation only. It generates records
  containing random IMSIs within a clamped pool of mocked devices, and
  sends them to the queue/topic the token worker(s) are listening to.
- ``tokenise_consumer_XX.py`` - The token consumer is intended for
  testing or as a reference implementation only. It receives records
  containing tokenised IMSIs from the queue/topicthe token worker(s)
  are sending to. It will just report at intervals on received tokens.

The components are executed via their corresponding Python scripts
(see list above). The entire configuration useful for productive use
is located within the configuration files. The token producer/consumer
may have some hard wired constants worth tinkering with embedded
within the reference code.

Components are started for example like the following (for the token
base):

.. code-block:: sh

   whakakapi/scripts/tokenise_base_zmq.py

The services can be terminated using the common *CRTL-C* (may need
repeating to kill all threads), or via a ``kill`` command.

For some modules, the ``setup.py`` used in the deployment will
generate handy entry scripts that will be more conveniently
usable. These are in the path of the virtual
environment. Alternatively, they will activate the virtual environment
automatically upon execution, thus not requiring a separately
activated virtual environment.


Service Daemon Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In productive environments it is usually required that certain
services are started in daemon mode at system boot time, and properly
stopped again on shut down or reboot. This is particularly true for
the Token Base service.

For exemplary configuration of such services using either (an older
legacy) ``upstart`` or (current) ``systemd`` see :ref:`install-mdr`.


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
