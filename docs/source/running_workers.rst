Executing Workers
=================

PyHive Batch Worker (WS)
------------------------

The PyHive batch worker requires first the install of the Qrious
``qat`` module.

Install Qat for PyHive Magic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This section is required for the use of the PyHive batch worker only.
 
Download the Python Wheel package and move it to the target machine in
MDR. The Wheel file should be found under
https://bitbucket.org/qriousnz/qat/src/master/dist/ (private
repository, requires login).

With the activated ``whakakapi`` virtual environment as ``root``
install the ``qat`` wheel:

.. code-block:: sh

   pip install qat-1.3.2-py2.py3-none-any.whl


Running the PyHive Batch Worker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following snippet will show how the PyHive Batch Worker can be
executed without first activating the corresponding ``whakakapi``
virtual environment.

.. code-block:: sh

   export VENVDIR=/usr/local/virtualenvs
   export VENV=$VENVDIR/whakakapi
   
   $VENV/bin/whakakapi_hive_worker \
       lbs_agg.locs2tokenise imsi lbs_agg.locs_tokenised



PySpark Hive Batch Worker (WS)
------------------------------

.. code-block:: sh

   export VENVDIR=/usr/local/virtualenvs
   export VENV=$VENVDIR/whakakapi
   export PYSPARK_PYTHON=$VENV/bin/python
   export PYSPARK_DRIVER_PYTHON=$VENV/bin/python

   spark2-submit --master yarn --conf spark.yarn.queue=root.low \
                 $VENV/lib/python3.5/site-packages/whakakapi/scripts/tokenise_worker_ws_hive_pyspark.py \
                 lbs_agg.locs2tokenise imsi lbs_agg.locs_tokenised3

Check output:

.. code-block:: sh

   export HDFS_OUT=hdfs://cdh-hdfs-isilon-01.storage.corp.qrious.co.nz:8020/apps/hive/warehouse/lbs_agg.db/locs_tokenised3
   hdfs dfs -ls $HDFS_OUT
   hdfs dfs -du -s -h $HDFS_OUT


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
