.. _install-mdr:

Installing Whakakapi WS Components in MDR Data Centre
=====================================================

These instructions are making use of the already existing Anaconda
Python 3.5 setup (under ``/usr/local/anaconda3/``).

This install is conducted with ``root`` privileges to be accessible by
everyone later. It also considers a work-around for a bug in
``pyvenv`` by providing new/upgraded version of ``pip`` in the
``whakakapi`` virtual environment. The procedure documented below will
assume that no plumbing for virtual environments, etc. are present,
yet, and that an external network connection (for HTTP/S) requires
proxying.


Prerequisites in MDR Environment
--------------------------------

These prerequisites are required before ``whakakapi`` can be installed
in the intended way.

.. code-block:: sh

   # Become root, and establish an environment for this work.
   sudo su -
   umask 022
   export http_proxy=10.100.109.5:80
   export https_proxy=10.100.109.5:80
   export PY_HOME=/usr/local/anaconda3
   export VENVDIR=/usr/local/virtualenvs
   mkdir -p $VENVDIR
   export VENV=$VENVDIR/whakakapi

   # Create and activate a new virtual environment for whakakapi.
   $PY_HOME/bin/pyvenv --system-site-packages --without-pip $VENV
   source $VENV/bin/activate
   
   # Provide an updated pip to fix the broken pyvenv.
   curl https://bootstrap.pypa.io/get-pip.py | python
   deactivate
   source $VENV/bin/activate
   $PY_HOME/bin/pip install --upgrade pip

   # Set up the user and log dirs.
   useradd --system --create-home \
           --comment 'Whakakapi Tokeniser' \
           whakakapi
   mkdir /var/log/whakakapi
   chown whakakapi:whakakapi /var/log/whakakapi


Install the Whakakapi Tokeniser (for a RESTful Token Base)
----------------------------------------------------------

These instructions and the configuration is applicable both for the
Token Base Service as well as for hosts used for Token Workers only.

First follow the instructions given in the section "Prerequisites in
MDR Environment" above. Next deploy the ``whakakapi`` code from the
Git repository:

.. code-block:: sh

   # For the workers.
   pip install --upgrade \
       'git+https://gitlab.com/qrious/whakakapi.git#egg=whakakapi[wsworker]'
   # For the token base servicde.
   pip install --upgrade \
       'git+https://gitlab.com/qrious/whakakapi.git#egg=whakakapi[wsbase]'

Configure ``whakakapi`` for operation:

.. code-block:: sh

   su - whakakapi
   mkdir -p /home/whakakapi/.config
   touch /home/whakakapi/.config/whakakapi
   chmod 0600 /home/whakakapi/.config/whakakapi

Configure the ``whakakapi`` operational concerns. The configuration
file will override the values configured (and documented) in
``whakakapi/config.py``. Under Linux, this YAML configuration file is
commonly ``${HOME}/.config/whakakapi``.

.. code-block:: yaml
   
   # Which file to store the token table in.
   token_log_filename: /home/whakakapi/imsi_token_vlog.gz
   # Name of the IMSI field in processed records.
   imsi_record_name: imsi
   # Reporting to the logs every 100k records, but at latest every 10 min.
   reporting_interval_records: 100000
   reporting_interval_time: 600
   
   # Token base Web Service configuration.
   # The interface and port the service binds to.
   # (0.0.0.0: binds to all interfaces, might be dangerous!)
   ws_base_bind_host: 10.100.99.137
   ws_base_port: 5000
   # Host that serves the token base service (used by the worker).
   ws_base_host: cdhclient-lbs01.corp.qrious.co.nz
   # Base URL the worker uses to connect to the token base service.
   # ('{host}' and '{port}' are replaced by 'ws_base_host' and 'ws_base_port'.)
   ws_base_url: http://{host}:{port}
   # Do not use the local proxy configured for the environment.
   ws_worker_use_proxy: false

   # Email reporting configuration.
   mail_notifications_to:
     - sre@qrious.co.nz
     - guy.kloss@qrious.co.nz
   smtp_server: localhost


Ansible Deployment of Whakakapi to a Cluster
--------------------------------------------

The deployment of all code (for install and upgrade) to an entire
cluster can be automated using an Ansible play book. Please, refer to
the play book code and documentation here:

https://bitbucket.org/qriousnz/ansible-pb-whakakapi/src

For Hadoop-based workers, Whakakapi should be deployed to the entire
cluster (nodes ``cdh-s1.corp.qrious.co.nz`` through
``cdh-s24.corp.qrious.co.nz``).

A suitable ``inventory`` file may look like the following. The
corresponding ``ansible.pem`` key file is securely stored in our
Bitwarden instance.

.. code-block::

   [masters]
   cdhclient-lbs01.corp.qrious.co.nz ansible_host=10.100.99.137
   
   [workers]
   
   cdh-s1.corp.qrious.co.nz ansible_host=10.100.99.93
   cdh-s2.corp.qrious.co.nz ansible_host=10.100.99.94
   cdh-s3.corp.qrious.co.nz ansible_host=10.100.99.95
   cdh-s4.corp.qrious.co.nz ansible_host=10.100.99.96
   cdh-s5.corp.qrious.co.nz ansible_host=10.100.99.97
   cdh-s6.corp.qrious.co.nz ansible_host=10.100.99.98
   cdh-s7.corp.qrious.co.nz ansible_host=10.100.99.99
   cdh-s8.corp.qrious.co.nz ansible_host=10.100.99.101
   cdh-s9.corp.qrious.co.nz ansible_host=10.100.99.102
   cdh-s10.corp.qrious.co.nz ansible_host=10.100.99.103
   cdh-s11.corp.qrious.co.nz ansible_host=10.100.99.104
   cdh-s12.corp.qrious.co.nz ansible_host=10.100.99.105
   cdh-s13.corp.qrious.co.nz ansible_host=10.100.99.106
   cdh-s14.corp.qrious.co.nz ansible_host=10.100.99.107
   cdh-s15.corp.qrious.co.nz ansible_host=10.100.99.109
   cdh-s16.corp.qrious.co.nz ansible_host=10.100.99.110
   cdh-s17.corp.qrious.co.nz ansible_host=10.100.99.111
   cdh-s18.corp.qrious.co.nz ansible_host=10.100.99.112
   cdh-s19.corp.qrious.co.nz ansible_host=10.100.99.113
   cdh-s20.corp.qrious.co.nz ansible_host=10.100.99.132
   cdh-s21.corp.qrious.co.nz ansible_host=10.100.99.133
   cdh-s22.corp.qrious.co.nz ansible_host=10.100.99.134
   cdh-s23.corp.qrious.co.nz ansible_host=10.100.99.135
   cdh-s24.corp.qrious.co.nz ansible_host=10.100.99.136
   
   [all:vars]
   ansible_user=ansible
   ansible_ssh_private_key_file=~/.ssh/ansible.pem


Deployment the Token Base as a System Service
---------------------------------------------

There are different ways of hosting the Token Base service: Fronted by
a 'proper' Web server (running the Token Base service code, e.g. using
WSGI), or natively using the Flask built-in development server. The
former is preferred for performance, reliability as well as security
purposes.

If one **has** to run the Token Base service using the Flask built-in
server, one has to integrate it with the system's service management
system. Depending on the underlying ``init`` system, the machinery for
this is different. There are three different type of ``init`` systems:
System V (very old), ``upstart`` (old), ``systemd`` (current). Only
the two newer ones will be documented here.

uWSGI fronted by Nginx
^^^^^^^^^^^^^^^^^^^^^^

Install the required distribution packages for the Nginx Web server:

.. code-block:: sh

   sudo yum install nginx libXdmcp

Create a WSGI entry point in ``/home/whakakapi/whakakapi_base.wsgi``:

.. code-block:: python

   import os
   import sys
   import site
   
   VENV = '/usr/local/virtualenvs/whakakapi'
   ALLDIRS = [os.path.join(VENV, 'lib/python3.5/site-packages')]
   
   print('Python version: {0}'.format(sys.version))
   
   # Remember original sys.path.
   prev_sys_path = list(sys.path)
   
   # Add each new site-packages directory.
   for directory in ALLDIRS:
       site.addsitedir(directory)
   
   # Reorder sys.path so new directories at the front.
   new_sys_path = [] 
   for item in list(sys.path): 
       if item not in prev_sys_path: 
           new_sys_path.append(item) 
           sys.path.remove(item) 
   sys.path[:0] = new_sys_path
   
   # Now start the Token Base Service.
   from whakakapi.scripts.tokenise_base_ws import main
   application = main()

Start the uWSGI server for testing alone:

.. code-block:: sh

   uwsgi --socket 0.0.0.0:5000 --protocol=http --wsgi-file whakakapi_base.wsgi

Then 'hit' it with ``curl`` for a quick test:

.. code-block:: sh

   http_proxy='' curl -i http://10.100.99.137:5000/mapping?limit=5

Terminate the server using CTRL-C.

Now create a uWSGI configuration file
``/home/whakakapi/whakakapi_base.ini``:

.. code-block:: ini

   [uwsgi]
   wsgi-file = whakakapi_base.wsgi
   
   chdir = /home/whakakapi
   virtualenv = /usr/local/virtualenvs/whakakapi

   master = true
   processes = 1
   enable-threads = true
   threads = 16
   
   socket = whakakapi_base.sock
   uid = whakakapi
   gid = nginx
   chmod-socket = 660
   vacuum = true
   
   die-on-term = true

Create an ``upstart`` or ``systemd`` style system service start up
configuration as described in the sections below, depending on whether
the system is using the former or latter ``init`` system.

Create an Nginx site configuration file in
``/etc/nginx/conf.d/whakakapi_base_nginx.conf``:

.. code-block::

   server {
       listen 5000;
       server_name cdhclient-lbs01.corp.qrious.co.nz;
   
       location / {
           include uwsgi_params;
           uwsgi_pass unix:/home/whakakapi/whakakapi_base.sock;
       }
   }

A sample file is provided in the Whakakapi repository under
``contrib/whakakapi_base_nginx.conf`` which can be copied.

Note: The ``location`` section may also contain ``gzip`` directives
for response body compression. Though, tests on the local environment
have shown that the compression overhead over the short distance and
small latency networking does not benefit response times or
throughput.

Nginx must have access to our application directory in order to access
the socket file there.

.. code-block:: sh

   sudo usermod -a -G whakakapi nginx
   sudo chmod 750 /home/whakakapi
   sudo chown :nginx /home/whakakapi/imsi_token_vlog.gz
   sudo chown :nginx /home/whakakapi
   
Now start and enable (for reboot) Nginx:

.. code-block:: sh

   sudo service nginx start


Apache WSGI Fronted
^^^^^^^^^^^^^^^^^^^

Install the required distribution packages for the Apache (2.2) Web
server and the WSGI module:

.. code-block:: sh

   sudo yum install httpd mod_wsgi

Create a WSGI entry point in ``/home/whakakapi/whakakapi_base.wsgi``:

.. code-block:: python

   import os
   import sys
   import site
   
   VENV = '/usr/local/virtualenvs/whakakapi'
   ALLDIRS = [os.path.join(VENV, 'lib/python3.5/site-packages')]
   
   print('Python version: {0}'.format(sys.version))
   
   # Remember original sys.path.
   prev_sys_path = list(sys.path)
   
   # Add each new site-packages directory.
   for directory in ALLDIRS:
       site.addsitedir(directory)
   
   # Reorder sys.path so new directories at the front.
   new_sys_path = [] 
   for item in list(sys.path): 
       if item not in prev_sys_path: 
           new_sys_path.append(item) 
           sys.path.remove(item) 
   sys.path[:0] = new_sys_path
   
   # Now start the Token Base Service.
   from whakakapi.scripts.tokenise_base_ws import main
   application = main()

Create an Apache (2.2) site configuration file in
``/etc/httpd/conf.d/whakakapi_base.conf``:

.. code-block::

   WSGISocketPrefix /var/run/wsgi
   Listen 5000
   <VirtualHost *:5000>
       ServerAdmin guy.kloss@qrious.co.nz
       ServerName cdhclient-lbs01.corp.qrious.co.nz
   
       WSGIScriptAlias / /home/whakakapi/whakakapi_base.wsgi
   
       WSGIDaemonProcess whakakapi_base user=whakakapi group=whakakapi \
           processes=1 threads=10 maximum-requests=1000 umask=0007 \
           python-path=/usr/local/virtualenvs/whakakapi/lib/python3.5/site-packages \
           display-name=whakakapi_base
   
       <Directory /home/whakakapi>
           WSGIProcessGroup whakakapi_base
           WSGIApplicationGroup %{GLOBAL}
           Order deny,allow
           Allow from all
       </Directory>
   
       ErrorLog /var/log/httpd/whakakapi_base.error.log
       CustomLog /var/log/httpd/whakakapi_base.log common
   </VirtualHost>

Note: If using ``mod_wsgi`` in version 3.4 or above, it is advisable
to replace the ``python-path`` option on the ``WSGIDaemonProcess``
directive with ``python-home``. This will make the configuration
tidier by only requiring the base directory of the virtual
environment: ``/usr/local/virtualenvs/whakakapi``. Source:
http://blog.dscpl.com.au/2014/09/using-python-virtual-environments-with.html

Touch up some permissions, so that Apache can access the ``whakakapi``
user's file contents it needs:

.. code-block:: sh

   sudo usermod -a -G whakakapi apache
   sudo chmod 750 /home/whakakapi
   sudo chmod 660 /home/whakakapi/imsi_token_vlog.gz
   sudo chown :apache /home/whakakapi/imsi_token_vlog.gz
   sudo chown :apache /home/whakakapi


upstart
^^^^^^^

Create an ``upstart`` "Job Configuration" file in
``/etc/init/whakakapi_base.conf`` (owned by ``root``). A sample file
is provided in the Whakakapi repository under
``contrib/whakakapi_base.conf`` which can be copied.

Assure that the permissions for the file are set correctly:

.. code-block:: sh

   sudo chmod 644 /etc/init/whakakapi_base.conf

Now tell ``upstart`` to start it during the boot sequence. Reload all
service configurations:

.. code-block:: sh

   sudo initctl reload-configuration

And start the service:

.. code-block:: sh

   sudo start whakakapi_base


systemd
^^^^^^^

Create a ``systemd`` "unit file" in
``/lib/systemd/system/whakakapi_base.service`` (owned by ``root``). A
sample file is provided in the Whakakapi repository under
``contrib/whakakapi_base.service`` which can be copied.

Note: Try if the location ``/usr/local/systemd/system/`` works. That
seems cleaner.

Assure that the permissions for the file are set correctly:

.. code-block:: sh

   sudo chmod 644 /lib/systemd/system/whakakapi_base.service

Now tell ``systemd`` to start it during the boot sequence. First
reload:

.. code-block:: sh

   sudo systemctl daemon-reload

Then enable your service:

.. code-block:: sh

   sudo systemctl enable whakakapi_base.service

Now the Whakakapi token base service will be started along with the
system at boot time. To control the service (directly) one can use the
``service`` or ``systemctl`` commands, e.g. for starting:

.. code-block:: sh

   sudo service whakakapi_base start
   # or
   sudo systemctl start whakakapi_base
   


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
