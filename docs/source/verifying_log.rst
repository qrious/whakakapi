Verifying Log
=============

This section describes the data structure(s) and mechanisms of the
verifying log. It is used to assure that no data inconsistencies can
ever occur in a running environment. Or to put it in other terms: if
an inconsistency *should* happen, it will be detected and processing
will stop immediately.

Any instance of the ``VerifyingLog`` class will carry an uninterrupted
sequence of records (described below). Each record inserted will have
to follow the sequence uninterruptedly as well as meet the consistency
requirements (validation of the computed hash value, described below).

If these records are to be transmitted or stored in text (ASCII) form,
the binary values (``imsi``, ``token`` and ``hash``) should be Base-64
encoded.


Records
-------

The data structure breaks down records into a number of fields.

- ``index`` -- Integer index continuously counting upwards, starting
  from zero. Gaps in the numbering are not permitted. This field is
  unique and automatically incremented for any new record added.
- ``imsi`` -- The IMSI in binary form (8 bytes). IMSI values coming in
  or going out may need to be converted from/to 64 bit signed integers
  to this format. This field must be unique and never empty.
- ``token`` -- The corresponding token to the IMSI from the previous
  field in the same format (binary form, 8 bytes, may need conversion
  to/from 64 bit signed integers used for 'external'
  communication). This field must be unique and never empty.
- ``hash`` -- A SHA-256 hash value in binary form (32 bytes) to assure
  the integrity of the record as well as the record together with its
  preceding sequence. This field must never be empty.
- ``ts`` -- A time stamp in UNIX epoch seconds encoded as an
  integer. This last field is optional, and for informational purposes
  only, thus it may be empty or missing.


Computation of Record Hash
--------------------------

The veifying record hash is produced by concatenating a number of byte
sequences, and then hashing the total content.

The hash of record :math:`n` is computed as follows:

.. math::

   hash_n = \operatorname{SHA-256}(hash_{n-1}~|~index_n~|~imsi_n~|~token_n)

Notes:

- In this case :math:`hash_{n-1}` is the :math:`hash` value of the
  previous record (if it exists). If it does not exist, the content
  of a ``GENESIS_BLOCK`` (configured as a constant in the code) is
  used.
- The :math:`index` value is the integer converted to the shortest
  possible byte sequence in big endian encoding (e.g. 42: 1 byte,
  4711: 2 bytes, 325895: 3 bytes, etc.)
- As mentioned in the section above, :math:`imsi` and :math:`token`
  are used in their binary (8 bytes) form natively.
- The time stamp ``ts`` is of informational purpose only, and thus not
  considered int he computation of the record hash.


Persistence of Records
----------------------

Out of the box the verifying log can persist a token table in a file
(various compressions possible, e.g. ``gzip``), just by using the
optional ``filename`` parameter in the constructor call. Records will
be written as one record per line, each line as a JSON encoded list
item.

If one is using a database as a token base storage service (e.g. by
using a stored procedure and storage schema as described using
``schema/schema.sql``), one must be aware that automatic sequence
indexing in the DB starts with index ``1`` (not ``0`` as the verifying
log assumes). The easiest way to solve the problem is to create a
dummy record with index ``0`` in the storage table to allow the
database to operate within common expected parameters. A description
for such a dummy record is given in the SQL schema description file.


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
