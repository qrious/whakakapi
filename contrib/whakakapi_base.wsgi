# Activate virtual environment in the presence of an `activate_this.lpy` file:
# activate_this = '/usr/local/virtualenvs/whakakapi/bin/activate_this.py'
# with open(activate_this) as file_:
#     exec(file_.read(), dict(__file__=activate_this))

# Alternative, according to Graham Dumpleton:
# http://blog.dscpl.com.au/2014/09/using-python-virtual-environments-with.html

import os
import sys
import site

VENV = '/usr/local/virtualenvs/whakakapi'
ALLDIRS = [os.path.join(VENV, 'lib/python3.5/site-packages')]

print('Python version: {0}'.format(sys.version))

# Remember original sys.path.
prev_sys_path = list(sys.path)

# Add each new site-packages directory.
for directory in ALLDIRS:
    site.addsitedir(directory)

# Reorder sys.path so new directories at the front.
new_sys_path = [] 
for item in list(sys.path): 
    if item not in prev_sys_path: 
        new_sys_path.append(item) 
        sys.path.remove(item) 
sys.path[:0] = new_sys_path

# Now start the Token Base Service.
from whakakapi.scripts.tokenise_base_ws import main
application = main()
