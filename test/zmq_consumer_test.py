# -*- coding: utf-8 -*-
"""
Tests for the zmq_consumer module.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

from unittest import mock
import unittest

from whakakapi.zmq_consumer import GetRecords


class GetRecordsTest(unittest.TestCase):
    """Testing the GetRecords class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch('whakakapi.zmq_consumer.REPORTING_INTERVAL', 2)
    @mock.patch('whakakapi.zmq_consumer.logger', autospec=True)
    @mock.patch('whakakapi.zmq_producer.zmq_comms.Puller', autospec=True)
    def test_process(self, PullerMock, logger_mock):
        '''process vanilla'''
        record_receiver = GetRecords('tcp://127.0.0.1:4711')
        self.assertEqual(PullerMock.call_count, 1)
        record_receiver.process(b'foo')
        self.assertEqual(logger_mock.info.call_count, 0)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(record_receiver.counter, 1)
        record_receiver.process(b'bar')
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(record_receiver.counter, 2)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
