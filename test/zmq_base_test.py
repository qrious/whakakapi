# -*- coding: utf-8 -*-
"""
Tests for the zmq_base module.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import io
from unittest import mock  # @UnusedImport
import unittest

import test_data
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.zmq_base import ZmqTokenSender
from whakakapi.zmq_base import ZmqTokenService


@mock.patch('whakakapi.zmq_base.zmq_comms.Publisher', autospec=True)
@mock.patch('whakakapi.zmq_base.logger', autospec=True)
class TokenSenderTest(unittest.TestCase):
    """Testing the ZmqTokenSender class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_publish_directed(self, logger_mock, PublisherMock):
        '''publish a directed message'''
        header = {'request_for': 'imsi',
                  'more_records': 0,
                  'tx': 'aDR4MHI'}
        message = ManagedMessage(topic=b'aGFsOTAwMA',
                                 header=header,
                                 body=b'My hovercraft is full of eels.')
        responder = ZmqTokenSender('tcp://127.0.0.1:4711')
        self.assertEqual(PublisherMock.call_count, 1)
        responder.publish(message)
        self.assertEqual(logger_mock.debug.call_count, 0)
        self.assertEqual(responder.counter, 1)
        self.assertEqual(responder._sender.publish.call_count, 1)

    def test_publish_broadcast(self, logger_mock, _):
        '''publish a broadcast message'''
        header = {'request_for': 'imsi',
                  'more_records': 0}
        message = ManagedMessage(topic=b'update',
                                 header=header,
                                 body=b'My hovercraft is full of eels.')
        responder = ZmqTokenSender('tcp://127.0.0.1:4711')
        responder.publish(message)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(responder.counter, 1)
        self.assertEqual(responder._sender.publish.call_count, 1)


class TokenServiceTest(unittest.TestCase):
    """Testing the ZmqTokenService class."""

    test_log = None
    my_token_sender = None
    my_service = None

    @mock.patch('whakakapi.zmq_producer.zmq_comms.Puller', autospec=True)
    @mock.patch('whakakapi.zmq_base.ZmqTokenSender', autospec=True)
    def setUp(self, TokenSenderMock, _):
        self.test_log = VerifyingLog()
        self.test_log.insert_many(test_data.DESERIALISED_LOG)
        self.my_token_sender = TokenSenderMock('foo')
        self.my_service = ZmqTokenService('foo',
                                          self.test_log,
                                          self.my_token_sender)

    def tearDown(self):
        pass

    @mock.patch('whakakapi.zmq_producer.zmq_comms.Puller', autospec=True)
    def test_constructor(self, PullerMock):
        '''constructor'''
        my_service = ZmqTokenService('foo', self.test_log, self.my_token_sender)
        self.assertEqual(PullerMock.call_count, 1)
        self.assertEqual(my_service.counter, 0)
        self.assertEqual(my_service._receiver.run.call_count, 1)

    def test_process_request_for_imsi(self):
        '''process request for existing IMSI'''
        parsed_request = {'request_for': 'imsi',
                          'imsi': 'NTMwMDUwMDAwMDAwMDA0',
                          'requester_tip': 3}
        record = ('[4, "NTMwMDUwMDAwMDAwMDA0", "q4knBdnAHZPGfDTauSKWVg==",'
                  ' "TXCWZSpSDQEphoJ91PiiBb1BZid3XPP7MUjK5yINC3I="]')
        self.test_log.get_record = mock.MagicMock(
            spec=self.test_log.get_record,
            return_value=record)
        result = self.my_service._process_request_for_imsi(parsed_request)
        self.assertTupleEqual(result, (record.encode(), 0))
        self.assertEqual(self.test_log.get_record.call_count, 1)
        self.assertEqual(self.my_token_sender.publish.call_count, 0)

    def test_process_request_for_imsi_with_catchup(self):
        '''process request for existing IMSI with catchup'''
        parsed_request = {'request_for': 'imsi',
                          'imsi': 'NTMwMDUwMDAwMDAwMDA0',
                          'requester_tip': 2}
        record = ('[4, "NTMwMDUwMDAwMDAwMDA0", "q4knBdnAHZPGfDTauSKWVg==",'
                  ' "TXCWZSpSDQEphoJ91PiiBb1BZid3XPP7MUjK5yINC3I="]')
        self.test_log.get_record = mock.MagicMock(
            spec=self.test_log.get_record,
            return_value=record)
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after,
            return_value=(b'stuff', 0))
        result = self.my_service._process_request_for_imsi(parsed_request)
        self.assertTupleEqual(result, (b'stuff', 0))
        self.assertEqual(self.test_log.get_record.call_count, 1)
        self.assertEqual(self.my_token_sender.publish.call_count, 0)
        self.assertEqual(self.my_service._process_request_for_after.call_count, 1)

    def test_process_request_for_imsi_new_imsi(self):
        '''process request for new IMSI'''
        parsed_request = {'request_for': 'imsi',
                          'imsi': 'NTMwMDUwMDAwMDAwMDA1',
                          'requester_tip': 4}
        record = ('[5, "NTMwMDUwMDAwMDAwMDA2", "abc123abc123abc123abc1==",'
                  ' "ABC123abcABC123abcABC123abcABC123abcABC123a="]')
        self.test_log.get_record = mock.MagicMock(
            spec=self.test_log.get_record,
            return_value=record)
        result = self.my_service._process_request_for_imsi(parsed_request)
        self.assertTupleEqual(result, (record.encode(), 0))
        self.assertEqual(self.test_log.get_record.call_count, 1)
        self.assertEqual(self.my_service.token_sender.publish.call_count, 1)
        self.assert_(isinstance(
            self.my_service.token_sender.publish.call_args[0][0],
            ManagedMessage))

    def test_process_request_for_imsi_new_imsi_with_catchup(self):
        '''process request for new IMSI with catchup'''
        parsed_request = {'request_for': 'imsi',
                          'imsi': 'NTMwMDUwMDAwMDAwMDA1',
                          'requester_tip': 3}

        def side_effect(*args, **kwargs):  # @UnusedVariable
            self.test_log.tip += 1
            return test_data.RESPONSE_RECORD

        self.test_log.get_record = mock.MagicMock(
            spec=self.test_log.get_record,
            side_effect=side_effect)
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after,
            return_value=(b'stuff', 0))
        result = self.my_service._process_request_for_imsi(parsed_request)
        self.assertTupleEqual(result, (b'stuff', 0))
        self.assertEqual(self.test_log.get_record.call_count, 1)
        self.assertEqual(self.my_token_sender.publish.call_count, 1)
        self.assert_(isinstance(
            self.my_service.token_sender.publish.call_args[0][0],
            ManagedMessage))
        self.assertEqual(self.my_service._process_request_for_after.call_count, 1)

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after(self, logger_mock):
        '''process request for records after'''
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': 1}
        records = '\n'.join(test_data.SERIALISED_LOG_NO_TS.strip().split('\n')[2:]) + '\n'
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (records.encode(), 0))
        self.assertEqual(logger_mock.debug.call_count, 0)
        self.assertEqual(logger_mock.info.call_count, 0)

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after_imsi_relay(self, logger_mock):
        '''process request for records after relayed from IMSI'''
        parsed_request = {'request_for': 'imsi',
                          'imsi': 'NTMwMDUwMDAwMDAwMDAy',
                          'requester_tip': 1}
        records = '\n'.join(test_data.SERIALISED_LOG_NO_TS.strip().split('\n')[2:]) + '\n'
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (records.encode(), 0))
        self.assertEqual(logger_mock.debug.call_count, 0)
        self.assertEqual(logger_mock.info.call_count, 0)

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after_with_limit(self, logger_mock):
        '''process request for records after with limit'''
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': 1,
                          'limit': 2}
        records = '\n'.join(test_data.SERIALISED_LOG_NO_TS.strip().split('\n')[2:4]) + '\n'
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (records.encode(), 1))
        self.assertEqual(logger_mock.debug.call_count, 0)
        self.assertEqual(logger_mock.info.call_count, 0)

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after_at_end(self, logger_mock):
        '''process request for records after at end'''
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': 4}
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (b'', 0))
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(logger_mock.info.call_count, 0)

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after_time_reporting(self, logger_mock):
        '''process request for records after with time reporting'''
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': 1}
        self.my_service._after_timing_reporting_threshold = 2
        self.my_service._process_request_for_after(parsed_request)
        self.assertEqual(logger_mock.debug.call_count, 0)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assert_('for 3 records:' in logger_mock.info.call_args[0][0])

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_request_for_after_past_end(self, logger_mock):
        '''process request for records after past end'''
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': 5}
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (b'', 0))
        self.assertEqual(logger_mock.debug.call_count, 1)

    def test_process_request_for_after_empty_log(self):
        '''process request for records after on empty log'''
        test_log = VerifyingLog()
        self.my_service.token_table = test_log
        parsed_request = {'request_for': 'records_after',
                          'requester_tip': None,
                          'limit': 100}
        result = self.my_service._process_request_for_after(parsed_request)
        self.assertTupleEqual(result, (b'', 0))

    def test_process_imsi_request(self):
        '''process a vanilla IMSI request'''
        self.my_service._process_request_for_imsi = mock.MagicMock(
            spec=self.my_service._process_request_for_imsi,
            return_value=(test_data.RESPONSE_RECORD.encode(), 0)
        )
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after)
        self.my_service.process(test_data.REQUEST_MESSAGE_IMSI)
        self.assertEqual(self.my_service._process_request_for_imsi.call_count, 1)
        self.assertEqual(self.my_service._process_request_for_after.call_count, 0)
        self.assertEqual(self.my_token_sender.publish.call_count, 1)
        published_message = self.my_token_sender.publish.call_args[0][0]
        self.assertEqual(published_message.topic.decode(),
                         test_data.REQUEST_HEADER['from'])
        self.assertDictEqual(published_message.header, {
            'tx': test_data.REQUEST_HEADER['tx'],
            'request_for': 'imsi',
            'more_records': 0})
        self.assertEqual(published_message.body,
                         test_data.RESPONSE_RECORD.encode())

    def test_process_imsi_request_no_tx(self):
        '''process an IMSI request with no tx ID'''
        self.my_service._process_request_for_imsi = mock.MagicMock(
            spec=self.my_service._process_request_for_imsi,
            return_value=(test_data.RESPONSE_RECORD.encode(), 0)
        )
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after)
        self.my_service.process(test_data.REQUEST_MESSAGE_IMSI_NO_TX)
        self.assertEqual(self.my_service._process_request_for_imsi.call_count, 1)
        self.assertEqual(self.my_service._process_request_for_after.call_count, 0)
        self.assertEqual(self.my_token_sender.publish.call_count, 1)
        published_message = self.my_token_sender.publish.call_args[0][0]
        self.assertEqual(published_message.topic.decode(),
                         test_data.REQUEST_HEADER['from'])
        self.assertDictEqual(published_message.header, {
            'request_for': 'imsi',
            'more_records': 0})
        self.assertEqual(published_message.body,
                         test_data.RESPONSE_RECORD.encode())

    def test_process_records_after_request(self):
        '''process a vanilla 'records after' request'''
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after,
            return_value=(test_data.SERIALISED_LOG_NO_TS.encode(), 0)
        )
        self.my_service._process_request_for_imsi = mock.MagicMock(
            spec=self.my_service._process_request_for_imsi)
        self.my_service.process(test_data.REQUEST_MESSAGE_FROM)
        self.assertEqual(self.my_service._process_request_for_imsi.call_count, 0)
        self.assertEqual(self.my_service._process_request_for_after.call_count, 1)
        self.assertEqual(self.my_token_sender.publish.call_count, 1)
        published_message = self.my_token_sender.publish.call_args[0][0]
        self.assertEqual(published_message.topic.decode(),
                         test_data.REQUEST_HEADER['from'])
        self.assertDictEqual(published_message.header, {
            'request_for': 'records_after',
            'tx': test_data.REQUEST_HEADER['tx'],
            'more_records': 0})
        self.assertEqual(published_message.body,
                         test_data.SERIALISED_LOG_NO_TS.encode())

    @mock.patch('whakakapi.zmq_base.logger', autospec=True)
    def test_process_unknown_request(self, logger_mock):
        '''process an unknown request'''
        self.my_service._process_request_for_after = mock.MagicMock(
            spec=self.my_service._process_request_for_after)
        self.my_service._process_request_for_imsi = mock.MagicMock(
            spec=self.my_service._process_request_for_imsi)
        self.my_service.process(test_data.REQUEST_MESSAGE_INVALID)
        self.assertEqual(self.my_service._process_request_for_imsi.call_count, 0)
        self.assertEqual(self.my_service._process_request_for_after.call_count, 0)
        self.assertEqual(self.my_token_sender.publish.call_count, 0)
        self.assertEqual(logger_mock.warning.call_count, 1)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
