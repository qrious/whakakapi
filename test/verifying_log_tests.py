# -*- coding: utf-8 -*-
"""
Tests for the verifying_log module.
"""

# Created: 2017-04-10 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import io
from unittest import mock
import unittest

from test_data import DESERIALISED_LOG
from test_data import SERIALISED_LOG
from test_data import SERIALISED_LOG_NO_TS
from whakakapi.verifying_log import GENESIS_BLOCK
from whakakapi.verifying_log import VerifyingLog
from whakakapi.verifying_log import deserialisable_value
from whakakapi.verifying_log import deserialise_record
from whakakapi.verifying_log import int_to_bytes
from whakakapi.verifying_log import serialisable_value
from whakakapi.verifying_log import serialise_record
from whakakapi.verifying_log import tip_to_integer


class VerifyingLogModuleTest(unittest.TestCase):
    """Testing the functions in the verifying_log module."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_serialise_record(self):
        '''serialising a record'''
        tests = [
            [b'foo', 'bar', 42, 3.141, True, None, ''],
            (b'foo', 'bar', 42, 3.141, True, None, ''),
            [],
            (),
            [b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89'],
            DESERIALISED_LOG[0]
        ]
        expected = [
            '["Zm9v", "YmFy", 42, 3.141, true, null, ""]',
            '["Zm9v", "YmFy", 42, 3.141, true, null, ""]',
            '[]',
            '[]',
            '["ZFJMIoYAPWNDOrWCdZrxiQ=="]',
            SERIALISED_LOG.split('\n')[0]
        ]
        for test, check in zip(tests, expected):
            self.assertEqual(serialise_record(test), check)

    def test_serialisable_value(self):
        '''making a value serialisable'''
        tests = [b'foo', 'bar', 42, 3.141, True, None, '',
                 b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89']
        expected = ['Zm9v', 'YmFy', 42, 3.141, True, None, '',
                    'ZFJMIoYAPWNDOrWCdZrxiQ==']
        for test, check in zip(tests, expected):
            self.assertEqual(serialisable_value(test), check)

    def test_deserialise_record(self):
        '''deserialising a record'''
        tests = [
            '["Zm9v", "YmFy", 42, 3.141, true, null, ""]',
            '[]',
            SERIALISED_LOG.split('\n')[0]
        ]
        expected = [
            [b'foo', b'bar', 42, 3.141, True, None, b''],
            [],
            list(DESERIALISED_LOG[0])
        ]
        for test, check in zip(tests, expected):
            self.assertListEqual(deserialise_record(test), check)

    def test_deserialisable_value(self):
        '''making a serialisable value accessible again'''
        tests = ['Zm9v', 'YmFy', 42, 3.141, True, None, '',
                 'ZFJMIoYAPWNDOrWCdZrxiQ==']
        expected = [b'foo', b'bar', 42, 3.141, True, None, b'',
                    b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89']
        for test, check in zip(tests, expected):
            self.assertEqual(deserialisable_value(test), check)

    def test_tip_to_integer(self):
        '''tip indicators mapped to signed integers for comparison'''
        tests = [None, 0, 1, 42]
        expected = [-1, 0, 1, 42]
        for test, check in zip(tests, expected):
            self.assertEqual(tip_to_integer(test), check)

    def test_int_to_bytes(self):
        '''convert an unsigned integer to bytes'''
        tests = [0, 1, 42, 4711, 321987, 987654321]
        expected = [b'\x00\x00\x00\x00', b'\x00\x00\x00\x01', b'\x00\x00\x00*',
                    b'\x00\x00\x12g', b'\x00\x04\xe9\xc3', b':\xdeh\xb1']
        for test, check in zip(tests, expected):
            self.assertEqual(int_to_bytes(test), check)

    def test_int_to_bytes_fail(self):
        '''convert an unsigned integer to bytes, unsuitable parameters'''
        tests = [-1, None, 1 << 34]
        expected = [OverflowError, AttributeError, OverflowError]
        for test, check in zip(tests, expected):
            self.assertRaises(check, int_to_bytes, test)


class VerifyingLogTest(unittest.TestCase):
    """Testing the VerifyingLog class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch('whakakapi.verifying_log.VerifyingLog.load', autospec=True)
    def test_init(self, load_mock):
        '''constructor vanilla'''
        my_log = VerifyingLog()
        self.assertEqual(my_log.filename, None)
        self.assertEqual(load_mock.call_count, 0)

    @mock.patch('whakakapi.verifying_log.VerifyingLog.load', autospec=True)
    @mock.patch('whakakapi.verifying_log.gzip.open', autospec=True)
    def test_init_with_filename(self, open_mock, load_mock):
        '''constructor test with file name'''
        test_log = '/tmp/foo.log'
        my_log = VerifyingLog(test_log)
        self.assertEqual(my_log.filename, test_log)
        self.assertEqual(load_mock.call_count, 1)
        self.assertEqual(open_mock.call_count, 1)
        self.assertEqual(open_mock.call_args, mock.call('/tmp/foo.log', 'rt'))
        self.assertEqual(my_log.filename, test_log)

    @mock.patch('whakakapi.verifying_log.logger', autospec=True)
    @mock.patch('whakakapi.verifying_log.VerifyingLog.load', autospec=True)
    @mock.patch('whakakapi.verifying_log.gzip.open', autospec=True,
                side_effect=FileNotFoundError(2, 'No such file or directory'))
    def test_init_with_filename_missing(self, open_mock, load_mock,
                                        logger_mock):
        '''constructor test with non-existent file name'''
        test_log = '/tmp/foo.log'
        my_log = VerifyingLog(test_log)
        self.assertEqual(my_log.filename, test_log)
        self.assertEqual(load_mock.call_count, 0)
        self.assertEqual(open_mock.call_count, 1)
        self.assertEqual(open_mock.call_args, mock.call('/tmp/foo.log', 'rt'))
        self.assertEqual(my_log.filename, test_log)
        self.assertEqual(logger_mock.warn.call_args[0][0],
                         'Log file /tmp/foo.log not present: creating it.')

    @mock.patch('hashlib.sha256', autospec=True)
    def test_new_record_hash_first(self, sha256_mock):
        '''computing the hash of a new record on an empty log'''
        hash_mock = mock.MagicMock()
        hash_mock.digest = mock.MagicMock(return_value=b'mashed')
        sha256_mock.return_value = hash_mock
        my_log = VerifyingLog()
        record_hash = my_log._new_record_hash(0, b'08/15', b'randomtoken')
        self.assertEqual(record_hash, b'mashed')
        self.assertEqual(sha256_mock.call_count, 1)
        self.assertEqual(hash_mock.digest.call_count, 1)
        self.assertEqual(sha256_mock.call_args[0][0],
                         GENESIS_BLOCK + b'\x00\x00\x00\x0008/15randomtoken')

    @mock.patch('hashlib.sha256', autospec=True)
    def test_new_record_hash_second(self, sha256_mock):
        '''computing the hash of a new record on a log already used'''
        hash_mock = mock.MagicMock()
        hash_mock.digest = mock.MagicMock(return_value=b'mashed')
        sha256_mock.return_value = hash_mock
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        record_hash = my_log._new_record_hash(1, b'08/15', b'randomtoken')
        self.assertEqual(record_hash, b'mashed')
        self.assertEqual(sha256_mock.call_count, 1)
        self.assertEqual(hash_mock.digest.call_count, 1)
        self.assertEqual(sha256_mock.call_args[0][0],
                         b'othermashed\x00\x00\x00\x0108/15randomtoken')

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=b'randomtoken')
    @mock.patch('time.time', autospec=True, return_value=1492046404)
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_add_first(self, new_record_hash_mock, time_mock, random_mock):
        '''adding first element'''
        my_log = VerifyingLog()
        result = my_log.add(b'08/15')
        self.assertEqual(new_record_hash_mock.call_count, 1)
        self.assertEqual(time_mock.call_count, 1)
        self.assertEqual(random_mock.call_count, 1)
        self.assertTupleEqual(result,
                              (0, b'08/15', b'randomtoken', b'mashed',
                               1492046404))
        self.assertEqual(len(my_log.imsis), 1)
        self.assertEqual(len(my_log.imsis_map), 1)
        self.assertEqual(len(my_log.tokens), 1)
        self.assertEqual(len(my_log.hashes), 1)
        self.assertEqual(len(my_log.timestamps), 1)
        self.assertEqual(my_log.tip, 0)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=b'randomtoken')
    @mock.patch('time.time', autospec=True, return_value=1492046404)
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    @mock.patch('whakakapi.verifying_log.VerifyingLog.load', autospec=True)
    @mock.patch('whakakapi.verifying_log.gzip.open', autospec=True)
    def test_add_first_with_file(self, open_mock, *_unused):
        '''adding first element and add to log file'''
        file_mock = open_mock.return_value.__enter__.return_value
        my_log = VerifyingLog('test.log')
        result = my_log.add(b'08/15')
        self.assertTupleEqual(result,
                              (0, b'08/15', b'randomtoken', b'mashed',
                               1492046404))
        self.assertEqual(open_mock.call_args, mock.call('test.log', 'at'))
        self.assertEqual(file_mock.write.call_args,
                         mock.call('[0, "MDgvMTU=", "cmFuZG9tdG9rZW4=",'
                                   ' "bWFzaGVk", 1492046404]\n'))

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=b'randomtoken')
    @mock.patch('time.time', autospec=True, return_value=1492046404)
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_add_second(self, new_record_hash_mock, time_mock, random_mock):
        '''adding first element'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        result = my_log.add(b'08/15')
        self.assertEqual(new_record_hash_mock.call_count, 1)
        self.assertEqual(time_mock.call_count, 1)
        self.assertEqual(random_mock.call_count, 1)
        self.assertTupleEqual(result,
                              (1, b'08/15', b'randomtoken', b'mashed',
                               1492046404))
        self.assertEqual(len(my_log.imsis), 2)
        self.assertEqual(len(my_log.imsis_map), 2)
        self.assertEqual(len(my_log.tokens), 2)
        self.assertEqual(len(my_log.hashes), 2)
        self.assertEqual(len(my_log.timestamps), 2)
        self.assertEqual(my_log.tip, 1)

    @mock.patch('nacl.utils.random', autospec=True,
                return_value=b'randomtoken')
    @mock.patch('time.time', autospec=True, return_value=1492046404)
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_add_existing(self, new_record_hash_mock, time_mock, random_mock):
        '''adding existing element'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        result = my_log.add(b'08/14')
        self.assertEqual(new_record_hash_mock.call_count, 0)
        self.assertEqual(time_mock.call_count, 0)
        self.assertEqual(random_mock.call_count, 0)
        self.assertEqual(result, None)
        self.assertEqual(len(my_log.imsis), 1)
        self.assertEqual(len(my_log.imsis_map), 1)
        self.assertEqual(len(my_log.tokens), 1)
        self.assertEqual(len(my_log.hashes), 1)
        self.assertEqual(len(my_log.timestamps), 1)
        self.assertEqual(my_log.tip, 0)

    @mock.patch('nacl.utils.random', autospec=True,
                side_effect=(b'randomtoken1', b'randomtoken2'))
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed2')
    def test_add_collision(self, _, random_mock):
        '''adding existing element'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/15']
        my_log.imsis_map = {b'08/15': 0}
        my_log.tokens = [b'randomtoken1']
        my_log.hashes = [b'mashed1']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        my_log.add(b'08/16')
        self.assertSetEqual(set(my_log.tokens),
                            {b'randomtoken1', b'randomtoken2'})
        self.assertEqual(random_mock.call_count, 2)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_first(self, new_record_hash_mock):
        '''inserting first record'''
        my_log = VerifyingLog()
        my_log.insert(0, b'08/15', b'randomtoken', b'mashed',
                      timestamp=1492046404)
        self.assertEqual(new_record_hash_mock.call_count, 1)
        self.assertEqual(len(my_log.imsis), 1)
        self.assertEqual(len(my_log.imsis_map), 1)
        self.assertEqual(len(my_log.tokens), 1)
        self.assertEqual(len(my_log.hashes), 1)
        self.assertEqual(len(my_log.timestamps), 1)
        self.assertEqual(my_log.tip, 0)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_second(self, new_record_hash_mock):
        '''inserting second record'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        my_log.insert(1, b'08/15', b'randomtoken', b'mashed',
                      timestamp=1492046404)
        self.assertEqual(new_record_hash_mock.call_count, 1)
        self.assertEqual(len(my_log.imsis), 2)
        self.assertEqual(len(my_log.imsis_map), 2)
        self.assertEqual(len(my_log.tokens), 2)
        self.assertEqual(len(my_log.hashes), 2)
        self.assertEqual(len(my_log.timestamps), 2)
        self.assertEqual(my_log.tip, 1)

    @mock.patch('whakakapi.verifying_log.logger', autospec=True)
    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_existing(self, new_record_hash_mock, logger_mock):
        '''inserting an existent record'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        my_log.insert(0, b'08/14', b'otherrandomtoken', b'othermashed',
                      timestamp=1492046394)
        self.assertEqual(new_record_hash_mock.call_count, 0)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(len(my_log.imsis), 1)
        self.assertEqual(len(my_log.imsis_map), 1)
        self.assertEqual(len(my_log.tokens), 1)
        self.assertEqual(len(my_log.hashes), 1)
        self.assertEqual(len(my_log.timestamps), 1)
        self.assertEqual(my_log.tip, 0)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_second_no_ts(self, new_record_hash_mock):
        '''inserting second record, without timestamp'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        my_log.insert(1, b'08/15', b'randomtoken', b'mashed')
        self.assertEqual(new_record_hash_mock.call_count, 1)
        self.assertEqual(len(my_log.imsis), 2)
        self.assertEqual(len(my_log.imsis_map), 2)
        self.assertEqual(len(my_log.tokens), 2)
        self.assertEqual(len(my_log.hashes), 2)
        self.assertEqual(len(my_log.timestamps), 2)
        self.assertEqual(my_log.tip, 1)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_failed_on_tip_first(self, new_record_hash_mock):
        '''inserting record, wrong record tip on first element'''
        my_log = VerifyingLog()
        self.assertRaises(ValueError, my_log.insert,
                          1, b'08/15', b'randomtoken', b'mashed',
                          timestamp=1492046404)
        self.assertEqual(new_record_hash_mock.call_count, 0)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_failed_on_tip(self, new_record_hash_mock):
        '''inserting record, wrong record tip'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertRaises(ValueError, my_log.insert,
                          2, b'08/15', b'randomtoken', b'mashed',
                          timestamp=1492046404)
        self.assertEqual(new_record_hash_mock.call_count, 0)

    @mock.patch('whakakapi.verifying_log.VerifyingLog._new_record_hash',
                autospec=True, return_value=b'mashed')
    def test_insert_failed_on_hash(self, new_record_hash_mock):
        '''inserting record, wrong record hash'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'otherrandomtoken']
        my_log.hashes = [b'othermashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertRaises(ValueError, my_log.insert,
                          1, b'08/15', b'randomtoken', b'bad_hash',
                          timestamp=1492046404)
        self.assertEqual(new_record_hash_mock.call_count, 1)

    @mock.patch('whakakapi.verifying_log.VerifyingLog.insert',
                autospec=True, return_value=b'mashed')
    def test_insert_many(self, insert_mock):
        '''inserting many records'''
        my_log = VerifyingLog()
        to_add = [(0, b'08/14', b'otherrandomtoken', b'othermashed',
                   1492046394),
                  (1, b'08/15', b'randomtoken', b'mashed', 1492046404)]
        my_log.insert_many(to_add)
        self.assertEqual(insert_mock.call_count, len(to_add))

    def test_get__record(self):
        '''getting a complete record by index'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        self.assertTupleEqual(my_log._get_record(2),
                              tuple(DESERIALISED_LOG[2]))

    def test_has_token(self):
        '''checking for a token record'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'randomtoken']
        my_log.hashes = [b'mashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        tests = [b'08/14', b'08/15']
        expected = [True, False]
        for test, check in zip(tests, expected):
            self.assertEqual(my_log.has_token(test), check)

    def test_get_token(self):
        '''getting a record's token'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'randomtoken']
        my_log.hashes = [b'mashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertEqual(my_log.get_token(b'08/14'), b'randomtoken')

    def test_get_token_failed(self):
        '''getting a missing record's token'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'randomtoken']
        my_log.hashes = [b'mashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertRaises(KeyError, my_log.get_token, b'08/15')

    def test_get_imsi(self):
        '''getting a record's IMSI'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'randomtoken']
        my_log.hashes = [b'mashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertEqual(my_log.get_imsi(b'randomtoken'), b'08/14')

    def test_get_imsi_failed(self):
        '''getting a missing record's IMSI'''
        my_log = VerifyingLog()
        my_log.imsis = [b'08/14']
        my_log.imsis_map = {b'08/14': 0}
        my_log.tokens = [b'randomtoken']
        my_log.hashes = [b'mashed']
        my_log.timestamps = [1492046394]
        my_log.tip = 0
        self.assertRaises(ValueError, my_log.get_imsi, b'otherrandomtoken')

    def test_load_with_imsis(self):
        '''load many records from IMSIs'''
        to_load = '\n'.join(SERIALISED_LOG.split('\n')[:2]) + '\n'
        my_log = VerifyingLog()
        my_log.load(imsis=io.StringIO(to_load))
        self.assertEqual(my_log.tip, 1)
        self.assertListEqual(my_log.imsis,
                             [DESERIALISED_LOG[0][1], DESERIALISED_LOG[1][1]])
        self.assertListEqual(my_log.tokens,
                             [DESERIALISED_LOG[0][2], DESERIALISED_LOG[1][2]])
        self.assertListEqual(my_log.hashes,
                             [DESERIALISED_LOG[0][3], DESERIALISED_LOG[1][3]])
        self.assertListEqual(my_log.timestamps,
                             [DESERIALISED_LOG[0][4], DESERIALISED_LOG[1][4]])
        self.assertDictEqual(my_log.imsis_map, {DESERIALISED_LOG[0][1]: 0,
                                                DESERIALISED_LOG[1][1]: 1})

    def test_load_with_further_imsis(self):
        '''load further records from IMSIs'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        to_load = '\n'.join(SERIALISED_LOG.split('\n')[2:])
        my_log.load(imsis=io.StringIO(to_load))
        self.assertEqual(my_log.tip, 4)
        self.assertDictEqual(my_log.imsis_map,
                             {DESERIALISED_LOG[i][1]: i
                              for i in range(len(DESERIALISED_LOG))})

    def test_get_record_by_index(self):
        '''getting a complete serialised record by index'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        result = my_log.get_record(2)
        self.assertEqual(result, SERIALISED_LOG_NO_TS.split('\n')[2])

    def test_get_record_by_index_with_ts(self):
        '''getting a complete serialised record by index with timestamp'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        result = my_log.get_record(2, include_ts=True)
        self.assertEqual(result, SERIALISED_LOG.split('\n')[2])

    def test_get_record_by_imsi(self):
        '''getting a complete serialised record by IMSI'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        result = my_log.get_record(imsi=b'530050000000002')
        self.assertEqual(result, SERIALISED_LOG_NO_TS.split('\n')[2])

    def test_get_record_fail(self):
        '''getting a complete serialised record by IMSI and index fail'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        self.assertRaises(SystemExit, my_log.get_record,
                          imsi=b'530050000000001', index=1)

    def test_get_records_after(self):
        '''get records after'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        lines, num, more = my_log.get_records_after(1)
        lines = lines.strip().split('\n')
        self.assertEqual(num, 3)
        self.assertEqual(lines[0], SERIALISED_LOG_NO_TS.split('\n')[2])
        self.assertEqual(more, 0)

    def test_get_records_after_on_empty_log(self):
        '''get records after on an empty log'''
        my_log = VerifyingLog()
        result = my_log.get_records_after()
        self.assertTupleEqual(result, ('', 0, 0))

    def test_get_records_after_from_start(self):
        '''get records after from start'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        lines, num, more = my_log.get_records_after()
        lines = lines.strip().split('\n')
        self.assertEqual(num, len(DESERIALISED_LOG))
        self.assertEqual(lines[0], SERIALISED_LOG_NO_TS.split('\n')[0])
        self.assertEqual(more, 0)

    def test_get_records_after_past_end(self):
        '''get records after past the end'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        result = my_log.get_records_after(7)
        self.assertTupleEqual(result, ('', 0, 0))

    def test_get_records_after_by_imsi(self):
        '''get records after by IMSI with timestamps'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        lines, num, more = my_log.get_records_after(imsi=b'530050000000001',
                                                    include_ts=True)
        lines = lines.strip().split('\n')
        self.assertEqual(num, 3)
        self.assertEqual(lines[0], SERIALISED_LOG.split('\n')[2])
        self.assertEqual(more, 0)

    def test_get_records_after_with_ts(self):
        '''get records after with timestamps'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        lines, num, more = my_log.get_records_after(1, include_ts=True)
        lines = lines.strip().split('\n')
        self.assertEqual(num, 3)
        self.assertEqual(lines[0], SERIALISED_LOG.split('\n')[2])
        self.assertEqual(more, 0)

    def test_get_records_after_fail(self):
        '''get records after by IMSI and index fail'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        self.assertRaises(SystemExit, my_log.get_records_after,
                          imsi=b'530050000000001', index=1)

    def test_get_records_after_with_ts_limit(self):
        '''get records after with timestamps and limit'''
        my_log = VerifyingLog()
        my_log.insert_many(DESERIALISED_LOG)
        lines, num, more = my_log.get_records_after(1, include_ts=True, limit=2)
        lines = lines.strip().split('\n')
        self.assertEqual(num, 2)
        self.assertEqual(lines[-1], SERIALISED_LOG.split('\n')[3])
        self.assertEqual(more, 1)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
