# -*- coding: utf-8 -*-
"""
Tests for the db_worker module.
"""

# Created: 2017-10-30 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import psycopg2
from unittest import mock
import unittest

from test_data import DESERIALISED_LOG
from whakakapi.db_worker import BATCH_PLAN_EXECUTE, INDIVIDUAL_PLAN_EXECUTE
from whakakapi.db_worker import DbTokenBase
from whakakapi.db_worker import record_transform
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog


class ModuleTest(unittest.TestCase):
    """Testing the db_worker module functions."""

    def test_record_transform(self):
        '''normal transformation of token records'''
        test = [(0,
                 memoryview(b'aaa0'),
                 memoryview(b'bbb0'),
                 memoryview(b'ccc0'),
                 0),
                (42,
                 memoryview(b'aaa42'),
                 memoryview(b'bbb42'),
                 memoryview(b'ccc42'),
                 123),
                (43,
                 memoryview(b'aaa43'),
                 memoryview(b'bbb43'),
                 memoryview(b'ccc43'),
                 124), ]
        expected = [(0, b'aaa0', b'bbb0', b'ccc0', 0),
                    (42, b'aaa42', b'bbb42', b'ccc42', 123),
                    (43, b'aaa43', b'bbb43', b'ccc43', 124)]
        for elements in range(5, 1, -1):
            to_test = [item[:elements] for item in test]
            for item, check in zip(record_transform(to_test), expected):
                self.assertEqual(item, check[:elements])


class DbTokenBaseTest(unittest.TestCase):
    """Testing the DbTokenBase class."""

    test_log = None
    my_base = None

    @mock.patch('psycopg2.connect', mock.create_autospec(psycopg2.connect))
    def setUp(self):
        self.test_log = VerifyingLog()
        # Load first two entries only.
        self.test_log.insert_many(DESERIALISED_LOG[:2])
        self.my_base = DbTokenBase(self.test_log)

    def test__request_token_existing(self):
        '''request existing token from DB'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        cursor_mock.fetchall.return_value = [DESERIALISED_LOG[1][:-1]]
        result = self.my_base._request_token(b'530050000000001', 5)
        self.assertEqual(result, DESERIALISED_LOG[1][2])
        self.assertEqual(self.test_log.tip, 1)
        self.assertEqual(cursor_mock.execute.call_count, 1)
        self.assertEqual(cursor_mock.close.call_count, 1)

    def test__request_token_new(self):
        '''request new token from DB'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        cursor_mock.fetchall.return_value = [DESERIALISED_LOG[2][:-1]]
        result = self.my_base._request_token(b'530050000000002', 5)
        self.assertEqual(result, DESERIALISED_LOG[2][2])
        self.assertEqual(self.test_log.tip, 2)
        self.assertEqual(cursor_mock.execute.call_count, 1)
        self.assertEqual(INDIVIDUAL_PLAN_EXECUTE,
                         cursor_mock.execute.call_args_list[0][0][0])
        self.assertTupleEqual(cursor_mock.execute.call_args_list[0][0][1],
                              (b'530050000000002',))
        self.assertEqual(cursor_mock.fetchall.call_count, 1)
        self.assertEqual(cursor_mock.close.call_count, 1)

    def test__request_token_skipped(self):
        '''request new token from DB, skipped one ahead'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        cursor_mock.fetchall.side_effect = ([DESERIALISED_LOG[3][:-1]],
                                            [DESERIALISED_LOG[2][:-1],
                                             DESERIALISED_LOG[3][:-1]],
                                            [])
        result = self.my_base._request_token(b'530050000000003', 5)
        self.assertEqual(result, DESERIALISED_LOG[3][2])
        self.assertEqual(self.test_log.tip, 3)
        self.assertEqual(cursor_mock.execute.call_count, 2)
        self.assertEqual(BATCH_PLAN_EXECUTE,
                         cursor_mock.execute.call_args_list[1][0][0])
        self.assertTupleEqual(cursor_mock.execute.call_args_list[1][0][1],
                              (1, 5))
        self.assertEqual(cursor_mock.fetchall.call_count, 2)

    def test__request_token_empty_log(self):
        '''request new token from DB, on empty log'''
        self.my_base.token_table = VerifyingLog()
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        cursor_mock.fetchall.side_effect = ([DESERIALISED_LOG[4][:-1]],
                                            [DESERIALISED_LOG[0][:-1],
                                             DESERIALISED_LOG[1][:-1]],
                                            [DESERIALISED_LOG[2][:-1],
                                             DESERIALISED_LOG[3][:-1]],
                                            [DESERIALISED_LOG[4][:-1]])
        result = self.my_base._request_token(b'530050000000004', 2)
        self.assertEqual(result, DESERIALISED_LOG[4][2])
        self.assertEqual(self.my_base.token_table.tip, 4)
        self.assertEqual(cursor_mock.execute.call_count, 4)
        self.assertTupleEqual(cursor_mock.execute.call_args_list[1][0][1],
                              (-1, 2))

    def test__request_token_multi_skipped(self):
        '''request new token from DB, skipped multiple ahead'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        cursor_mock.fetchall.side_effect = ([DESERIALISED_LOG[4][:-1]],
                                            [DESERIALISED_LOG[2][:-1],
                                             DESERIALISED_LOG[3][:-1]],
                                            [DESERIALISED_LOG[4][:-1]])
        result = self.my_base._request_token(b'530050000000004', 2)
        self.assertEqual(result, DESERIALISED_LOG[4][2])
        self.assertEqual(self.test_log.tip, 4)
        self.assertEqual(cursor_mock.execute.call_count, 3)
        self.assertTupleEqual(cursor_mock.execute.call_args_list[1][0][1],
                              (1, 2))
        self.assertTupleEqual(cursor_mock.execute.call_args_list[2][0][1],
                              (3, 2))

    def test__request_records_after(self):
        '''request `records after` from DB'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        db_records = [DESERIALISED_LOG[2][:-1],
                      DESERIALISED_LOG[3][:-1]]
        cursor_mock.fetchall.return_value = db_records
        result = self.my_base._request_records_after(1, 2)
        self.assertListEqual([x for x in result],
                             [x[:-1] for x in DESERIALISED_LOG[2:4]])
        self.assertEqual(self.test_log.tip, 3)
        self.assertEqual(cursor_mock.execute.call_count, 1)
        self.assertEqual(BATCH_PLAN_EXECUTE,
                         cursor_mock.execute.call_args_list[0][0][0])
        self.assertTupleEqual(cursor_mock.execute.call_args_list[0][0][1],
                              (2, 2))
        self.assertEqual(cursor_mock.fetchall.call_count, 1)

    def test__request_records_after_under_limit(self):
        '''request `records after` from DB, less than given limit'''
        cursor_mock = mock.create_autospec(psycopg2.extensions.cursor)
        self.my_base._db.cursor.return_value = cursor_mock
        db_records = [DESERIALISED_LOG[2][:-1],
                      DESERIALISED_LOG[3][:-1],
                      DESERIALISED_LOG[4][:-1]]
        cursor_mock.fetchall.return_value = db_records
        result = self.my_base._request_records_after(1, 5)
        self.assertListEqual([x for x in result],
                             [x[:-1] for x in DESERIALISED_LOG[2:5]])
        self.assertEqual(self.test_log.tip, 4)
        self.assertEqual(cursor_mock.execute.call_count, 1)
        self.assertTupleEqual(cursor_mock.execute.call_args_list[0][0][1],
                              (2, 5))
        self.assertEqual(cursor_mock.fetchall.call_count, 1)

    @mock.patch('whakakapi.db_worker.DbTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.db_worker.DbTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_imsi(self, request_token_mock,
                                    request_records_after_mock):
        '''request an IMSI token synchronously'''
        body = {'request_for': 'imsi', 'imsi': 'abcd1234', 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        result = self.my_base.request_impl_sync(message)
        self.assertEqual(request_token_mock.call_count, 1)
        self.assertEqual(request_token_mock.call_args_list[0],
                         mock.call(self.my_base, b'i\xb7\x1d\xd7m\xf8', 5))
        self.assertEqual(request_records_after_mock.call_count, 0)
        self.assertEqual(result, request_token_mock.return_value)

    @mock.patch('whakakapi.db_worker.DbTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.db_worker.DbTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_records_after(self, request_token_mock,
                                             request_records_after_mock):
        '''request 'records after' synchronously'''
        body = {'request_for': 'records_after',
                'requester_tip': 2, 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        result = self.my_base.request_impl_sync(message)
        self.assertEqual(request_token_mock.call_count, 0)
        self.assertEqual(request_records_after_mock.call_count, 1)
        self.assertEqual(request_records_after_mock.call_args_list[0],
                         mock.call(self.my_base, 2, 5))
        self.assertEqual(result, request_records_after_mock.return_value)

    @mock.patch('whakakapi.db_worker.DbTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.db_worker.DbTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_failed_op(self, request_token_mock,
                                         request_records_after_mock):
        '''request for an unsupported operation'''
        body = {'request_for': 'foo', 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        self.assertRaises(SystemExit,
                          self.my_base.request_impl_sync, message)
        self.assertEqual(request_token_mock.call_count, 0)
        self.assertEqual(request_records_after_mock.call_count, 0)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
