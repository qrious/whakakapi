# -*- coding: utf-8 -*-
"""
Tests for gauging the performance of the VerifyingLog class.

To run:

$ PERF_TEST=1 nosetests --nocapture
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import os
import random
import time
import unittest

from whakakapi.verifying_log import VerifyingLog


NO_DEVICES = 4000000
EPM = 2400000
REPORTING_STEPS = 1000000
LOG_PATH = '/tmp/test.vlog.gz'
SKIP = 'PERF_TEST' not in os.environ


def do_it(loops):
    start_time = time.time()
    delta_start = start_time
    my_log = VerifyingLog(LOG_PATH)
    dt = time.time() - delta_start
    print('Initialise log: time: {:2.2f} s, records: {}, coverage: {:2.2f} %'
          .format(dt, len(my_log.imsis),
                  100 * len(my_log.tokens) / NO_DEVICES))
    delta_start = time.time()
    generated = 0
    processed = 0
    for _ in range(loops):
        imsi = ('53005{:010d}'
                .format(random.randrange(0, NO_DEVICES))
                .encode())
        if not my_log.has_token(imsi):
            my_log.add(imsi)
            generated += 1
        else:
            my_log.get_token(imsi)
        processed += 1
        if processed % REPORTING_STEPS == 0:
            dt = time.time() - delta_start
            print('Steps: {}, time: {:2.2f} s,'
                  ' generated: {}, coverage: {:2.2f} %'
                  .format(processed, dt, generated,
                          100 * len(my_log.tokens) / NO_DEVICES))
            delta_start = time.time()
            generated = 0
    print('Total time {:2.1f}M tokenisations: {:2.2f} s'
          .format(loops / 1e6, time.time() - start_time))


class PerfTest(unittest.TestCase):
    """Testing the performance of the VerifyingLog class."""

    def test_doit(self):
        '''performance test'''
        if SKIP:
            return
        try:
            os.remove(LOG_PATH)
        except FileNotFoundError:
            pass
        print('*** Fist pass: No log file.')
        do_it(5 * EPM)
        print('*** Second pass: Existing log file.')
        do_it(5 * EPM)

        self.assert_(False, 'dummy to dump output on stdout')


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
