# -*- coding: utf-8 -*-
"""
Tests for the ws_base module.
"""

# Created: 2017-11-16 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import json
from unittest import mock  # @UnusedImport
import unittest

import test_data
from whakakapi.verifying_log import VerifyingLog
from whakakapi.ws_base import WsTokenService


class WsTokenServiceTest(unittest.TestCase):
    """Testing the WsTokenService class."""

    test_log = None
    my_token_sender = None
    my_service = None

    def setUp(self):
        self.test_log = VerifyingLog()
        self.test_log.insert_many(test_data.DESERIALISED_LOG[:2])
        self.my_service = WsTokenService(self.test_log)

    def tearDown(self):
        pass

    @mock.patch('whakakapi.ws_base.Response', autospec=True)
    def test__make_response(self, Response_mock):
        '''make a vanilla HTTP response'''
        result = self.my_service._make_response('gooniegoogoo')
        self.assertIs(result, Response_mock.return_value)
        self.assertTupleEqual(Response_mock.call_args[0][:2],
                              ('gooniegoogoo', 200))
        self.assertDictEqual(Response_mock.call_args[0][2],
            {'Content-Type': 'application/vnd.qrious.whakakapi'})

    @mock.patch('whakakapi.ws_base.Response', autospec=True)
    def test__make_response_custom(self, Response_mock):
        '''make a HTTP response with custom header and return code'''
        result = self.my_service._make_response(
            'Ford Prefect', status=302, headers={'X-Status': 'frobnicated'})
        self.assertIs(result, Response_mock.return_value)
        self.assertTupleEqual(Response_mock.call_args[0][:2],
                              ('Ford Prefect', 302))
        self.assertDictEqual(Response_mock.call_args[0][2],
            {'Content-Type': 'application/vnd.qrious.whakakapi',
             'X-Status': 'frobnicated'})

    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[0][1])
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_imsi_existing(self, make_response_mock, b64decode_mock):
        '''maps an existing/cached IMSI'''
        imsi = test_data.DESERIALISED_LOG[0][1]
        result = self.my_service.map_imsi(imsi)
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1],
                         test_data.SERIALISED_LOG_NO_TS.split('\n')[0])

    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value='530050000000666')
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_imsi_missing(self, make_response_mock, b64decode_mock):
        '''fails at mapping a missing IMSI'''
        imsi = '530050000000666'
        result = self.my_service.map_imsi(imsi)
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 1)
        error_body = json.loads(make_response_mock.call_args[0][1])
        self.assertDictEqual(error_body,
            {'title': 'IMSI not found.',
             'message': 'Requested IMSI 530050000000666 not found.',
             'code': 404001})

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[0][1])
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_or_create_imsi_existing(self, make_response_mock,
                                         b64decode_mock, request_mock):
        '''maps/creates an existing/cached IMSI'''
        request_mock.args = {}
        imsi = test_data.DESERIALISED_LOG[0][1]
        result = self.my_service.map_or_create_imsi(imsi)
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1],
                         test_data.SERIALISED_LOG_NO_TS.split('\n')[0])

    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[0][1])
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    @mock.patch('whakakapi.ws_base.logger', autospec=True)
    @mock.patch('whakakapi.ws_base.request', autospec=True)
    def test_map_or_create_imsi_remark(self, request_mock, logger_mock, *_):
        '''maps/creates an IMSI record prividing a remark'''
        request_mock.args = {'rem': 'gooniegoogoo'}
        imsi = test_data.DESERIALISED_LOG[0][1]
        self.my_service.map_or_create_imsi(imsi)
        self.assertEqual(logger_mock.info.call_args[0][0],
                         'Requester remark: "gooniegoogoo".')

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[1][1])
    @mock.patch('whakakapi.ws_base.WsTokenService.records_after',
                autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_or_create_imsi_skipped(self, make_response_mock,
                                        records_after_mock,
                                        b64decode_mock, request_mock):
        '''requested existing IMSI skips requesters tip'''
        request_mock.args = {'requester_tip': ''}
        imsi = test_data.DESERIALISED_LOG[1][1]
        result = self.my_service.map_or_create_imsi(imsi)
        self.assertIs(result, records_after_mock.return_value)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 0)

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[2][1])
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_or_create_imsi_new(self, make_response_mock,
                                    b64decode_mock, request_mock):
        '''maps/creates a new IMSI'''
        request_mock.args = {}
        imsi = test_data.DESERIALISED_LOG[2][1]
        result = self.my_service.map_or_create_imsi(imsi)
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(self.test_log.tip, 2)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertListEqual(json.loads(make_response_mock.call_args[0][1])[:2],
                             json.loads(test_data.SERIALISED_LOG_NO_TS.split('\n')[2])[:2])

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.base64.urlsafe_b64decode', autospec=True,
                return_value=test_data.DESERIALISED_LOG[2][1])
    @mock.patch('whakakapi.ws_base.WsTokenService.records_after',
                autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_map_or_create_imsi_new_skipped(self, make_response_mock,
                                            records_after_mock,
                                            b64decode_mock, request_mock):
        '''requested a new IMSI skips requesters tip'''
        request_mock.args = {'requester_tip': ''}
        imsi = test_data.DESERIALISED_LOG[2][1]
        result = self.my_service.map_or_create_imsi(imsi)
        self.assertIs(result, records_after_mock.return_value)
        self.assertEqual(self.test_log.tip, 2)
        self.assertEqual(b64decode_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_count, 0)

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_vanilla(self, make_response_mock, request_mock):
        '''returns records using `records_after`'''
        request_mock.args = {'records_after': ''}
        request_mock.headers = {}
        result = self.my_service.records_after()
        expected_body = '\n'.join(
            test_data.SERIALISED_LOG_NO_TS.split('\n')[:2]) + '\n'
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertNotIn('X-More-Records', request_mock.headers)
        self.assertDictEqual(self.my_service._update_table_cache,
                             {(-1, 100000): (expected_body, 2)})

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_no_caching(self, make_response_mock, request_mock):
        '''returns records using `records_after`, no caching with incomplete batch'''
        request_mock.args = {'records_after': '', 'limit': '1'}
        request_mock.headers = {}
        result = self.my_service.records_after()
        expected_body = test_data.SERIALISED_LOG_NO_TS.split('\n')[0] + '\n'
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertNotIn('X-More-Records', request_mock.headers)
        self.assertDictEqual(self.my_service._update_table_cache, {})

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_cached(self, make_response_mock, request_mock):
        '''returns records using `records_after`, cached'''
        request_mock.args = {'records_after': ''}
        request_mock.headers = {}
        expected_body = '\n'.join(
            test_data.SERIALISED_LOG_NO_TS.split('\n')[:2]) + '\n'
        self.my_service._update_table_cache = {
            (-1, 100000): (expected_body, 2)}
        result = self.my_service.records_after()
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertNotIn('X-More-Records', request_mock.headers)

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_requester_tip(self, make_response_mock,
                                         request_mock):
        '''returns records using `requester_tip`'''
        request_mock.args = {'requester_tip': ''}
        request_mock.headers = {}
        result = self.my_service.records_after()
        expected_body = '\n'.join(
            test_data.SERIALISED_LOG_NO_TS.split('\n')[:2]) + '\n'
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertNotIn('X-More-Records', request_mock.headers)

    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_partial(self, make_response_mock, request_mock):
        '''returns partial records using `records_after`'''
        self.test_log.insert_many(test_data.DESERIALISED_LOG[2:])
        request_mock.args = {'records_after': '', 'limit': '2'}
        make_response_mock.return_value.headers = {}
        result = self.my_service.records_after()
        expected_body = '\n'.join(
            test_data.SERIALISED_LOG_NO_TS.split('\n')[:2]) + '\n'
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertEqual(make_response_mock.return_value.headers['X-More-Records'], 3)

    @mock.patch('whakakapi.ws_base.config.records_per_request_limit', 2)
    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_partial_implicit_limit(self, make_response_mock,
                                                  request_mock):
        '''returns partial records using `records_after` without `limit` param'''
        self.test_log.insert_many(test_data.DESERIALISED_LOG[2:])
        request_mock.args = {'records_after': ''}
        make_response_mock.return_value.headers = {}
        result = self.my_service.records_after()
        expected_body = '\n'.join(
            test_data.SERIALISED_LOG_NO_TS.split('\n')[:2]) + '\n'
        self.assertIs(result, make_response_mock.return_value)
        self.assertEqual(make_response_mock.call_count, 1)
        self.assertEqual(make_response_mock.call_args[0][1], expected_body)
        self.assertEqual(make_response_mock.return_value.headers['X-More-Records'], 3)

    @mock.patch('whakakapi.ws_base.WsTokenService._after_timing_reporting_threshold', 3)
    @mock.patch('whakakapi.ws_base.logger', autospec=True)
    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True)
    def test_records_after_logged(self, _, request_mock, logger_mock):
        '''returns partial records with timing log'''
        self.test_log.insert_many(test_data.DESERIALISED_LOG[2:])
        request_mock.args = {'records_after': ''}
        self.my_service.records_after()
        self.assertEqual(logger_mock.info.call_args[0][0],
                         'Processing "records_after" request for 5 records: 0.00 s')

    @mock.patch('whakakapi.ws_base.logger', autospec=True)
    @mock.patch('whakakapi.ws_base.request', autospec=True)
    @mock.patch('whakakapi.ws_base.WsTokenService._make_response',
                autospec=True, return_value='response stuff')
    def test_records_after_empty(self, make_response_mock, request_mock,
                                 logger_mock):
        '''with exhausted records'''
        request_mock.args = {'records_after': '1'}
        self.my_service.records_after()
        self.assertEqual(logger_mock.debug.call_args[0][0],
                         'No more records available after index 1, own tip is 1.')
        self.assertEqual(make_response_mock.call_args[0][1], '')


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
