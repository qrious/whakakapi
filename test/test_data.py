# -*- coding: utf-8 -*-
"""
Tests for the verifying_log module.
"""

# Created: 2017-08-01 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'


SERIALISED_LOG = (
    '[0, "NTMwMDUwMDAwMDAwMDAw", "xqYKKFUkPgc=",'
    ' "mpH2I4Z3BIfd6OX24vzCVNu2YmJsCHPpoomkd9DzYBg=", 1492555378]\n'
    '[1, "NTMwMDUwMDAwMDAwMDAx", "5e2u/NGHe3M=",'
    ' "zcCjYwF/Cp7TODZZZ0vDvKOZBz5hAcOuwMzGF1pjKcM=", 1492555379]\n'
    '[2, "NTMwMDUwMDAwMDAwMDAy", "SR2wjfQ2DiA=",'
    ' "45rzTSWm+wD+f5QNnQ4lSoDzaeziWi7tx7lnuQHLa1M=", 1492555380]\n'
    '[3, "NTMwMDUwMDAwMDAwMDAz", "WQPWQgOovjw=",'
    ' "yBx6+MLmkL2QNDu92zb6jtP3bHpoEeSdRj3ADsvBi8g=", 1492555381]\n'
    '[4, "NTMwMDUwMDAwMDAwMDA0", "ZxzxF/w9yCg=",'
    ' "Rd3r7CQ8PFHSPFIhKeWw8zfOfMV1OZX9Q9TbxoB6HCo=", 1492555382]\n'
)
SERIALISED_LOG_NO_TS = (
    '[0, "NTMwMDUwMDAwMDAwMDAw", "xqYKKFUkPgc=",'
    ' "mpH2I4Z3BIfd6OX24vzCVNu2YmJsCHPpoomkd9DzYBg="]\n'
    '[1, "NTMwMDUwMDAwMDAwMDAx", "5e2u/NGHe3M=",'
    ' "zcCjYwF/Cp7TODZZZ0vDvKOZBz5hAcOuwMzGF1pjKcM="]\n'
    '[2, "NTMwMDUwMDAwMDAwMDAy", "SR2wjfQ2DiA=",'
    ' "45rzTSWm+wD+f5QNnQ4lSoDzaeziWi7tx7lnuQHLa1M="]\n'
    '[3, "NTMwMDUwMDAwMDAwMDAz", "WQPWQgOovjw=",'
    ' "yBx6+MLmkL2QNDu92zb6jtP3bHpoEeSdRj3ADsvBi8g="]\n'
    '[4, "NTMwMDUwMDAwMDAwMDA0", "ZxzxF/w9yCg=",'
    ' "Rd3r7CQ8PFHSPFIhKeWw8zfOfMV1OZX9Q9TbxoB6HCo="]\n'
)
DESERIALISED_LOG = [
    (0,
     b'530050000000000',
     b'\xc6\xa6\n(U$>\x07',
     b'\x9a\x91\xf6#\x86w\x04\x87\xdd\xe8\xe5\xf6\xe2\xfc\xc2T\xdb\xb6bbl'
     b'\x08s\xe9\xa2\x89\xa4w\xd0\xf3`\x18',
     1492555378),
    (1,
     b'530050000000001',
     b'\xe5\xed\xae\xfc\xd1\x87{s',
     b'\xcd\xc0\xa3c\x01\x7f\n\x9e\xd386YgK\xc3\xbc\xa3\x99\x07>a\x01\xc3'
     b'\xae\xc0\xcc\xc6\x17Zc)\xc3',
     1492555379),
    (2,
     b'530050000000002',
     b'I\x1d\xb0\x8d\xf46\x0e ',
     b'\xe3\x9a\xf3M%\xa6\xfb\x00\xfe\x7f\x94\r\x9d\x0e%J\x80\xf3i\xec\xe2Z'
     b'.\xed\xc7\xb9g\xb9\x01\xcbkS',
     1492555380),
    (3,
     b'530050000000003',
     b'Y\x03\xd6B\x03\xa8\xbe<',
     b'\xc8\x1cz\xf8\xc2\xe6\x90\xbd\x904;\xbd\xdb6\xfa\x8e\xd3\xf7lzh\x11'
     b'\xe4\x9dF=\xc0\x0e\xcb\xc1\x8b\xc8',
     1492555381),
    (4,
     b'530050000000004',
     b'g\x1c\xf1\x17\xfc=\xc8(',
     b'E\xdd\xeb\xec$<<Q\xd2<R!)\xe5\xb0\xf37\xce|\xc5u9\x95\xfdC\xd4\xdb'
     b'\xc6\x80z\x1c*',
     1492555382)
]

WORKER_ID = b'YWJjZDEyMzRBQkNENTY3OA=='
REQUEST_HEADER = {'from': 'YWJjZDEyMzRBQkNENTY3OA==',
                  'tx': 'd3h5ejEyMzRXWFlaNTY3OA=='}
REQUEST_BODY_IMSI = ({'request_for': 'imsi',
                      'imsi': 'NTMwMDUwMDAwMDAwMDA1',
                      'requester_tip': 4})
REQUEST_MESSAGE_IMSI = (b'YWJjZDEyMzRBQkNENTY3OA==:'
                        b' {"from": "YWJjZDEyMzRBQkNENTY3OA==",'
                        b' "tx": "d3h5ejEyMzRXWFlaNTY3OA=="}\n'
                        b'{"request_for": "imsi",'
                        b' "imsi": "NTMwMDUwMDAwMDAwMDA1",'
                        b' "requester_tip": 4}')
REQUEST_MESSAGE_IMSI_NO_TX = (b'YWJjZDEyMzRBQkNENTY3OA==:'
                              b' {"from": "YWJjZDEyMzRBQkNENTY3OA=="}\n'
                              b'{"request_for": "imsi",'
                              b' "imsi": "NTMwMDUwMDAwMDAwMDA1",'
                              b' "requester_tip": 4}')
REQUEST_BODY_FROM = ({'request_for': 'records_after',
                      'requester_tip': None})
REQUEST_MESSAGE_FROM = (b'YWJjZDEyMzRBQkNENTY3OA==:'
                        b' {"from": "YWJjZDEyMzRBQkNENTY3OA==",'
                        b' "tx": "d3h5ejEyMzRXWFlaNTY3OA=="}\n'
                        b'{"request_for": "records_after",'
                        b' "requester_tip": null}')
REQUEST_MESSAGE_INVALID = (b'YWJjZDEyMzRBQkNENTY3OA==:'
                           b' {"from": "YWJjZDEyMzRBQkNENTY3OA==",'
                           b' "tx": "d3h5ejEyMzRXWFlaNTY3OA=="}\n'
                           b'{"request_for": "record_at",'
                           b' "tip": 666}')
RESPONSE_RECORD = ('[5, "NTMwMDUwMDAwMDAwMDA2", "abc123abc123abc123abc1==",'
                   ' "ABC123abcABC123abcABC123abcABC123abcABC123a="]')
