# -*- coding: utf-8 -*-
"""
Tests for the worker module.
"""

# Created: 2017-08-14 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import base64
import io
import json
import time
from unittest import mock
import unittest

import test_data
from whakakapi.config import config
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.worker import RecordsSink
from whakakapi.worker import RecordsWorker
from whakakapi.worker import TokenBase
from whakakapi.worker import TokenUpdater
from whakakapi.worker import generate_id


BASE_HEADER_OFFSET = len(config.token_base_topic) + 2


class ModuleTest(unittest.TestCase):
    """Testing the worker module functions."""

    @mock.patch('nacl.utils.random', return_value=b'dEADbEEfdEADbEEf')
    def test_generate_id(self, random_mock):
        '''a (mock) random ID and representation'''
        self.assertEqual(generate_id(16), b'ZEVBRGJFRWZkRUFEYkVFZg')
        self.assertEqual(random_mock.call_count, 1)


class RecordsSinkTest(unittest.TestCase):
    """Testing the RecordsSink class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch('whakakapi.worker.RecordsSink.produce_impl', autospec=True)
    def test_produce(self, produce_impl_mock):
        '''request a message to the sink'''
        sink = RecordsSink()
        data = {'imsi': 'AAHiE+H2lAI=', 'foo': 42, 'bar': 'baz'}
        sink.produce(data)
        self.assertEqual(sink.counter, 1)
        self.assertEqual(produce_impl_mock.call_count, 1)
        message_sent = produce_impl_mock.call_args[0][1]
        self.assertDictEqual(json.loads(message_sent.decode()),
                             {'imsi': 530050000000002,
                              'foo': 42, 'bar': 'baz'})


class TokenBaseTest(unittest.TestCase):
    """Testing the TokenBase class."""

    test_log = None
    my_base = None
    logger_mock = None

    def setUp(self):
        self.test_log = VerifyingLog()
        self.test_log.insert_many(test_data.DESERIALISED_LOG)
        self.my_base = TokenBase(self.test_log)
        self.my_base.request_impl_async = mock.MagicMock()
        self.my_base.request_impl_sync = mock.MagicMock()

    def tearDown(self):
        pass

    def test_request_imsi(self):
        '''request a message to the base, IMSI only'''
        self.my_base.request(imsi=b'4711')
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        message = self.my_base.request_impl_async.call_args[0][0]
        self.assertEqual(message.header['from'].encode(),
                         self.my_base.worker_id)
        tx_id = message.header['tx']
        self.assertEqual(len(tx_id), 22)
        self.assertDictEqual(message.body,
                             {'request_for': 'imsi',
                              'imsi': 'NDcxMQ==',
                              'requester_tip': 4})
        self.assertEqual(self.my_base.counter, 1)
        self.assert_(tx_id in self.my_base.tx_in_flight)
        tx_data = self.my_base.tx_in_flight[tx_id]
        self.assert_('ts' in tx_data)
        self.assertDictEqual(tx_data['header'], message.header)
        self.assertDictEqual(tx_data['body'], message.body)

    @mock.patch('whakakapi.worker.TokenBase._is_request_in_flight',
                autospec=True, return_value=False)
    def test__request_async_after(self, is_request_in_flight_mock):
        '''request a message to the base, for `records after`'''
        tx_id = b'7Wd1Rg8S-pH8PFKd4JdSsg'
        body = {'request_for': 'records_after', 'requester_tip': 1}
        header = {'tx': tx_id, 'from': self.my_base.worker_id}
        message = ManagedMessage(body=body, header=header)
        self.my_base._request_async(message)
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        out_message = self.my_base.request_impl_async.call_args[0][0]
        self.assertIs(message, out_message)
        self.assertEqual(is_request_in_flight_mock.call_count, 1)
        self.assert_(tx_id in self.my_base.tx_in_flight)
        tx_data = self.my_base.tx_in_flight[tx_id]
        self.assert_('ts' in tx_data)
        self.assertDictEqual(tx_data['header'], message.header)
        self.assertDictEqual(tx_data['body'], body)

    @mock.patch('whakakapi.worker.TokenBase._is_request_in_flight',
                autospec=True, return_value=False)
    def test__request_async_after_initial(self, _):
        '''request a message to the base, for `records after` on empty log'''
        tx_id = b'7Wd1Rg8S-pH8PFKd4JdSsg'
        body = {'request_for': 'records_after',
                'requester_tip': None,
                'limit': 2}
        header = {'tx': tx_id, 'from': self.my_base.worker_id}
        message = ManagedMessage(body=body, header=header)
        self.my_base.token_table = VerifyingLog()
        self.my_base._request_async(message)
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        out_message = self.my_base.request_impl_async.call_args[0][0]
        self.assertIs(message, out_message)

    @mock.patch('whakakapi.worker.TokenBase._is_request_in_flight',
                autospec=True, return_value=False)
    def test__request_async_after_and_limit(self, _):
        '''request a message to the base, for `records after` with limit'''
        tx_id = b'7Wd1Rg8S-pH8PFKd4JdSsg'
        body = {'request_for': 'records_after',
                'requester_tip': 1,
                'limit': 2}
        header = {'tx': tx_id, 'from': self.my_base.worker_id}
        message = ManagedMessage(body=body, header=header)
        self.my_base._request_async(message)
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        out_message = self.my_base.request_impl_async.call_args[0][0]
        self.assertIs(message, out_message)

    @mock.patch('whakakapi.worker.TokenBase._is_request_in_flight',
                autospec=True, return_value=True)
    def test__request_async_imsi_already_in_flight(self, is_request_in_flight_mock):
        '''request a message to the base, IMSI already in flight'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'requester_tip': 4,
                   'imsi': 'NDcxMQ==',
                   'request_for': 'imsi'}
        message = ManagedMessage(body=tx_body, header=tx_header)
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}

        self.my_base._request_async(message)
        self.assertEqual(self.my_base.request_impl_async.call_count, 0)
        self.assertEqual(is_request_in_flight_mock.call_count, 1)

    def test__request_async_imsi_callback(self):
        '''request a message to the base, IMSI with callback'''
        tx_id = b'7Wd1Rg8S-pH8PFKd4JdSsg'
        body = {'request_for': 'imsi',
                'requester_tip': 1,
                'imsi': 'NDcxMQ=='}
        header = {'tx': tx_id, 'from': self.my_base.worker_id}
        message = ManagedMessage(body=body, header=header)
        my_callback = mock.MagicMock()
        self.my_base._request_async(message,
                                    callback=my_callback, cb_params=42)
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        out_message = self.my_base.request_impl_async.call_args[0][0]
        self.assertIs(message, out_message)
        self.assertIs(self.my_base.tx_in_flight[tx_id]['callback'],
                      my_callback)
        self.assertEqual(self.my_base.tx_in_flight[tx_id]['cb_params'], 42)

    def test__request_sync(self):
        '''request synchronously to the base'''
        tx_id = b'7Wd1Rg8S-pH8PFKd4JdSsg'
        body = {'request_for': 'records_after',
                'requester_tip': 1,
                'limit': 100}
        header = {'tx': tx_id, 'from': self.my_base.worker_id}
        message = ManagedMessage(body=body, header=header)
        self.my_base.token_table = mock.MagicMock(spec_set=VerifyingLog)
        result = self.my_base._request_sync(message)
        self.assertIs(result, self.my_base.request_impl_sync.return_value)
        self.assertEqual(self.my_base.request_impl_sync.call_count, 1)
        self.assertEqual(self.my_base.request_impl_sync.call_args,
                         mock.call(message))

    @mock.patch('whakakapi.worker.TokenBase._request_async', autospec=True)
    def test_request_async(self, request_async_mock):
        '''request a message to the base'''
        self.my_base.request(after=1)
        self.assertEqual(self.my_base.counter, 1)
        self.assertEqual(request_async_mock.call_count, 1)
        out_message = request_async_mock.call_args_list[0][0][1]
        self.assert_('from' in out_message.header)
        self.assert_('tx' in out_message.header)
        self.assertDictEqual(out_message.body,
                             {'request_for': 'records_after',
                              'requester_tip': 1})
        self.assertTupleEqual(request_async_mock.call_args_list[0][0][2:],
                              (None, None))

    @mock.patch('whakakapi.worker.TokenBase._request_sync', autospec=True)
    def test_request_sync(self, request_sync_mock):
        '''request synchronously to the base'''
        result = self.my_base.request(after=1, synchronous=True)
        self.assertIs(result, request_sync_mock.return_value)
        self.assertEqual(self.my_base.counter, 1)
        self.assertEqual(request_sync_mock.call_count, 1)
        out_message = request_sync_mock.call_args_list[0][0][1]
        self.assert_('from' in out_message.header)
        self.assert_('tx' in out_message.header)
        self.assertDictEqual(out_message.body,
                             {'request_for': 'records_after',
                              'requester_tip': 1})

    def test_request_fail_imsi_and_after(self):
        '''request a message to the base, fails on IMSI and `after`'''
        self.assertRaises(SystemExit,
                          self.my_base.request, imsi=b'4711', after=2)

    def test__is_request_in_flight_empty_tx(self):
        '''message to the base, empty tx_in_flight'''
        self.my_base.counter = 1
        message_body = {'request_for': 'imsi',
                        'imsi': '4711',
                        'requester_tip': 4}
        self.assertFalse(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_not_in_flight(self):
        '''message to the base, IMSI not in flight'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'requester_tip': 4,
                   'imsi': 'MDgxNQ==',
                   'request_for': 'imsi'}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'imsi',
                        'imsi': '4711',
                        'requester_tip': 4}
        self.assertFalse(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_in_flight(self):
        '''message to the base, IMSI already in flight'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'requester_tip': 4,
                   'imsi': 'NDcxMQ==',
                   'request_for': 'imsi'}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'imsi',
                        'imsi': '4711',
                        'requester_tip': 4}
        self.assertTrue(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_after_in_flight(self):
        '''message to the base, `records after` already in flight'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'request_for': 'records_after', 'requester_tip': 1}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'records_after',
                        'requester_tip': 1}
        self.assertTrue(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_after_in_flight_earlier_tip(self):
        '''message to the base, for `records after` already in flight with earlier tip'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'request_for': 'records_after', 'requester_tip': 1}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'records_after',
                        'requester_tip': 2}
        self.assertTrue(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_after_in_flight_later_tip(self):
        '''message to the base, for `records after` already in flight with later tip'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'request_for': 'records_after', 'requester_tip': 1}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'records_after',
                        'requester_tip': 0}
        self.assertFalse(self.my_base._is_request_in_flight(message_body))

    def test__is_request_in_flight_after_in_flight_none_tip(self):
        '''message to the base, for `records after` already in flight with None tip'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'request_for': 'records_after', 'requester_tip': None}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'records_after',
                        'requester_tip': 2}
        self.assertTrue(self.my_base._is_request_in_flight(message_body))

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test__is_request_in_flight_after_in_flight_impossibru(self, logger_mock):
        '''message to the base, for `records after` already impossibru in flight'''
        self.my_base.counter = 1
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_ts = 1505787196.9317458
        tx_header = {'from': self.my_base.worker_id, 'tx': tx_id}
        tx_body = {'request_for': 'impossibru', 'requester_tip': 1}
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'header': tx_header,
                                            'ts': tx_ts}
        message_body = {'request_for': 'records_after',
                        'requester_tip': 2}
        self.assert_(self.my_base._is_request_in_flight(message_body) is None)
        self.assert_(logger_mock.warning.call_count, 1)

    @mock.patch('whakakapi.config.config.records_per_request_limit', 42)
    def test_update_table(self):
        '''initial token table update'''
        self.my_base.update_table()
        self.assertEqual(self.my_base.update_pending, True)
        self.assertEqual(self.my_base.request_impl_async.call_count, 1)
        message = self.my_base.request_impl_async.call_args[0][0]
        self.assertDictEqual(message.body,
                             {'limit': 42,
                              'request_for': 'records_after',
                              'requester_tip': 4})

    def test__process_head_item_empty_buffer(self):
        '''process head of token request queue on empty buffer'''
        self.assertFalse(self.my_base._process_head_item())

    def test__process_head_item_success(self):
        '''process head of token request queue with success'''
        my_callback = mock.MagicMock(return_value=True)
        self.my_base.token_request_buffer.append((my_callback, 42))
        self.assertTrue(self.my_base._process_head_item())
        my_callback.assert_called_once_with(42)
        self.assertEqual(len(self.my_base.token_request_buffer), 0)

    def test__process_head_item_failed(self):
        '''process head of token request queue with failure'''
        my_callback = mock.MagicMock(return_value=False)
        self.my_base.token_request_buffer.append((my_callback, 42))
        self.assertFalse(self.my_base._process_head_item())
        my_callback.assert_called_once_with(42)
        self.assertEqual(len(self.my_base.token_request_buffer), 1)

    @mock.patch('whakakapi.worker.TokenBase._process_head_item', autospec=True,
                side_effect=(True, None))
    def test_catch_up_processing_single_item(self, process_head_item_mock):
        '''catch up with single in-flight request succeeding'''
        self.my_base.catch_up_processing()
        self.assertEqual(process_head_item_mock.call_count, 2)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_catch_up_processing_item_fail_none_pending(self, logger_mock):
        '''catch up with item in-flight request failing'''
        self.my_base.update_pending = False
        with mock.patch.multiple(
                self.my_base,
                _process_head_item=mock.MagicMock(return_value=False),
                update_table=mock.DEFAULT):
            self.my_base.catch_up_processing()
            self.assertEqual(self.my_base._process_head_item.call_count, 1)
            self.assertEqual(logger_mock.debug.call_count, 1)
            self.assertEqual(self.my_base.update_table.call_count, 1)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_catch_up_processing_item_fail_pending(self, logger_mock):
        '''catch up with item in-flight request failing, table update pending'''
        self.my_base.update_pending = True
        with mock.patch.multiple(
                self.my_base,
                _process_head_item=mock.MagicMock(return_value=False),
                update_table=mock.DEFAULT):
            self.my_base.catch_up_processing()
            self.assertEqual(self.my_base._process_head_item.call_count, 1)
            self.assertEqual(logger_mock.debug.call_count, 1)
            self.assertEqual(self.my_base.update_table.call_count, 0)

    def test_cleanup_expired_transactions_noop(self):
        '''no clean up transactions available'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 3,
                   'imsi': 'NDcxMQ==',
                   'request_for': 'imsi'}
        tx_ts = time.time() - 0.5 * config.transaction_ttl
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'ts': tx_ts}
        self.my_base.cleanup_expired_transactions()
        self.assert_(tx_id in self.my_base.tx_in_flight)

    def test_cleanup_expired_transactions_past_ttl(self):
        '''clean up transaction past its TTL'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 4,
                   'imsi': 'NDcxMQ==',
                   'request_for': 'imsi'}
        tx_ts = time.time() - config.transaction_ttl - 1
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'ts': tx_ts}
        self.my_base.cleanup_expired_transactions()
        self.assert_(tx_id not in self.my_base.tx_in_flight)

    def test_cleanup_expired_transactions_old_tip(self):
        '''clean up transaction older than own tip'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 2,
                   'request_for': 'records_after'}
        tx_ts = time.time() - 0.5 * config.transaction_ttl
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'ts': tx_ts}
        self.my_base.cleanup_expired_transactions()
        self.assert_(tx_id not in self.my_base.tx_in_flight)

    def test_cleanup_expired_transactions_old_imsi(self):
        '''clean up transaction for existing IMSI'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 3,
                   'imsi': 'NTMwMDUwMDAwMDAwMDAz',
                   'request_for': 'imsi'}
        tx_ts = time.time() - 0.5 * config.transaction_ttl
        self.my_base.tx_in_flight[tx_id] = {'body': tx_body,
                                            'ts': tx_ts}
        self.my_base.cleanup_expired_transactions()
        self.assert_(tx_id not in self.my_base.tx_in_flight)

    def test_pop_transaction(self):
        '''pop a transaction'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 3,
                   'imsi': 'NTMwMDUwMDAwMDAwMDAz',
                   'request_for': 'imsi'}
        tx_ts = time.time() - 0.5 * config.transaction_ttl
        tx = {'body': tx_body, 'ts': tx_ts}
        self.my_base.tx_in_flight[tx_id] = tx
        result = self.my_base.pop_transaction(tx_id)
        self.assert_(tx_id not in self.my_base.tx_in_flight)
        self.assertDictEqual(result, tx)

    def test_pop_transaction_on_non_existing(self):
        '''pop a transaction on a non existing tx_in_flight'''
        tx_id = '7Wd1Rg8S-pH8PFKd4JdSsg'
        tx_body = {'requester_tip': 3,
                   'imsi': 'NTMwMDUwMDAwMDAwMDAz',
                   'request_for': 'imsi'}
        tx_ts = time.time() - 0.5 * config.transaction_ttl
        tx = {'body': tx_body, 'ts': tx_ts}
        self.my_base.tx_in_flight[tx_id] = tx
        result = self.my_base.pop_transaction('aaaaaaaaaaaaaaaaaaaaaa')
        self.assert_(tx_id in self.my_base.tx_in_flight)
        self.assertEqual(result, None)


class RecordsWorkerTest(unittest.TestCase):
    """Testing the RecordsWorker class."""

    test_log = None
    records_sink = None
    token_base = None

    @mock.patch('whakakapi.worker.RecordsSink', autospec=True)
    @mock.patch('whakakapi.worker.TokenBase', autospec=True)
    def setUp(self, TokenBaseMock, RecordsSinkMock):
        self.test_log = VerifyingLog()
        self.test_log.insert_many(test_data.DESERIALISED_LOG)
        self.records_sink = RecordsSinkMock()
        self.token_base = TokenBaseMock(self.test_log)
        self.my_worker = RecordsWorker(self.test_log, self.records_sink,
                                       self.token_base)

    def tearDown(self):
        pass

    def test_new_instance(self):
        '''testing making a new instance'''
        self.assertEqual(self.token_base.update_table.call_count, 1)
        self.assertEqual(self.my_worker.token_table, self.test_log)
        self.assertEqual(self.my_worker.records_sink, self.records_sink)
        self.assertEqual(self.my_worker.token_base, self.token_base)

    def test__process_imsi_missing(self):
        '''processing an IMSI not in token table'''
        self.my_worker.counter = 3
        self.my_worker._interval_cache_misses = 1
        data = {'imsi': b'missing_', 'foo': 'bar'}
        success = self.my_worker._process_imsi(data)

        self.assertFalse(success)
        callback, data_check = self.token_base.token_request_buffer.append.call_args[0][0]
        self.assertEqual(callback, self.my_worker._process_imsi)
        self.assertDictEqual(data_check, data)
        self.assertEqual(self.records_sink.produce.call_count, 0)
        self.assertEqual(self.token_base.request.call_count, 1)
        self.assertEqual(self.token_base.request.call_args[1]['callback'],
                         self.my_worker._process_imsi)
        self.assertDictEqual(self.token_base.request.call_args[1]['cb_params'],
                             {'imsi': b'missing_', 'foo': 'bar'})
        self.assertEqual(self.token_base.request.call_args[1]['imsi'],
                         b'missing_')
        self.assertEqual(self.token_base.token_request_buffer.append.call_count, 1)
        self.assertEqual(self.my_worker._interval_cache_misses, 2)

    def test__process_imsi_missing_sync(self):
        '''processing an IMSI not in token table, synchronous mode'''
        self.my_worker.synchronous = True
        def _update_token_table(imsi=None, synchronous=None):
            self.test_log.add(b'missing_')
        self.token_base.request.side_effect = _update_token_table
        result = self.my_worker._process_imsi({'imsi': b'missing_',
                                               'foo': 'bar'})

        token = base64.standard_b64encode(
            self.test_log.get_token(b'missing_')).decode().rstrip('=')
        self.assertDictEqual(result, {'imsi': token, 'foo': 'bar'})
        self.assertEqual(self.records_sink.produce.call_count, 0)
        self.assertEqual(self.token_base.request.call_count, 1)
        self.assertEqual(self.token_base.request.call_args,
                         mock.call(imsi=b'missing_', synchronous=True))
        self.assertEqual(self.token_base.token_request_buffer.append.call_count, 0)

    @mock.patch('whakakapi.config.config.reporting_interval', 5)
    def test__process_imsi_cached(self):
        '''processing an IMSI cached in token table'''
        self.my_worker.counter = 3
        self.my_worker._interval_cache_misses = 2
        success = self.my_worker._process_imsi({'imsi': b'530050000000002',
                                                'foo': 'bar'})

        self.assertTrue(success)
        self.assertEqual(self.token_base.request.call_count, 0)
        self.assertEqual(self.token_base.token_request_buffer.append.call_count, 0)
        self.assertEqual(self.records_sink.produce.call_count, 1)
        self.assertDictEqual(self.records_sink.produce.call_args[0][0],
                             {'imsi': 'SR2wjfQ2DiA', 'foo': 'bar'})
        self.assertEqual(self.my_worker._interval_cache_misses, 2)

    def test__process_imsi_cached_sync(self):
        '''processing an IMSI cached in token table, synchronous mode'''
        self.my_worker.synchronous = True
        result = self.my_worker._process_imsi({'imsi': b'530050000000002',
                                               'foo': 'bar'})

        self.assertDictEqual(result,
                             {'imsi': 'SR2wjfQ2DiA', 'foo': 'bar'})
        self.assertEqual(self.token_base.request.call_count, 0)
        self.assertEqual(self.token_base.token_request_buffer.append.call_count, 0)
        self.assertEqual(self.records_sink.produce.call_count, 0)

    @mock.patch('whakakapi.config.config.reporting_interval_records', 5)
    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test__process_imsi_cached_reporting(self, logger_mock):
        '''processing an IMSI cached in token table, log reporting'''
        self.my_worker._interval_records_processed = 4
        self.my_worker._interval_cache_misses = 2
        success = self.my_worker._process_imsi({'imsi': b'530050000000002',
                                                'foo': 'bar'})

        self.assertTrue(success)
        self.assertEqual(self.token_base.request.call_count, 0)
        self.assertEqual(self.records_sink.produce.call_count, 1)
        self.assertDictEqual(self.records_sink.produce.call_args[0][0],
                             {'imsi': 'SR2wjfQ2DiA', 'foo': 'bar'})
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(self.my_worker._interval_cache_misses, 0)
        self.assertEqual(self.my_worker._interval_records_processed, 0)

    @mock.patch('whakakapi.config.config.reporting_interval_time', 5)
    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test__process_imsi_cached_reporting_time(self, logger_mock):
        '''processing an IMSI cached in token table, time-based log reporting'''
        self.my_worker._interval_start_time = time.time() - 5
        self.my_worker._interval_cache_misses = 2
        success = self.my_worker._process_imsi({'imsi': b'530050000000002',
                                                'foo': 'bar'})

        self.assertTrue(success)
        self.assertEqual(self.token_base.request.call_count, 0)
        self.assertEqual(self.records_sink.produce.call_count, 1)
        self.assertDictEqual(self.records_sink.produce.call_args[0][0],
                             {'imsi': 'SR2wjfQ2DiA', 'foo': 'bar'})
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(self.my_worker._interval_cache_misses, 0)
        self.assertEqual(self.my_worker._interval_records_processed, 0)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value=True)
    def test_process_cached(self, process_imsi_mock, logger_mock):
        '''processing a token for an IMSI in token table'''
        data = {'imsi': b'\x00\x01\xe2\x13\xe1\xf6\x94\x02', 'foo': 'bar'}
        result = self.my_worker.process(
            b'{"imsi": 530050000000002, "foo": "bar"}')

        self.assertEqual(result, True)
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 1)
        self.assertDictEqual(process_imsi_mock.call_args[0][1], data)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value=False)
    def test_process_missing(self, process_imsi_mock, logger_mock):
        '''processing a token for a missing IMSI in token table'''
        data = {'imsi': b'missing_', 'foo': 'bar'}
        result = self.my_worker.process(
            b'{"imsi": 7883959562216040287, "foo": "bar"}')

        self.assertEqual(result, False)
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 1)
        self.assertDictEqual(process_imsi_mock.call_args[0][1], data)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value={'foo': 42})
    def test_process_cached_sync_bytes(self, process_imsi_mock, logger_mock):
        '''processing a token for an IMSI in token table, bytes in/out'''
        self.my_worker.synchronous = True
        data = {'imsi': b'\x00\x01\xe2\x13\xe1\xf6\x94\x02', 'foo': 'bar'}
        result = self.my_worker.process(
            b'{"imsi": 530050000000002, "foo": "bar"}')

        self.assertEqual(result, b'{"foo": 42}')
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 1)
        self.assertDictEqual(process_imsi_mock.call_args[0][1], data)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value={'foo': 42})
    def test_process_cached_sync_str(self, process_imsi_mock, logger_mock):
        '''processing a token for an IMSI in token table, string in/out'''
        self.my_worker.synchronous = True
        data = {'imsi': b'\x00\x01\xe2\x13\xe1\xf6\x94\x02', 'foo': 'bar'}
        result = self.my_worker.process(
            '{"imsi": 530050000000002, "foo": "bar"}')

        self.assertEqual(result, '{"foo": 42}')
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 1)
        self.assertDictEqual(process_imsi_mock.call_args[0][1], data)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value={'foo': 42})
    def test_process_cached_sync_dict(self, process_imsi_mock, logger_mock):
        '''processing a token for an IMSI in token table, dictionary in/out'''
        self.my_worker.synchronous = True
        data = {'imsi': b'\x00\x01\xe2\x13\xe1\xf6\x94\x02', 'foo': 'bar'}
        result = self.my_worker.process(
            {'imsi': 530050000000002, 'foo': 'bar'})

        self.assertDictEqual(result, {'foo': 42})
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 1)
        self.assertDictEqual(process_imsi_mock.call_args[0][1], data)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.worker.RecordsWorker._process_imsi',
                autospec=True, return_value={'foo': 42})
    def test_process_cached_bad_input(self, process_imsi_mock, logger_mock):
        '''processing a token for an IMSI in token table, bad input format'''
        result = self.my_worker.process([530050000000002, 'foo'])

        self.assertEqual(result, None)
        self.assertEqual(self.my_worker.counter, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(logger_mock.warning.call_count, 1)
        self.assertEqual(process_imsi_mock.call_count, 0)


class TokenUpdaterTest(unittest.TestCase):
    """Testing the TokenUpdater class."""

    test_log = None
    token_base = None

    @mock.patch('whakakapi.worker.TokenBase', autospec=True)
    def setUp(self, TokenBaseMock):
        self.test_log = VerifyingLog()
        self.test_log.insert_many(test_data.DESERIALISED_LOG)
        self.token_base = TokenBaseMock(self.test_log)
        self.my_updater = TokenUpdater(self.test_log, self.token_base)

    def tearDown(self):
        pass

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_process_single_imsi(self, logger_mock):
        '''processing a single IMSI message'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 0}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[0].encode())
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, 0)
        self.assertEqual(self.token_base.update_table.call_count, 0)
        self.assertFalse(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 0)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 1)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 1)

    def test_process_single_imsi_callback(self):
        '''processing a single IMSI message, with callback'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        my_callback = mock.MagicMock()
        transaction = {
            'callback': my_callback,
            'cb_params': 42,
            'foo': 'bar'
        }
        self.token_base.pop_transaction.return_value = transaction
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 0}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[0].encode())
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, 0)
        self.assertEqual(my_callback.call_count, 1)
        self.assertTupleEqual(my_callback.call_args[0], (42,))

    def test_process_single_imsi_missing_more_records(self):
        '''processing a single IMSI message, missing `more_records` attribute'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi", "tx": "aDR4MHI"}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[0].encode())
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, 0)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_process_existing_imsi(self, logger_mock):
        '''processing a single existing IMSI message'''
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 0}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[4].encode())
        self.my_updater.process(message)

        self.assertEqual(self.test_log.tip, 4)
        self.assertEqual(self.token_base.update_table.call_count, 0)
        self.assertFalse(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 0)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 0)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 0)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_process_multiple_imsi(self, logger_mock):
        '''processing a multiple IMSI message'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 0}\n'
                   + '\n'.join(test_data.SERIALISED_LOG_NO_TS
                               .split('\n')[:2]).encode())
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, 1)
        self.assertEqual(self.token_base.update_table.call_count, 0)
        self.assertFalse(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 1)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 1)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_process_multiple_imsi_with_more(self, logger_mock):
        '''processing a multiple IMSI message, with more to come'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 3}\n'
                   + '\n'.join(test_data.SERIALISED_LOG_NO_TS
                               .split('\n')[:2]).encode())
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, 1)
        self.assertEqual(self.token_base.update_table.call_count, 1)
        self.assertTrue(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 1)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 1)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.verifying_log.VerifyingLog.insert_many',
                autospec=True, side_effect=ValueError)
    def test_process_single_imsi_verification_error(self, insert_many_mock,
                                                    logger_mock):
        '''processing a single IMSI message, with verification error'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 3}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[4].encode())
        self.my_updater.process(message)

        self.assertEqual(insert_many_mock.call_count, 1)
        self.assertEqual(test_log.tip, None)
        self.assertEqual(self.token_base.update_table.call_count, 1)
        self.assertTrue(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 0)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 0)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    @mock.patch('whakakapi.verifying_log.VerifyingLog.insert_many',
                autospec=True, side_effect=ValueError)
    def test_process_single_imsi_callback_verification_error(self,
                                                             insert_many_mock,
                                                             logger_mock):
        '''processing a single IMSI message, callback, with verification error'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        my_callback = mock.MagicMock()
        transaction = {
            'callback': my_callback,
            'cb_params': 42,
            'foo': 'bar'
        }
        self.token_base.pop_transaction.return_value = transaction
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 3}\n'
                   + test_data.SERIALISED_LOG_NO_TS.split('\n')[4].encode())
        self.my_updater.process(message)

        self.assertEqual(insert_many_mock.call_count, 1)
        self.assertEqual(test_log.tip, None)
        self.assertEqual(self.token_base.update_table.call_count, 0)
        self.assertTrue(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 0)
        self.assertEqual(logger_mock.warning.call_count, 1)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 0)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 0)
        self.assertEqual(my_callback.call_count, 0)

    @mock.patch('whakakapi.worker.logger', autospec=True)
    def test_process_single_imsi_out_of_sequence(self, logger_mock):
        '''processing a single out-of-sequence IMSI message'''
        test_log = VerifyingLog()
        self.my_updater.token_table = test_log
        self.token_base.update_pending = True
        self.token_base.pop_transaction.return_value = None
        message = (b'aGFsOTAwMA: {"request_for": "imsi",'
                   b' "tx": "aDR4MHI", "more_records": 3}\n'
                   b'[42, "NTMwMDUwMDAwMDAwMDAw", "WwLN9KHM3uW9p9zPpAAS1Q==",'
                   b' "RklgOPDR7kuMHrkWNoOaWbDW4UuueTN7cFl1mNr/78k="]\n')
        self.my_updater.process(message)

        self.assertEqual(test_log.tip, None)
        self.assertEqual(self.token_base.update_table.call_count, 1)
        self.assertTrue(self.token_base.update_pending)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.warning.call_count, 0)
        self.assertEqual(self.token_base.cleanup_expired_transactions.call_count, 0)
        self.assertEqual(self.token_base.catch_up_processing.call_count, 0)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
