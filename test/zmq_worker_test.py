# -*- coding: utf-8 -*-
"""
Tests for the zmq_worker module.
"""

# Created: 2017-08-14 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

from unittest import mock
import unittest

from whakakapi.config import config
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.zmq_worker import ZmqRecordsSink
from whakakapi.zmq_worker import ZmqRecordsWorker
from whakakapi.zmq_worker import ZmqTokenBase
from whakakapi.zmq_worker import ZmqTokenUpdater


END_POINT = 'tcp://127.0.0.1:4711'
BASE_HEADER_OFFSET = len(config.token_base_topic) + 2


@mock.patch('builtins.super', autospec=True)
@mock.patch('whakakapi.zmq_worker.zmq_comms.Pusher', autospec=True)
class ZmqRecordsSinkTest(unittest.TestCase):
    """Testing the ZmqRecordsSink class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_constructor(self, PusherMock, super_mock):
        '''constructor'''
        _ = ZmqRecordsSink(END_POINT)
        self.assertEqual(super_mock.call_count, 1)
        self.assertEqual(PusherMock.call_count, 1)
        self.assertEqual(PusherMock.call_args[0][0], END_POINT)

    def test_produce_impl(self, _1, _2):
        '''request a message to the sink'''
        sink = ZmqRecordsSink(END_POINT)
        my_message = ManagedMessage(header={'from': 'me'}, body=b'hello world')
        sink.request_impl(my_message)
        self.assertEqual(sink._sink.request.call_count, 1)
        self.assertEqual(sink._sink.request.call_args[0][0],
                         b'update: {"from": "me"}\nhello world')


@mock.patch('builtins.super', autospec=True)
@mock.patch('whakakapi.zmq_worker.zmq_comms.Pusher', autospec=True)
class ZmqTokenBaseTest(unittest.TestCase):
    """Testing the ZmqTokenBase class."""

    test_log = None

    def setUp(self):
        self.test_log = VerifyingLog()

    def test_constructor(self, PusherMock, super_mock):
        '''constructor'''
        _ = ZmqTokenBase(END_POINT, self.test_log)
        self.assertEqual(super_mock.call_count, 1)
        self.assertEqual(PusherMock.call_count, 1)
        self.assertEqual(PusherMock.call_args[0][0], END_POINT)

    def test_produce_impl_async(self, _1, _2):
        '''request a message to the base'''
        base = ZmqTokenBase(END_POINT, self.test_log)
        my_message = ManagedMessage(header={'from': 'me'}, body=b'hello world')
        base.request_impl_async(my_message)
        self.assertEqual(base._base.request.call_count, 1)
        self.assertEqual(base._base.request.call_args[0][0],
                         b'update: {"from": "me"}\nhello world')


@mock.patch('builtins.super', autospec=True)
@mock.patch('whakakapi.zmq_worker.zmq_comms.Puller', autospec=True)
class ZmqRecordsWorkerTest(unittest.TestCase):
    """Testing the ZmqRecordsWorker class."""

    test_log = None

    @mock.patch('whakakapi.worker.RecordsSink', autospec=True)
    @mock.patch('whakakapi.worker.TokenBase', autospec=True)
    def setUp(self, TokenBaseMock, RecordsSinkMock):
        self.test_log = VerifyingLog()
        self.token_base = TokenBaseMock(self.test_log)
        self.records_sink = RecordsSinkMock()

    def test_constructor(self, PullerMock, super_mock):
        '''constructor'''
        _ = ZmqRecordsWorker(END_POINT, self.test_log,
                             self.records_sink, self.token_base)
        self.assertEqual(super_mock.call_count, 1)
        self.assertEqual(PullerMock.call_count, 1)
        self.assertEqual(PullerMock.call_args[0][0], END_POINT)


@mock.patch('builtins.super', autospec=True)
@mock.patch('whakakapi.zmq_worker.zmq_comms.Subscriber', autospec=True)
class ZmqTokenUpdaterTest(unittest.TestCase):
    """Testing the ZmqTokenUpdater class."""

    test_log = None

    @mock.patch('whakakapi.worker.TokenBase', autospec=True)
    def setUp(self, TokenBaseMock):
        self.test_log = VerifyingLog()
        self.token_base = TokenBaseMock(self.test_log)

    def test_constructor(self, SubscriberMock, super_mock):
        '''constructor'''
        _ = ZmqTokenUpdater(END_POINT, b'aGFsOTAwMA',
                            self.test_log, self.token_base)
        self.assertEqual(super_mock.call_count, 1)
        self.assertEqual(SubscriberMock.call_count, 1)
        self.assertEqual(SubscriberMock.call_args[0][0], END_POINT)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
