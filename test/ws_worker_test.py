# -*- coding: utf-8 -*-
"""
Tests for the ws_worker module.
"""

# Created: 2017-11-15 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import copy
from unittest import mock
import unittest

import requests

from test_data import SERIALISED_LOG, DESERIALISED_LOG
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.ws_worker import WsTokenBase


@mock.patch('whakakapi.ws_worker._get_session', autospec=True,
            return_value=mock.create_autospec(spec=requests.Session))
class WsTokenBaseTest(unittest.TestCase):
    """Testing the WsTokenBase class."""

    test_log = None
    my_base = None
    ok_response = None

    def setUp(self):
        self.test_log = VerifyingLog()
        # Load first two entries only.
        self.test_log.insert_many(DESERIALISED_LOG[:2])
        self.my_base = WsTokenBase(self.test_log)
        self.ok_response = mock.MagicMock(spec=requests.models.Response)
        self.ok_response.status_code = 200
        self.ok_response.headers = {'X-More-Records': '0'}

    def test__request_token_existing(self, get_session_mock):
        '''request existing token from WS'''
        self.ok_response.content = SERIALISED_LOG.split('\n')[1].encode()
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = self.ok_response
        result = self.my_base._request_token(b'530050000000001', 5)
        self.assertEqual(result, DESERIALISED_LOG[1][2])
        self.assertEqual(self.test_log.tip, 1)
        self.assertEqual(post_mock.call_count, 1)

    @mock.patch('whakakapi.ws_worker.logger', autospec=True)
    def test__request_token_error(self, logger_mock, get_session_mock):
        '''request error for token from WS'''
        error_response = mock.MagicMock(spec=requests.models.Response)
        error_response.status_code = 404
        error_response.content = 'Not Found'
        error_response.url = 'http://localhost:5000/mapping/YnJhdGFwZmVs'
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = error_response
        self.assertRaises(SystemExit, self.my_base._request_token,
                          b'bratapfel', 5)
        self.assertEqual(post_mock.call_count, 1)
        self.assertEqual(logger_mock.error.call_count, 1)
        self.assertEqual(logger_mock.error.call_args[0][0],
                         'Could not fetch token from token base for URL'
                         ' http://localhost:5000/mapping/YnJhdGFwZmVs'
                         ' with HTTP Error 404: Not Found')

    def test__request_token_new(self, get_session_mock):
        '''request new token from WS'''
        self.ok_response.content = SERIALISED_LOG.split('\n')[2].encode()
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = self.ok_response
        result = self.my_base._request_token(b'530050000000002', 5)
        self.assertEqual(result, DESERIALISED_LOG[2][2])
        self.assertEqual(self.test_log.tip, 2)
        self.assertEqual(post_mock.call_count, 1)

    @mock.patch('os.getpid', autospec=True)
    def test__request_token_skipped(self, getpid_mock, get_session_mock):
        '''request new token from WS, skipped one ahead'''
        self.ok_response.content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = self.ok_response
        getpid_mock.return_value = 42
        self.my_base.counter = 2
        result = self.my_base._request_token(b'530050000000003', 5)
        self.assertEqual(result, DESERIALISED_LOG[3][2])
        self.assertEqual(self.test_log.tip, 3)
        self.assertEqual(post_mock.call_count, 1)
        self.assertEqual(
            post_mock.call_args_list[0],
            mock.call('http://localhost:5000/mapping/NTMwMDUwMDAwMDAwMDAz',
                      params={'requester_tip': 1,
                              'limit': 5,
                              'rem': 'token count 2 on 42'}))

    @mock.patch('whakakapi.ws_worker.config.ws_worker_request_remark', False)
    def test__request_token_empty_log(self, get_session_mock):
        '''request new token from WS, on empty log'''
        self.my_base.token_table = VerifyingLog()
        responses = [copy.deepcopy(self.ok_response),
                     copy.deepcopy(self.ok_response),
                     copy.deepcopy(self.ok_response)]
        responses[0].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[0:2]).encode())
        responses[0].headers = {'X-More-Records': '3'}
        responses[1].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        responses[1].headers = {'X-More-Records': '1'}
        responses[2].content = SERIALISED_LOG.split('\n')[4].encode()
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = responses[0]
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = responses[1:]
        result = self.my_base._request_token(b'530050000000004', 2)
        self.assertEqual(result, DESERIALISED_LOG[4][2])
        self.assertEqual(self.my_base.token_table.tip, 4)
        self.assertEqual(post_mock.call_count, 1)
        self.assertEqual(get_mock.call_count, 2)
        self.assertEqual(
            post_mock.call_args_list[0],
            mock.call('http://localhost:5000/mapping/NTMwMDUwMDAwMDAwMDA0',
                      params={'requester_tip': None, 'limit': 2}))
        self.assertEqual(
            get_mock.call_args_list[0],
            mock.call('http://localhost:5000/mapping',
                      params={'requester_tip': 1, 'limit': 2}))

    def test__request_token_multi_skipped(self, get_session_mock):
        '''request new token from WS, skipped multiple ahead'''
        responses = [copy.deepcopy(self.ok_response),
                     copy.deepcopy(self.ok_response)]
        responses[0].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        responses[0].headers = {'X-More-Records': '1'}
        responses[1].content = SERIALISED_LOG.split('\n')[4].encode()
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = responses[0]
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = None
        get_mock.return_value = responses[1]
        result = self.my_base._request_token(b'530050000000004', 2)
        self.assertEqual(result, DESERIALISED_LOG[4][2])
        self.assertEqual(self.test_log.tip, 4)
        self.assertEqual(post_mock.call_count, 1)
        self.assertEqual(get_mock.call_count, 1)

    @mock.patch('whakakapi.ws_worker.logger', autospec=True)
    def test__request_token_multi_error(self, logger_mock, get_session_mock):
        '''request new token from WS, error on fetching further'''
        error_response = mock.MagicMock(spec=requests.models.Response)
        error_response.status_code = 404
        error_response.content = 'Not Found'
        responses = [copy.deepcopy(self.ok_response), error_response]
        responses[0].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        responses[0].headers = {'X-More-Records': '1'}
        post_mock = get_session_mock.return_value.post
        post_mock.reset_mock()
        post_mock.return_value = responses[0]
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = None
        get_mock.return_value = responses[1]
        self.assertRaises(SystemExit, self.my_base._request_token,
                          b'530050000000004', 2)
        self.assertEqual(self.test_log.tip, 3)
        self.assertEqual(post_mock.call_count, 1)
        self.assertEqual(get_mock.call_count, 1)
        self.assertEqual(logger_mock.error.call_count, 1)
        self.assertEqual(logger_mock.error.call_args[0][0],
                         'Could not fetch batch of tokens from token base'
                         ' from tip 3 with HTTP Error 404: Not Found')

    def test__request_records_after(self, get_session_mock):
        '''request `records after` from WS'''
        self.ok_response.content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = None
        get_mock.return_value = self.ok_response
        result = self.my_base._request_records_after(2)

        self.assertListEqual([tuple(x) for x in result],
                             DESERIALISED_LOG[2:4])
        self.assertEqual(self.test_log.tip, 3)
        self.assertEqual(get_mock.call_count, 1)
        self.assertEqual(
            get_mock.call_args_list[0],
            mock.call('http://localhost:5000/mapping',
                      params={'requester_tip': 1, 'limit': 2}))

    @mock.patch('whakakapi.ws_worker.logger', autospec=True)
    def test__request_records_after_error(self, logger_mock,
                                          get_session_mock):
        '''request `records after` from WS with HTTP error'''
        error_response = mock.MagicMock(spec=requests.models.Response)
        error_response.status_code = 404
        error_response.content = 'Not Found'
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = None
        get_mock.return_value = error_response
        self.assertRaises(SystemExit, self.my_base._request_records_after, 2)

        self.assertEqual(self.test_log.tip, 1)
        self.assertEqual(get_mock.call_count, 1)
        self.assertEqual(logger_mock.error.call_count, 1)
        self.assertEqual(logger_mock.error.call_args[0][0],
                         'Could not fetch batch of tokens from token base'
                         ' from tip 1 with HTTP Error 404: Not Found')

    def test__request_records_after_under_limit(self, get_session_mock):
        '''request `records after` from WS, less than given limit'''
        self.ok_response.content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:5]).encode())
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = None
        get_mock.return_value = self.ok_response
        result = self.my_base._request_records_after(5)

        self.assertListEqual([tuple(x) for x in result],
                             DESERIALISED_LOG[2:5])
        self.assertEqual(self.test_log.tip, 4)
        self.assertEqual(get_mock.call_count, 1)

    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_imsi(self, request_token_mock,
                                    request_records_after_mock, _):
        '''request an IMSI token synchronously'''
        body = {'request_for': 'imsi', 'imsi': 'abcd1234', 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        result = self.my_base.request_impl_sync(message)
        self.assertEqual(request_token_mock.call_count, 1)
        self.assertEqual(request_token_mock.call_args_list[0],
                         mock.call(self.my_base, b'i\xb7\x1d\xd7m\xf8', 5))
        self.assertEqual(request_records_after_mock.call_count, 0)
        self.assertEqual(result, request_token_mock.return_value)

    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_records_after(self, request_token_mock,
                                             request_records_after_mock, _):
        '''request 'records after' synchronously'''
        body = {'request_for': 'records_after',
                'requester_tip': 2, 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        result = self.my_base.request_impl_sync(message)
        self.assertEqual(request_token_mock.call_count, 0)
        self.assertEqual(request_records_after_mock.call_count, 1)
        self.assertEqual(request_records_after_mock.call_args_list[0],
                         mock.call(self.my_base, 5))
        self.assertEqual(result, request_records_after_mock.return_value)

    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_records_after',
                autospec=True)
    @mock.patch('whakakapi.ws_worker.WsTokenBase._request_token',
                autospec=True)
    def test_request_impl_sync_failed_op(self, request_token_mock,
                                         request_records_after_mock, _):
        '''request for an unsupported operation'''
        body = {'request_for': 'foo', 'limit': 5}
        header = {'tx': 'foo', 'from': self.my_base.worker_id.decode()}
        message = ManagedMessage(body=body, header=header)
        self.assertRaises(SystemExit,
                          self.my_base.request_impl_sync, message)
        self.assertEqual(request_token_mock.call_count, 0)
        self.assertEqual(request_records_after_mock.call_count, 0)

    @mock.patch('whakakapi.ws_worker.config.records_per_request_limit', 2)
    def test_update_table(self, get_session_mock):
        '''update token table via WS, on empty log'''
        self.my_base.token_table = VerifyingLog()
        responses = [copy.deepcopy(self.ok_response),
                     copy.deepcopy(self.ok_response),
                     copy.deepcopy(self.ok_response)]
        responses[0].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[0:2]).encode())
        responses[0].headers = {'X-More-Records': '3'}
        responses[1].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[2:4]).encode())
        responses[1].headers = {'X-More-Records': '1'}
        responses[2].content = SERIALISED_LOG.split('\n')[4].encode()
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = responses
        self.my_base.update_table()
        self.assertEqual(self.my_base.token_table.tip, 4)
        self.assertEqual(get_mock.call_count, 3)
        self.assertEqual(
            get_mock.call_args_list[0],
            mock.call('http://localhost:5000/mapping',
                      params={'requester_tip': None, 'limit': 2}))
        self.assertEqual(
            get_mock.call_args_list[1],
            mock.call('http://localhost:5000/mapping',
                      params={'requester_tip': 1, 'limit': 2}))
        self.assertEqual(
            get_mock.call_args_list[2],
            mock.call('http://localhost:5000/mapping',
                      params={'requester_tip': 3, 'limit': 2}))

    @mock.patch('whakakapi.ws_worker.config.records_per_request_limit', 2)
    @mock.patch('whakakapi.ws_worker.logger', autospec=True)
    def test_update_table_error(self, logger_mock, get_session_mock):
        '''update token table via WS, on empty log with error'''
        error_response = mock.MagicMock(spec=requests.models.Response)
        error_response.status_code = 500
        error_response.content = 'Internal Server Error'
        self.my_base.token_table = VerifyingLog()
        responses = [copy.deepcopy(self.ok_response), error_response]
        responses[0].content = (
            '\n'.join(SERIALISED_LOG.split('\n')[0:2]).encode())
        responses[0].headers = {'X-More-Records': '3'}
        get_mock = get_session_mock.return_value.get
        get_mock.reset_mock()
        get_mock.side_effect = responses
        self.assertRaises(SystemExit, self.my_base.update_table)
        self.assertEqual(self.my_base.token_table.tip, 1)
        self.assertEqual(get_mock.call_count, 2)
        self.assertEqual(logger_mock.error.call_args[0][0],
                         'Could not fetch batch of tokens from token base'
                         ' from tip 1 with HTTP Error 500:'
                         ' Internal Server Error')


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
