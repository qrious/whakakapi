# -*- coding: utf-8 -*-
"""
Tests for the zmq_producer module.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

from unittest import mock  # @UnusedImport
import unittest

from whakakapi.zmq_producer import MakeRecords


class MakeRecordsTest(unittest.TestCase):
    """Testing the MakeRecords class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch('whakakapi.zmq_producer.REPORTING_INTERVAL', 2)
    @mock.patch('whakakapi.zmq_producer.zmq_comms.Pusher', autospec=True)
    @mock.patch('whakakapi.zmq_producer.logger', autospec=True)
    def test_produce(self, logger_mock, PusherMock):
        '''producer vanilla'''
        record_maker = MakeRecords('tcp://127.0.0.1:4711')
        self.assertEqual(PusherMock.call_count, 1)
        record_maker.request(b'foo')
        self.assertEqual(record_maker._sink.request.call_count, 1)
        self.assertEqual(logger_mock.info.call_count, 0)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(record_maker.counter, 1)
        record_maker.request(b'bar')
        self.assertEqual(record_maker._sink.request.call_count, 2)
        self.assertEqual(logger_mock.info.call_count, 1)
        self.assertEqual(logger_mock.debug.call_count, 1)
        self.assertEqual(record_maker.counter, 2)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
