# -*- coding: utf-8 -*-
"""
Tests for the messages module.
"""

# Created: 2017-08-11 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import json
import unittest

from test_data import REQUEST_BODY_IMSI
from test_data import REQUEST_HEADER
from test_data import REQUEST_MESSAGE_IMSI
from test_data import WORKER_ID
from whakakapi.messages import ManagedMessage
from whakakapi.messages import standard_b64decode


class ModuleTest(unittest.TestCase):
    """Testing the messages module functions."""

    def test_standard_b64decode(self):
        '''base64 decoding various values'''
        tests = ['Zm9v', 'Zm8=', 'Zg==', 'YmFy', '',
                 b'Zm9v', b'Zm8=', b'Zg==', b'YmFy', b'',
                 'ZFJMIoYAPWNDOrWCdZrxiQ==',
                 b'ZFJMIoYAPWNDOrWCdZrxiQ==']
        expected = [b'foo', b'fo', b'f', b'bar', b'',
                    b'foo', b'fo', b'f', b'bar', b'',
                    b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89',
                    b'dRL"\x86\x00=cC:\xb5\x82u\x9a\xf1\x89']
        for test, check in zip(tests, expected):
            self.assertEqual(standard_b64decode(test), check)


class ManagedMessageTest(unittest.TestCase):
    """Testing the ManagedMessage class."""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_constructor_all_attributes(self):
        '''constructor with all individual attributes'''
        message = ManagedMessage(topic=WORKER_ID,
                                 header=REQUEST_HEADER,
                                 body=REQUEST_BODY_IMSI)
        self.assertEqual(message.topic, WORKER_ID)
        self.assertDictEqual(message.header, REQUEST_HEADER)
        self.assertEqual(message.body, REQUEST_BODY_IMSI)
        self.assertEqual(len(message.message), len(REQUEST_MESSAGE_IMSI))

    def test_constructor_empty_topic(self):
        '''constructor with individual attributes and an empty topic'''
        message = ManagedMessage(topic=b'',
                                 header=REQUEST_HEADER,
                                 body=REQUEST_BODY_IMSI)
        self.assertEqual(message.topic, b'')
        self.assertDictEqual(message.header, REQUEST_HEADER)
        self.assertDictEqual(message.body, REQUEST_BODY_IMSI)
        self.assert_(message.message.startswith(b'{'))

    def test_constructor_raw(self):
        '''constructor with raw content test'''
        message = ManagedMessage(raw=REQUEST_MESSAGE_IMSI)
        self.assertEqual(message.topic, WORKER_ID)
        self.assertDictEqual(message.header, REQUEST_HEADER)
        self.assertDictEqual(json.loads(message.body), REQUEST_BODY_IMSI)
        self.assertEqual(len(message.message), len(REQUEST_MESSAGE_IMSI))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
