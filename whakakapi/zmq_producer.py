# -*- coding: utf-8 -*-
"""
Module producing (mock) records to be tokenised.

IMSIs are taken as a 64 bit signed integer (converted from a big endian 64 bit
byte sequence).

NZ Spark IMSIs are of the format '53005{:010d}'.format(i)
"""

# Created: 2017-05-01 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging
import math
import nacl.utils

import whakakapi.zmq_communicator as zmq_comms


IMSI_LENGTH_BITS = 64
REPORTING_INTERVAL = 5000
RANDOM_ROOT = b'\xab\xde\x14\x98\x1b1\xe9P'

logger = logging.getLogger(__name__)


class ImsiFaker(object):
    """
    Produces a fake IMSI value as a 64 bit signed integer.

    The 64 bits are composed of a random sequence of bits to suit the
    approximate number of devices, and as a remainder a fixed number of
    (random) root bits.
    """
    root_bytes = b''
    device_bytes_len = 0
    device_right_mask = 0

    def __init__(self, no_devices):
        """
        Constructs the object with the invariable parameters to construct an
        IMSI.

        :param no_devices: Appxoximate number of devices to simulate. Will be
            rounded up to the next power of 2.
        """
        device_bits_len = math.ceil(math.log2(no_devices))
        root_bits_len = IMSI_LENGTH_BITS - device_bits_len
        root_bytes_len = math.ceil(root_bits_len / 8.0)
        left_root_mask = 0xff >> (8 - (root_bits_len % 8))
        self.root_bytes = bytearray(RANDOM_ROOT[:root_bytes_len])
        self.root_bytes[0] &= left_root_mask
        self.device_bytes_len = math.ceil(device_bits_len / 8.0)
        self.device_right_mask = ~left_root_mask & 0xff

    def request(self) -> int:
        """
        Produces a new IMSI value.

        :return: IMSI in signed integer format.
        """
        imsi_bytes = bytearray(nacl.utils.random(self.device_bytes_len))
        imsi_bytes[-1] &= self.device_right_mask
        imsi_bytes[-1] |= self.root_bytes[0]
        imsi_bytes += self.root_bytes[1:]
        return int.from_bytes(imsi_bytes, byteorder='big', signed=True)


class MakeRecords(object):
    """
    Producer of (mock) records (beginning of pipeline).
    """
    counter = 0
    """Internal counter for number of produced records."""
    _sink = None  # type: zmq_comms.Pusher
    """ZeroMQ sink for outgoing pipeline."""

    def __init__(self, end_point: str, bind: bool=True):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: True).
        """
        self._sink = zmq_comms.Pusher(end_point, bind=bind)

    def request(self, message: bytes):
        """
        Sends (produces) a message to be sent onwards in the pipeline.

        :param message: Binary message object (unmanaged, plain JSON content).
        """
        self._sink.request(message)
        self.counter += 1
        if self.counter % REPORTING_INTERVAL == 0:
            logger.info('Sent {}: {}'.format(self.counter, message))
        else:
            logger.debug('Sent {}: {}'.format(self.counter, message))
