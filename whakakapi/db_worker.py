# -*- coding: utf-8 -*-
"""
Module providing a worker communicating via PostgreSQL to provide tokeniseation
services.
"""

# Created: 2017-10-30 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging
import sys
from typing import Union, Iterable, List

import psycopg2

from whakakapi import worker
from whakakapi.config import config
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.verifying_log import deserialisable_value
from whakakapi.verifying_log import tip_to_integer

# DB table columns: idx, imsi, token, hashcode, ts
# Note: DB sequencing starts with 1 (not 0), thus a dummy entry for 0 has
#       to be present in the table.
INDIVIDUAL_SQL_QUERY = ('SELECT idx, imsi, token, hashcode'
                        '    FROM qr_get_or_create_token($1)')
INDIVIDUAL_PLAN = 'individual_plan'
INDIVIDUAL_PLAN_EXECUTE = 'EXECUTE individual_plan (%s)'
BATCH_SQL_QUERY = ('SELECT idx, imsi, token, hashcode'
                   '    FROM tokeniser.imsi_tokens'
                   '    WHERE idx > $1'
                   '    ORDER BY idx'
                   '    LIMIT $2')
BATCH_PLAN = 'batch_plan'
BATCH_PLAN_EXECUTE = 'EXECUTE batch_plan (%s, %s)'
logger = logging.getLogger(__name__)


def record_transform(records: Union[int, bytes]) -> Iterable[List[Union[int, bytes]]]:
    """Transformation generator."""
    formats = (int, bytes, bytes, bytes, int)
    for record in records:
        yield tuple(the_format(value)
                    for the_format, value in zip(formats, record))


class DbTokenBase(worker.TokenBase):
    """
    Proxy object to manage all interactions with the token base service using
    a PostgreSQL token base.
    """

    _db = None  # type: zmq_comms.Pusher
    """Database connection."""

    def __init__(self, token_table: VerifyingLog):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param token_table: Token lookup table (append only log).
        """
        super().__init__(token_table)
        self._db = psycopg2.connect(database=config.token_db_name,
                                    user=config.token_db_user,
                                    host=config.token_db_host,
                                    port=config.token_db_port,
                                    password=config.token_db_password)
        cursor = self._db.cursor()
        # Prepare some statements (plan) for swifter execution.
        cursor.execute('PREPARE {} AS {}'
                       .format(INDIVIDUAL_PLAN, INDIVIDUAL_SQL_QUERY))
        cursor.execute('PREPARE {} AS {}'
                       .format(BATCH_PLAN, BATCH_SQL_QUERY))
        self._db.commit()
        cursor.close()

    def _request_token(self, imsi: bytes, limit: int) -> bytes:
        """
        Request a single token from the DB. If the index of this token is
        beyond the next one from the current token table's tip, the missing
        ones will be retrieved and added to the token table.

        :param imsi: IMSI to request a token for.
        :param limit: Maximum number of records to return.
        :return: Token for the requested IMSI.
        """
        self._db.commit()
        self._db.autocommit = True
        cursor = self._db.cursor()
        cursor.execute(INDIVIDUAL_PLAN_EXECUTE, (imsi,))
        records = cursor.fetchall()
        # Columns: idx, imsi, token, hashcode, ts
        new_tip, token = records[0][0], bytes(records[0][2])
        old_tip = tip_to_integer(self.token_table.tip)
        if new_tip == old_tip + 1:
            self.token_table.insert_many(record_transform(records))
            old_tip = tip_to_integer(self.token_table.tip)
        while new_tip > old_tip:
            # Fetch stuff from current tip to update local cache until
            # we've got them all.
            cursor.execute(BATCH_PLAN_EXECUTE, (old_tip, limit))
            records = cursor.fetchall()
            self.token_table.insert_many(record_transform(records))
            old_tip = tip_to_integer(self.token_table.tip)
        cursor.close()
        return token

    def _request_records_after(self, requester_tip: int, limit: int) -> Iterable:
        """
        Request a range of token records from the DB. The retrieved records
        will be added to the token table.

        :param requester_tip: Index after which records are to be returned.
        :param limit: Maximum number of records to return.
        :return: Iterable of records.
        """
        cursor = self._db.cursor()
        cursor.execute(BATCH_PLAN_EXECUTE, (requester_tip + 1, limit))
        records = cursor.fetchall()
        self.token_table.insert_many(record_transform(records))

        cursor.close()
        return record_transform(records)

    def request_impl_sync(self, message: ManagedMessage) -> Union[bytes, Iterable]:
        """
        To produce (send) an outgoing request message synchronously.

        Note: This method already inserts all retrieved records into the
              local token table.

        :param message: Message defining the token base service inquiry.
        :return: Token for the requested IMSI on IMSI requests, on
            'range after' requests an iterable of token table records.
        """
        # Extract some key parameters from the message.
        request_for = message.body['request_for']
        limit = message.body.get('limit', config.records_per_request_limit)
        if request_for == 'imsi':
            imsi = deserialisable_value(message.body.get('imsi'))
            return self._request_token(imsi, limit)
        elif request_for == 'records_after':
            requester_tip = message.body['requester_tip']
            return self._request_records_after(requester_tip, limit)
        else:
            logger.error('Got unknown DB token base request: {}!'
                         .format(request_for))
            sys.exit(1)

    def update_table(self):
        """
        Update the token table.
        """
        cursor = self._db.cursor()
        limit = config.records_per_request_limit
        records = ['dummy']
        while records:
            # Fetch stuff from current tip to update local cache until
            # we've got them all.
            old_tip = tip_to_integer(self.token_table.tip)
            cursor.execute(BATCH_PLAN_EXECUTE, (old_tip, limit))
            records = cursor.fetchall()
            self.token_table.insert_many(record_transform(records))

        cursor.close()
