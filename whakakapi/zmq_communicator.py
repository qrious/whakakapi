# -*- coding: utf-8 -*-
"""
Module providing ZeroMQ communication services between workers, base logs and
source/sink components in the tokenisation chain.
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging
from typing import Callable
import zmq

from whakakapi.config import config

logger = logging.getLogger(__name__)
_zmq_context = None


def get_zmq_context():
    """
    Returns a (singleton) ZeroMQ context object.

    :return: {zmq.Context}
    """
    global _zmq_context
    if _zmq_context is None or _zmq_context.closed:
        _zmq_context = zmq.Context()
    return _zmq_context


class ZmqCommunicator(object):
    """
    ZeroMQ communicator base class.
    """
    _socket = None
    _end_point = None
    _message_prefix = None

    def __init__(self, end_point: str):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        """
        self._end_point = end_point

    def _split_message(self, message: bytes) -> (bytes, bytes, bytes):
        """
        Splits a raw binary message into a topic, header and body component.

        :param message: Raw incoming message.
        :return: Topic, header and body component as byte strings.
        """
        header_sep_index = message.index(b'\n')
        message_header = message[:header_sep_index]
        message_body = message[header_sep_index + 1:]
        topic = None
        if b':' in message_header:
            index = message.index(b':')
            topic = message[:index]
            message_header = message[index + 2:]
        return topic, message_header, message_body

    def _assemble_message(self,
                          message_body: bytes,
                          message_header: bytes=None,
                          topic: bytes=None) -> bytes:
        """
        Assembles a message from its components (topic, header and body) into
        a byte string wire message.

        :param message_body: Byte string body.
        :param message_header: Byte string header.
        :param topic: Byte string topic.
        :return: Wire message byte string.
        """
        message_prefix = topic if topic else self._message_prefix
        message = []
        if message_header:
            message.append(message_header)
        message.append(message_body)
        message = b'\n'.joing(message)
        if message_prefix:
            return message_prefix + b': ' + message
        else:
            return message


class Publisher(ZmqCommunicator):
    """
    Publisher for broadcast messages.
    """

    def __init__(self, end_point: str, bind: bool=True):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: True).
        """
        super().__init__(end_point)
        self._socket = get_zmq_context().socket(zmq.PUB)
        if bind:
            self._socket.bind(self._end_point)
        else:
            self._socket.connect(self._end_point)
        self._message_prefix = config.update_topic

    def publish(self, raw_message: bytes):
        """
        To request (send) an outgoing message (not implemented).

        :param raw_message: Raw outgoing message.
        """
        logger.debug('zmq_comms.Publisher send: {}'.format(raw_message))
        self._socket.send(raw_message)


class Subscriber(ZmqCommunicator):
    """
    Subscriber for broadcast messages.
    """

    running = False
    """Boolean indicating whether the receiver event loop is running."""
    process_callback = None  # type: Callable
    """Callback function to process a received message."""

    def __init__(self, end_point: str, bind: bool=False,
                 subscribe_to: bytes=None, callback: Callable=None):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: False).
        :param subscribe_to: "Topic" to subscribe to.
        :param callback: Callback function to process a received message.
        """
        super().__init__(end_point)
        self._socket = get_zmq_context().socket(zmq.SUB)
        if bind:
            self._socket.bind(self._end_point)
        else:
            self._socket.connect(self._end_point)
        self._socket.setsockopt(zmq.SUBSCRIBE, config.update_topic)
        if subscribe_to:
            self._socket.setsockopt(zmq.SUBSCRIBE, subscribe_to)
        self._message_prefix = config.update_topic
        self.process_callback = callback

    def run(self):
        """
        Runs the message receiving loop.
        """
        self.running = True
        while self.running:
            try:
                raw_message = self._socket.recv()
                logger.debug('zmq_comms.Subscriber received: {}'
                             .format(raw_message))
                self.process_callback(raw_message)
            except (KeyboardInterrupt, zmq.ContextTerminated):
                self.running = False
                break


class Pusher(ZmqCommunicator):
    """
    The transmitting end of a pipeline communication path.
    """

    def __init__(self, end_point: str, bind: bool=True):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: True).
        """
        super().__init__(end_point)
        self._socket = get_zmq_context().socket(zmq.PUSH)
        if bind:
            self._socket.bind(self._end_point)
        else:
            self._socket.connect(self._end_point)

    def request(self, raw_message: bytes):
        """
        To request (send) an outgoing message (not implemented).

        :param raw_message: Raw outgoing message.
        """
        logger.debug('zmq_comms.Pusher send: {}'.format(raw_message))
        self._socket.send(raw_message)


class Puller(ZmqCommunicator):
    """
    The receiving end of a pipeline communication path.
    """

    running = False
    """Boolean indicating whether the receiver event loop is running."""
    process_callback = None  # type: Callable
    """Callback function to process a received message."""

    def __init__(self, end_point: str, bind: bool=False,
                 callback: Callable=None):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: False).
        :param callback: Callback function to process a received message.
        """
        super().__init__(end_point)
        self._socket = get_zmq_context().socket(zmq.PULL)
        if bind:
            self._socket.bind(self._end_point)
        else:
            self._socket.connect(self._end_point)
        self.process_callback = callback

    def run(self):
        """
        Runs the message receiving loop.
        """
        self.running = True
        while self.running:
            try:
                raw_message = self._socket.recv()
                logger.debug('zmq_comms.Puller received: {}'
                             .format(raw_message))
                self.process_callback(raw_message)
            except (KeyboardInterrupt, zmq.ContextTerminated):
                self.running = False
                break
