# -*- coding: utf-8 -*-
"""
Synchronisation tool for mutual exclusion in threading.
"""

# Created: 2017-07-04 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'


def synchronized(method):
    """
    Synchronisation decorator adapted from examples given here:
    http://theorangeduck.com/page/synchronized-python

    Note: Using the `wrapt` package with the @synchronized decorator as
          described here is more elegant, but about 30% less performant:
          http://wrapt.readthedocs.io/en/latest/examples.html
    """
    def sync_method(self, *args, **kwargs):
        with self._lock:
            return method(self, *args, **kwargs)

    return sync_method
