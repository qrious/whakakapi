#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a stand-alone tokenisation worker
using a RESTful Web Service as a token base.
"""

# Created: 2017-11-03 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import json
import logging
import random
import time

from whakakapi import verifying_log
from whakakapi.config import config
from whakakapi.messages import standard_b64decode
from whakakapi.worker import RecordsWorker
from whakakapi.ws_worker import WsTokenBase
from whakakapi.zmq_producer import ImsiFaker


OUT_FILE = 'output.csv'
MESSAGES = 1000000
REPORTING_INTERVAL = 10  # In seconds.
NO_DEVICES = 500000
AVG_TIME_PER_RECORD = 0.0  # 0.0001

logger = logging.getLogger(__name__)


class FileRecordsSink(object):
    """
    Outgoing end of the worker pipeline writing to a CSV file.
    """
    _out_file = None  # type: io.FileIO
    """CSV output file."""

    def __init__(self, out_file_name: str):
        """
        Constructor.

        :param out_file_name: Output file name.
        """
        self._out_file = open(out_file_name, 'at')

    def write(self, record):
        """
        To request (send) an outgoing message.

        :param message: Outgoing record.
        """
        self._out_file.write('{}\n'.format(json.dumps(record)))


def main(no_messages: int):
    """
    Set up everything needed and start producing (mock) records.

    :param no_messages: Number of messages to request.
    """
    # Plumbing for (auto-updated) token table.
    logger.info('Making token table.')
    token_table = verifying_log.VerifyingLog()
    logger.info('Making token base.')
    token_base = WsTokenBase(token_table)
    logger.info('Updating token base.')
    token_base.update_table()
    logger.info('Making token sink.')
    records_sink = FileRecordsSink(OUT_FILE)
    worker = RecordsWorker(token_table, records_sink, token_base,
                           synchronous=True)

    # Our tokenisation worker, with access to token service.
    logger.info('Making token worker.')

    logger.info('Producing (fake) records.')
    my_imsi_faker = ImsiFaker(NO_DEVICES)
    interval_start_time = time.time()
    for counter in range(no_messages):
        imsi = my_imsi_faker.request()
        # For NZ Spark style 'real' IMSIs:
        # '53005{:010d}'.format(random.randrange(0, NO_DEVICES)
        data = {config.imsi_record_name: imsi, 'lat': 3.141, 'long': 42.123}
        result = worker.process(data)
        token = standard_b64decode(result[config.imsi_record_name])
        token_decimal = int.from_bytes(token, byteorder='big', signed=True)
        result[config.imsi_record_name] = token_decimal
        records_sink.write(result)
        time.sleep(random.random() * 2 * AVG_TIME_PER_RECORD)
        if time.time() - interval_start_time > REPORTING_INTERVAL:
            logger.info('Sent {}: {}'.format(counter, data))
            interval_start_time = time.time()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(asctime)s %(message)s')
    main(MESSAGES)
