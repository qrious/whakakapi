#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a stand-alone tokenisation worker
using a RESTful Web Service as a token base. Records are read using PyHive
from a source table and written to a destination table again via PyHive.

Note: This tool requires the Qrious `qat` library for operation on the Hive
      tables.
"""

# Created: 2018-05-07 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017-2018 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import argparse
import logging
import time
from typing import Iterable, Tuple

import qat
from whakakapi import verifying_log
from whakakapi.config import config
from whakakapi.messages import standard_b64decode
from whakakapi.worker import RecordsWorker
from whakakapi.ws_worker import WsTokenBase


REPORTING_INTERVAL = 60  # In seconds.
HIVE_MAX_GULP_RECORDS = 100000
HIVE_MAX_WRITE_RECORDS = 10000
VERBOSE_HIVE = False

logger = logging.getLogger(__name__)


class HiveRecordsSink(object):
    """
    Outgoing end of the worker pipeline writing to a Hive table.
    """
    _out_table_name = ''
    """Hive output table name."""
    _out_records = None  # # type: list
    """Buffer of the accumulated records to write."""
    buffer_count = 0
    """Number of elements in the write buffer."""

    def __init__(self, out_table_name: str, source_table_name: str):
        """
        Constructor.

        :param out_table_name: Name of the output table.
        :param source_table_name: Name of the source table.
        """
        self._out_table_name = out_table_name
        self._out_records = []
        self.buffer_count = 0
        logger.info('Creating target table "{}".'.format(out_table_name))
        with qat.db.HiveDblink(engine=qat.db.HiveDblink.MR) as dblink:
            con, cur = dblink.con, dblink.cur
            sql = ('CREATE TABLE {dest} LIKE {source}'
                   .format(source=source_table_name,
                           dest=self._out_table_name))
            cur.execute(sql, async=VERBOSE_HIVE)
            if VERBOSE_HIVE:
                dblink.prog()
            con.commit()

    def write(self, record: Iterable):
        """
        To record an outgoing (tokenised) record.

        :param record: Outgoing record with all fields/column values.
        """
        self._out_records.append(record)
        self.buffer_count += 1
        if self.buffer_count >= HIVE_MAX_WRITE_RECORDS:
            self.flush()

    def flush(self):
        """
        Flushes buffered write content to the output.
        """
        if not self.buffer_count:
            return
        logger.debug('Flush write {} records to Hive.'
                     .format(self.buffer_count))
        with qat.db.HiveDblink(engine=qat.db.HiveDblink.MR) as dblink:
            con, cur = dblink.con, dblink.cur
            sql_vals_clause, args = qat.db.Hive.rows_to_insert_dets(
                rows=self._out_records)
            sql = ('INSERT INTO {table} VALUES {clause}'
                   .format(table=self._out_table_name, clause=sql_vals_clause))
            cur.execute(sql, args, async=VERBOSE_HIVE)
            if VERBOSE_HIVE:
                dblink.prog()
            con.commit()
        # Now reset the buffer.
        self._out_records = []
        self.buffer_count = 0


class HiveTableTokeniser(object):
    """
    Tokenises an entire Hive source table with an IMSI column to an equivalent
    output Hive table in a batch using PyHive.
    """

    _token_table = None  # type: verifying_log.VerifyingLog
    _token_base = None  # type: WsTokenBase
    _records_sink = None  # type: FileRecordsSink
    _worker = None  # type: RecordsWorker
    source_table = ''
    dest_table = ''
    column_names = None  # type: Iterable[str]
    imsi_index = 0

    def __init__(self, source_table: str, dest_table: str, imsi_column: str):
        """
        Tokeniser for Hive tables.

        Creates a copy of the table with the IMSI field replaced by a tokenised
        value.

        :param source_table: Hive table to read untokenised records from.
        :param dest_table: New Hive table to write tokenised records to.
        :param imsi_column: Name of the column containing the IMSI
            (to tokenise).
        """
        self.source_table = source_table
        self.dest_table = dest_table

        # Get the column names.
        self.column_names = [name for name, _ in self._get_column_details()]
        self.imsi_index = self.column_names.index(imsi_column)

        # Plumbing for (auto-updated) token table.
        logger.info('Making token table.')
        self._token_table = verifying_log.VerifyingLog()
        logger.info('Making token base.')
        self._token_base = WsTokenBase(self._token_table)
        logger.info('Updating token base.')
        self._token_base.update_table()
        logger.info('Making token sink.')
        self._records_sink = HiveRecordsSink(self.dest_table,
                                             self.source_table)
        logger.info('Making token worker.')
        self._worker = RecordsWorker(self._token_table, self._records_sink,
                                     self._token_base, synchronous=True)

    def _get_column_details(self) -> Iterable[Tuple[str, str]]:
        """
        Automatically detects the data definition of the source table.

        :return: Iterable containing the column name and the (SQL) data type
            (as a tuple) for every column in the table.
        """
        with qat.db.HiveDblink(engine=qat.db.HiveDblink.SPARK) as dblink:
            cur = dblink.cur
            sql = "DESCRIBE {}".format(self.source_table)
            cur.execute(sql)
            res = cur.fetchall()
        column_details = []
        for column, data_type, extra in res:
            if extra is None:
                break
            column_details.append((column, data_type))
        return column_details

    def tokenise_table(self):
        """
        Creates a copy of the table with the IMSI field replaced by a tokenised
        value.
        """
        interval_start_time = time.time()
        counter = 0
        # First, let's find the index of the IMSI column.
        logger.info('Processing table records.')
        with qat.db.HiveDblink(engine=qat.db.HiveDblink.MR) as dblink:
            cur = dblink.cur
            sql = ('''SELECT {columns} FROM {source}'''
                   .format(columns=', '.join(self.column_names),
                           source=self.source_table))
            cur.execute(sql, async=VERBOSE_HIVE)
            if VERBOSE_HIVE:
                dblink.prog()
            running = True
            while running:
                # Fetch another gulp from the table.
                logger.debug('Reading {} records from Hive.'
                             .format(HIVE_MAX_GULP_RECORDS))
                rows = cur.fetchmany(HIVE_MAX_GULP_RECORDS)
                if not rows:
                    running = False
                    continue
                # Tokenise the records of the gulp.
                for record in rows:
                    data = {config.imsi_record_name: record[self.imsi_index]}
                    result = self._worker.process(data)
                    token = standard_b64decode(result[config.imsi_record_name])
                    token_decimal = int.from_bytes(
                        token, byteorder='big', signed=True)
                    record_out = list(record)
                    record_out[self.imsi_index] = token_decimal
                    self._records_sink.write(record_out)
                    counter += 1
                    if time.time() - interval_start_time >= REPORTING_INTERVAL:
                        logger.info('Tokenised {} records'.format(counter))
                        interval_start_time = time.time()
                self._records_sink.flush()
        logger.info('Finished tokenising table records from table'
                    ' "{}" to "{}".'
                    .format(self.source_table, self.dest_table))


def main(source_table: str, dest_table: str, imsi_column: str):
    """
    Set up everything needed and start producing (mock) records.

    :param source_table: Hive table to read untokenised records from.
    :param dest_table: New Hive table to write tokenised records to.
    :param imsi_column: Name of the column containing the IMSI (to tokenise).
    """
    tokeniser = HiveTableTokeniser(source_table, dest_table, imsi_column)
    tokeniser.tokenise_table()


def _console():
    """
    Entry point for console execution.
    """
    # Set up logger.
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(name)s\t%(asctime)s %(message)s')
    # Silence the PyHive logger.
    pyhive_logger = logging.getLogger('pyhive')
    pyhive_logger.setLevel(logging.WARNING)

    # Parse command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('source_table',
                        help='Path to the input table file to be tokenised')
    parser.add_argument('imsi_column',
                        help=('Name of the column with the IMSI'
                              ' to be tokenised'))
    parser.add_argument('dest_table',
                        help='Path to write tokenised table file to')
    args = parser.parse_args()

    imsi_column = args.imsi_column
    source_table = args.source_table
    dest_table = args.dest_table

    # Now let's do the work.
    main(source_table, dest_table, imsi_column)


if __name__ == '__main__':
    _console()
