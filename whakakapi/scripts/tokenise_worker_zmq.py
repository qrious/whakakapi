#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a ZeroMQ-based tokenisation worker.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging

from whakakapi import verifying_log
from whakakapi import zmq_communicator
from whakakapi.config import config
from whakakapi.zmq_worker import ZmqRecordsSink
from whakakapi.zmq_worker import ZmqRecordsWorker
from whakakapi.zmq_worker import ZmqTokenBase
from whakakapi.zmq_worker import ZmqTokenUpdater

logger = logging.getLogger(__name__)


def main():
    """
    Set up everything needed and start a fully assembled tokenisation worker
    node.
    """
    # Plumbing for (auto-updated) token table.
    token_table = verifying_log.VerifyingLog()
    token_base = ZmqTokenBase(config.service_queue, token_table)
    worker_id = token_base.worker_id
    token_updater = ZmqTokenUpdater(config.update_queue, worker_id,
                                    token_table, token_base)
    token_updater.run()

    # Our tokenisation worker, with access to token service.
    records_sink = ZmqRecordsSink(config.out_queue)
    consumer = ZmqRecordsWorker(config.in_queue, token_table,
                                records_sink, token_base)
    consumer.run()

    # When finishing, tidy up.
    # TODO: Check whether we can terminate the ZeroMQ context to break out.
    consumer.join()
    zmq_communicator.get_zmq_context().destroy(linger=None)
    token_updater.running = False
    token_updater.join()
    try:
        zmq_communicator.get_zmq_context().term()
    except KeyboardInterrupt:
        logger.info('Interrupted, shutting down.')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(asctime)s %(message)s')
    main()
