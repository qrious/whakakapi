Service Scripts
===============

Within the `scripts/` directory of this repository, a number of
executable codes are located. These are on the one hand services (such
as the token base services) as well as worker implementations. The
codes are of varying levels of maturity (fully tested and scalable
production, less tested or scalable or exemplary code).

Services and workers:

- `tokenise_base_ws.py` -- Token Base service using a RESTful Web
  Service.
- `tokenise_base_zmq.py` -- Token Base service using a ZeroMQ
  (REST-like) event service.
- `tokenise_worker_ws_pyhive.py` -- Worker component for batch
  tokenisation of entire tables (source to destination table).


Sample codes:

- `tokenise_worker_db_standalone.py` -- Worker with a dummy component
  for batch tokenisation using faked dummy IMSI records for testing
  scalability. Note: One **MUST NEVER** run more than one of these in
  parallel against a PostgrSQL-based Token Base.
- `tokenise_worker_ws.py` -- Worker with a dummy component for batch
  tokenisation using faked dummy IMSI records for testing scalability.
- `tokenise_worker_zmq.py` -- Worker component tokenising data
  received via a stream and sending it on via a stream. The stream is
  implemented usnig ZeroMQ.
- `tokenise_producer_zmq.py` -- Fake IMSI record producing component
  communicating with a ZeroMQ stream worker component.
- `tokenise_consumer_zmq.py` -- Consumer component for a ZeroMQ stream
  worker component.


Levels of maturity:

- `ws` -- Web Services codes: Well tested and very scalable.
- `zmq` -- ZeroMQ codes: Fully asynchronous event messaging. Less
  tested, some issues, but generally scalable. Needs further work for
  corner cases.
- `db` -- PostgreSQL database codes: Fully tested, but of poor
  scalability as well as with issues for concurrent operation due to
  issues of PostgreSQL with generating contiguous indexes on
  concurrently invoced queries.
