#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a Web Service-based tokenisation
base service.
"""

# Created: 2017-11-10 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging.handlers
import smtplib
import sys
import traceback

from whakakapi import verifying_log
from whakakapi import ws_base
from whakakapi.config import config

logger = logging.getLogger(__name__)

_token_table = None
_token_service = None


def _setup_logger():
    log_format = '%(levelname)s\t%(name)s\t%(asctime)s\t%(name)s: %(message)s'
    file_handler = logging.handlers.RotatingFileHandler(
        filename=config.log_file,
        maxBytes=config.log_max_bytes,
        backupCount=config.log_backup_count)
    formatter = logging.Formatter(log_format)
    file_handler.setFormatter(formatter)
    logging.basicConfig(level=config.log_level,
                        format=log_format,
                        handlers=[file_handler])


def _send_error_mail(message):
    """
    Sends an error mail notification to configured recipients.

    :param message: String representation of the message to send.
    """
    sender = config.mail_sender
    receiver = ', '.join(config.mail_notifications_to)
    headers = ('From: {}\n'
               'To: {}\n'
               'Subject: {}\n\n'
               .format(sender, receiver, config.mail_subject))
    server = smtplib.SMTP(config.smtp_server)
    server.sendmail(sender, receiver, headers + message)
    server.quit()


def main() -> ws_base.Flask:
    """
    Set up everything needed for a fully assembled tokenisation base service.

    :return: The Flask app.
    """
    global _token_table
    global _token_service
    _setup_logger()
    # Plumbing for file persisted token service.
    _token_table = verifying_log.VerifyingLog(config.token_log_filename)
    logger.info("Loaded {} token table entries, ready for rock'n'roll."
                .format(verifying_log.tip_to_integer(_token_table.tip) + 1))
    _token_service = ws_base.WsTokenService(_token_table)
    app = ws_base.make_app(_token_service)
    return app


def _console():
    """
    Entry point for console execution.
    """
    _setup_logger()

    try:
        app = main()
        app.run(host=config.ws_base_bind_host, port=config.ws_base_port)
    except Exception as e:
        stack_trace = traceback.format_exc()
        logger.error('Quitting, something went wrong:\n{}'
                     .format(e.args[0]))
        message = 'Stack trace:\n{}'.format(stack_trace)
        logger.error(message)
        _send_error_mail(message)
        sys.exit(1)


if __name__ == '__main__':
    _console()
