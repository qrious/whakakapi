#!/usr/bin/env python3
# $example on:spark_hive$
"""
Module providing an entry point to start a stand-alone tokenisation worker
using a RESTful Web Service as a token base. Records are processed using
PySpark (for Spark 2) from a source table and written to a destination table
again.
"""

# Created: 2018-05-14 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017-2018 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import argparse
import logging
import os

from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import LongType
from whakakapi import verifying_log
from whakakapi.config import config
from whakakapi.messages import standard_b64decode
from whakakapi.worker import RecordsWorker
from whakakapi.ws_worker import WsTokenBase


_record_workers = {}
logger = logging.getLogger(__name__)


class DummyRecordsSink(object):
    """Dummy."""


def get_record_worker() -> RecordsWorker:
    """
    Singleton registry to return a records worker instance per PID.
    """
    pid = os.getpid()
    if pid not in _record_workers:
        # Plumbing for (auto-updated) token table.
        logger.info('Making token table (PID {}).'.format(pid))
        token_table = verifying_log.VerifyingLog()
        logger.info('Making token base (PID {}).'.format(pid))
        token_base = WsTokenBase(token_table)
        logger.info('Updating token base (PID {}).'.format(pid))
        token_base.update_table()
        logger.info('Making token worker (PID {}).'.format(pid))
        records_sink = DummyRecordsSink()
        record_worker = RecordsWorker(token_table, records_sink, token_base,
                                      synchronous=True)
        cache_object = {
            'token_table': token_table,
            'token_base': token_base,
            'records_sink': records_sink,
            'records_worker': record_worker
        }
        _record_workers[pid] = cache_object
    return _record_workers[pid]['records_worker']


def imsi_to_token(imsi: int) -> int:
    """
    Returns the tokenised IMSI.

    :param imsi: Untokenised IMSI value as signed 64 bit integer.
    :return: Token for the IMSI as signed 64 bit integer.
    """
    data = {config.imsi_record_name: imsi}
    result = get_record_worker().process(data)
    token = standard_b64decode(result[config.imsi_record_name])
    token_decimal = int.from_bytes(token, byteorder='big', signed=True)
    return token_decimal


def main(source_table: str, dest_table: str, imsi_column: str):
    """
    Set up everything needed and start producing (mock) records.

    :param source_table: Hive table to read untokenised records from.
    :param dest_table: New Hive table to write tokenised records to.
    :param imsi_column: Name of the column containing the IMSI (to tokenise).
    """

    # Have a look at this on how it works.
    # https://spark.apache.org/docs/latest/sql-programming-guide.html#hive-tables

    warehouse_location = os.path.abspath('spark-warehouse')
    sc = SparkContext()
    sc.setLogLevel('WARN')
    # pyspark.sql.SparkSession(sc) for Spark 2.0 instead of HiveContext(sc).
    spark = (SparkSession
             .builder
             .appName('Batch Tokeniser')
             .config('spark.sql.warehouse.dir', warehouse_location)
             .config('spark.dynamicAllocation.enabled', 'true')
             .config('spark.dynamicAllocation.maxExecutors', 20)
             .config('hive.exec.dynamic.partition', 'true')
             .config('hive.exec.dynamic.partition.mode', 'nonstrict')
             .enableHiveSupport()
             .getOrCreate())
    logger.info('Dropping (if exists) old destination table.')
    spark.sql('DROP TABLE IF EXISTS {}'.format(dest_table))
    logger.info('Creating destination table "{}".'.format(dest_table))
    spark.sql('CREATE TABLE {dest} LIKE {source}'
              .format(source=source_table, dest=dest_table))
    # Make the Spark Context available to the token base.
    # (Argh, `sc` doesn't pickle! :-( )
    # get_record_worker().token_base.sc = sc
    logger.info('Starting the tokenisation of "{source}" to "{dest}".'
                .format(source=source_table, dest=dest_table))
    input_df = spark.read.table(source_table)
    # Create a deterministic user defined function for tokenisation.
    tokenise_udf = udf(f=imsi_to_token, returnType=LongType())
    output_df = input_df.withColumn(imsi_column, tokenise_udf(imsi_column))
    # To write straight to Hive:
    (output_df
        .write
        .mode('append')
        .insertInto(dest_table))
    sc.stop()
    logger.info('Tokenisation finished.')


def _console():
    """
    Entry point for console execution.
    """
    # Set up logger.
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(name)s\t%(asctime)s %(message)s')

    # Parse command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('source_table',
                        help='Path to the input table file to be tokenised')
    parser.add_argument('imsi_column',
                        help=('Name of the column with the IMSI'
                              ' to be tokenised'))
    parser.add_argument('dest_table',
                        help='Path to write tokenised table file to')
    args = parser.parse_args()

    imsi_column = args.imsi_column
    source_table = args.source_table
    dest_table = args.dest_table

    # Now let's do the work.
    main(source_table, dest_table, imsi_column)


if __name__ == '__main__':
    _console()
