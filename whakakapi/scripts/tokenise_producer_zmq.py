#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a  ZeroMQ-based tokenisation (mock)
record producer end-point.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import json
import logging
import random
import time

from whakakapi import zmq_communicator
from whakakapi import zmq_producer
from whakakapi.config import config


MESSAGES = 1000000
NO_DEVICES = 40000  # 4000000
AVG_TIME_PER_RECORD = 0.0001


def main(no_messages: int):
    """
    Set up everything needed and start producing (mock) records.

    :param no_messages: Number of messages to request.
    """
    producer = zmq_producer.MakeRecords(config.in_queue, bind=True)
    my_imsi_faker = zmq_producer.ImsiFaker(NO_DEVICES)

    for _ in range(no_messages):
        imsi = my_imsi_faker.request()
        # For NZ Spark style 'real' IMSIs:
        # '53005{:010d}'.format(random.randrange(0, NO_DEVICES)
        data = {'imsi': imsi, 'lat': 3.141, 'long': 42.123}
        msg = json.dumps(data)
        producer.request(msg.encode())
        time.sleep(random.random() * 2 * AVG_TIME_PER_RECORD)
    zmq_communicator.get_zmq_context().destroy(linger=None)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(asctime)s %(message)s')
    main(MESSAGES)
