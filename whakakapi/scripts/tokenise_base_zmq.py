#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a ZeroMQ-based tokenisation base
service.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging

from whakakapi import verifying_log
from whakakapi import zmq_base
from whakakapi import zmq_communicator
from whakakapi.config import config

logger = logging.getLogger(__name__)


def main():
    """
    Set up everything needed for a fully assembled tokenisation base service.
    """
    # Plumbing for file persisted token service.
    token_table = verifying_log.VerifyingLog(config.token_log_filename)
    logger.info("Loaded {} token table entries, ready for rock'n'roll."
                .format(verifying_log.tip_to_integer(token_table.tip) + 1))
    token_sender = zmq_base.ZmqTokenSender(config.update_queue)
    token_service = zmq_base.ZmqTokenService(config.service_queue,
                                             token_table,
                                             token_sender)
    try:
        zmq_communicator.get_zmq_context().term()
    except KeyboardInterrupt:
        logger.info('Interrupted, shutting down.')
        # FIXME: Look at Python destructor to close log file. Maybe via try/except/finally on KeyboardInterrupt
        token_table._log_file.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(asctime)s %(message)s')
    main()
