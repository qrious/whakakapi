#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module providing an entry point to start a ZeroMQ-based tokenisation consumer
end-point.
"""

# Created: 2017-07-31 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging

from whakakapi import zmq_communicator
from whakakapi import zmq_consumer
from whakakapi.config import config

logger = logging.getLogger(__name__)


def main():
    """
    Set up everything needed and start receiving (mock) records.
    """
    consumer = zmq_consumer.GetRecords(config.out_queue, bind=True)
    try:
        zmq_communicator.get_zmq_context().term()
    except KeyboardInterrupt:
        logger.info('Interrupted, shutting down.')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        # filename='runner.log',
                        format='%(levelname)s\t%(asctime)s %(message)s')
    main()
