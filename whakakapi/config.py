# -*- coding: utf-8 -*-
"""
Module managing configuration settings for config files and defaults. System
and user level configuration files are detected and automatically read. The
configuration files are following a YAML syntax.

Note: Some commented lines refer to the use of the `pydantic` package. It is
      nice for configuration, but only available for Python 3.6 onwards.
"""

# Created: 2017-06-20 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging
import os

import appdirs
import yaml


# import pydantic
# class _Settings(pydantic.BaseSettings):
class _Settings(object):
    """
    Default configuration options.
    """

    # ### General options.
    # Maximum numbers the worker requests from the token base service.
    records_per_request_limit = 100000
    # Number of messages to process before reporting on the log.
    # (E.g. every 1000 records, but at the latest every 600 seconds.)
    reporting_interval_records = 1000
    # Number of seconds to process before reporting on the log.
    # (E.g. every 1000 records, but at the latest every 600 seconds.)
    reporting_interval_time = 600  # 10 min.
    # File name for persisting the token base.
    token_log_filename = 'imsi_token_vlog.gz'
    # Name of the IMSI field in processed records.
    imsi_record_name = 'imsi'

    # ### Logging and error reporting email options.
    # Log level.
    log_level = logging.INFO
    # Log file name.
    log_file = '/var/log/whakakapi/whakakapi.log'
    # Maximal number of bytes per log.
    log_max_bytes = 2 * 1024 ** 2  # 2 MiB
    # Number of log backups for log rotation.
    log_backup_count = 5
    # SMTP server host.
    smtp_server = 'foo.bar.baz'
    # Mail error notifications to.
    mail_notifications_to = ['sre@qrious.co.nz',
                             'guy.kloss@qrious.co.nz']
    # Mail sender.
    mail_sender = 'Whakakapi location data tokeniser <support@qrious.co.nz>'
    # Mail subject.
    mail_subject = "[Whakakapi] something didn't work in the tokenisation"

    # ### RESTful Web Service Token Base options.

    # Host name/IP the token base Web Service binds to.
    # (Interfaces to serve to, '0.0.0.0' for all interfaces.
    # This might be dangerous without additional precautions!)
    ws_base_bind_host = '127.0.0.1'
    # Host name/IP the worker uses to connect to the token base Web Service.
    ws_base_host = 'localhost'
    # Post number used for the token base Web Service.
    ws_base_port = 5000
    # Base URL the worker uses for the token base Web Service.
    ws_base_url = 'http://{host}:{port}'
    # Should the worker connect to the token base Web Service via a proxy
    # (if configured for the environment).
    ws_worker_use_proxy = True
    # Relay a 'progress remark' in token requests (informational only, e.g. for
    # communication of progress).
    ws_worker_request_remark = True

    # ### ZeroMQ options.

    # Receiving queue for the worker in ZeroMQ URL format
    # (e.g. ipc:///tmp/whakakapi_in, tcp://127.0.0.1:6000).
    in_queue = 'tcp://127.0.0.1:6000'
    # Outgoing queue for the worker in ZeroMQ URL format
    # (e.g. ipc:///tmp/whakakapi_out, tcp://127.0.0.1:6001).
    out_queue = 'tcp://127.0.0.1:6001'
    # Receiving queue for the worker in ZeroMQ URL format
    # (e.g. ipc:///tmp/whakakapi_service, tcp://127.0.0.1:6002).
    service_queue = 'tcp://127.0.0.1:6002'
    # Receiving queue for the worker in ZeroMQ URL format
    # (e.g. ipc:///tmp/whakakapi_updates, tcp://127.0.0.1:6003).
    update_queue = 'tcp://127.0.0.1:6003'
    # Topic for update broadcasts from the token base service.
    update_topic = b'update'
    # Topic for addressing the token base service.
    token_base_topic = b'base'
    # Seconds a transaction in flight is kept alive.
    transaction_ttl = 10

    # ### PostgreSQL Token Base options.

    # Host name/IP of the token DB (PostgreSQL host).
    token_db_host = '10.100.100.14'
    # TCP port the of the token DB listens to (PostgreSQL host).
    token_db_port = 5432
    # Name of the token DB (DB instance).
    token_db_name = 'transport_dev_db'
    # User name to access the token DB.
    token_db_user = 'transport_admin'
    # Password to access the token DB.
    token_db_password = 'super_secret'

    def __init__(self, appname):
        """
        Initialise the configuration by overriding defaults with system and/or
        user configurations.
        """
        config_paths = [appdirs.user_config_dir(appname),
                        appdirs.site_config_dir(appname)]

        for config_path in config_paths:
            if os.path.exists(config_path):
                file_config = yaml.load(open(config_path))
                for key, value in file_config.items():
                    setattr(self, key, value)

#     class Config:
#         env_prefix = 'MY_PREFIX_'  # defaults to 'APP_'
#         fields = {
#             'auth_key': {
#                 'alias': 'my_api_key'
#             }
#         }


# config = _Settings(**_user_config)
config = _Settings('whakakapi')
