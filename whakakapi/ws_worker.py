# -*- coding: utf-8 -*-
"""
Module providing a worker communicating via Web Services to provide
tokeniseation services.
"""

# Created: 2017-11-10 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import base64
import collections
import logging
import os
import sys
from typing import Union, Iterable

import backoff
import requests

from whakakapi import worker
from whakakapi.config import config
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import (deserialisable_value,
                                     deserialise_record,
                                     VerifyingLog)

INDIVIDUAL_REQUEST = '/mapping/{imsi}'
BATCH_REQUEST = '/mapping'
REQUESTER_TIP_PARAM = 'requester_tip'
LIMIT_PARAM = 'limit'
REMARKS_PARAM = 'rem'
_session = None
logger = logging.getLogger(__name__)


def _get_session() -> requests.Session:
    """
    Returns a session object to the web server.
    """
    global _session
    if not _session:
        logger.debug('Making new HTTP(S) session.')
        _session = requests.Session()
        _session.trust_env = config.ws_worker_use_proxy
    return _session


def _backoff_handler(details):
    """Handler for web service request back-off events."""
    logger.warning('Backing off {wait:0.1f} seconds afters {tries} tries,'
                   ' calling function {target} with args {args}.\n'
                   ' Exception: {ex}'
                   .format(ex=sys.exc_info(), **details))


class WsTokenBase(worker.TokenBase):
    """
    Proxy object to manage all interactions with the token base service using
    a token base Web Service.
    """
    ws_base_url = ''
    """Web Service base URL."""
    sc = None
    """Spark Context (if made available)."""

    def __init__(self, token_table: VerifyingLog):
        """
        Constructor.

        :param token_table: Token lookup table (append only log).
        """
        super().__init__(token_table)
        self.ws_base_url = config.ws_base_url.format(
            host=config.ws_base_host, port=config.ws_base_port)

    @backoff.on_exception(backoff.expo,
                          requests.exceptions.RequestException,
                          max_time=300,
                          on_backoff=_backoff_handler)
    def _request_token(self, imsi: bytes, limit: int) -> bytes:
        """
        Request a single token from the DB. If the index of this token is
        beyond the next one from the current token table's tip, the missing
        ones will be retrieved and added to the token table.

        :param imsi: IMSI to request a token for.
        :param limit: Maximum number of records to return.
        :return: Token for the requested IMSI.
        """
        session = _get_session()
        params = {REQUESTER_TIP_PARAM: self.token_table.tip,
                  LIMIT_PARAM: limit}
        if config.ws_worker_request_remark:
            params[REMARKS_PARAM] = ('token count {} on {}'
                                     .format(self.counter, os.getpid()))
            if self.sc:
                workers = self.sc._jsc.sc().getExecutorMemoryStatus().size()
                params[REMARKS_PARAM] += ' with {} workers'.format(workers)
        request = session.post(
            (self.ws_base_url + INDIVIDUAL_REQUEST)
            .format(imsi=base64.urlsafe_b64encode(imsi).decode()),
            params=params)
        if not request.status_code == 200:
            logger.error('Could not fetch token from token base for URL'
                         ' {} with HTTP Error {}: {}'
                         .format(request.url,
                                 request.status_code,
                                 request.content))
            sys.exit(1)

        records = (x.decode() for x in request.content.split(b'\n') if x)
        try:
            # Insert the (many) records.
            self.token_table.insert_many(deserialise_record(x)
                                         for x in records)
        except ValueError as e:
            logger.warn('Could not insert record(s)', e)
            self.token_base.update_table()
        while int(request.headers.get('X-More-Records', '0')) > 0:
            # Fetch stuff from current tip to update local cache until
            # we've got them all.
            params = {'requester_tip': self.token_table.tip, 'limit': limit}
            request = session.get(self.ws_base_url + BATCH_REQUEST,
                                  params=params)
            if not request.status_code == 200:
                logger.error('Could not fetch batch of tokens from token'
                             ' base from tip {} with HTTP Error {}: {}'
                             .format(self.token_table.tip,
                                     request.status_code,
                                     request.content))
                sys.exit(1)
            records = (x.decode() for x in request.content.split(b'\n') if x)
            try:
                # Insert the (many) records.
                self.token_table.insert_many(deserialise_record(x)
                                             for x in records)
            except ValueError as e:
                logger.warn('Could not insert record(s)', e)
                self.token_base.update_table()
        return self.token_table.get_token(imsi)

    def _request_records_after(self, limit: int) -> Iterable:
        """
        Request a range of token records from the DB. The retrieved records
        will be added to the token table.

        :param limit: Maximum number of records to return.
        :return: Iterable of records.
        """
        session = _get_session()
        params = {REQUESTER_TIP_PARAM: self.token_table.tip,
                  LIMIT_PARAM: limit}
        request = session.get(self.ws_base_url + BATCH_REQUEST,
                              params=params)
        if not request.status_code == 200:
            logger.error('Could not fetch batch of tokens from token'
                         ' base from tip {} with HTTP Error {}: {}'
                         .format(self.token_table.tip,
                                 request.status_code,
                                 request.content))
            sys.exit(1)

        records = list(deserialise_record(x)
                       for x in request.content.split(b'\n') if x)
        try:
            # Insert the (many) records.
            self.token_table.insert_many(records)
        except ValueError as e:
            logger.warn('Could not insert record(s)', e)
            self.token_base.update_table()
        return records

    def request_impl_sync(self, message: ManagedMessage) -> Union[bytes, Iterable]:
        """
        To produce (send) an outgoing request message synchronously.

        Note: This method already inserts all retrieved records into the
              local token table.

        :param message: Message defining the token base service inquiry.
        :return: Token for the requested IMSI on IMSI requests, on
            'range after' requests an iterable of token table records.
        """
        # Extract some key parameters from the message.
        request_for = message.body['request_for']
        limit = message.body.get('limit', config.records_per_request_limit)
        if request_for == 'imsi':
            imsi = deserialisable_value(message.body.get('imsi'))
            return self._request_token(imsi, limit)
        elif request_for == 'records_after':
            return self._request_records_after(limit)
        else:
            logger.error('Got unknown DB token base request: {}!'
                         .format(request_for))
            sys.exit(1)

    def update_table(self):
        """
        Update the token table.
        """
        session = _get_session()
        limit = config.records_per_request_limit
        DummyRequest = collections.namedtuple('DummyRequest', ['headers'])
        request = DummyRequest({'X-More-Records': 1})
        while int(request.headers.get('X-More-Records', '0')) > 0:
            # Fetch stuff from current tip to update local cache until
            # we've got them all.
            params = {'requester_tip': self.token_table.tip, 'limit': limit}
            request = session.get(self.ws_base_url + BATCH_REQUEST,
                                  params=params)
            if not request.status_code == 200:
                logger.error('Could not fetch batch of tokens from token'
                             ' base from tip {} with HTTP Error {}: {}'
                             .format(self.token_table.tip,
                                     request.status_code,
                                     request.content))
                sys.exit(1)
            records = (x.decode() for x in request.content.split(b'\n') if x)
            try:
                # Insert the (many) records.
                self.token_table.insert_many(deserialise_record(x)
                                             for x in records)
            except ValueError as e:
                logger.warn('Could not insert record(s)', e)
                self.token_base.update_table()
