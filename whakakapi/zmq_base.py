# -*- coding: utf-8 -*-
"""
Module providing a "ground truth" instance of a IMSI to token mapper. This
implementation communicates via messages using ZeroMQ.
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'


import base64
import json
import logging
import time
import weakref

from whakakapi import verifying_log
from whakakapi.config import config
from whakakapi.messages import ManagedMessage
import whakakapi.zmq_communicator as zmq_comms

logger = logging.getLogger(__name__)


class ZmqTokenSender(object):
    """
    Token service sender (publisher) communicating back to the token workers
    via ZeroMQ.
    """
    counter = 0
    """Internal counter for number of processed requests."""
    _sender = None  # type: zmq_comms.Publisher
    """ZeroMQ publisher for responses and broadcast notifications."""

    def __init__(self, end_point: str):
        """
        Constructor.

        :param end_point: Address of end point to bind to.
        """
        self._sender = zmq_comms.Publisher(end_point, bind=True)

    def publish(self, message: ManagedMessage):
        """
        Publishes the managed message.

        :param: Managed message to send.
        """
        if message.topic == config.update_topic:
            logger.debug('Sending broadcast {}: {}'
                         .format(self.counter, message))
        self._sender.publish(message.message)
        self.counter += 1


class ZmqTokenService(object):
    """
    Token service instance processing incoming requests communicating via
    ZeroMQ.
    """
    counter = 0
    """Internal counter for number of processed requests."""
    token_table = None
    """Reference to token sending handler."""
    token_sender = None
    """Threshold number of records to report timing on request after."""
    _after_timing_reporting_threshold = 100
    """Report in the logs only after this number of records sent back."""
    _receiver = None  # type: zmq_comms.Puller
    """ZeroMQ receiver for incoming requests."""

    def __init__(self, end_point: str,
                 token_table: verifying_log.VerifyingLog,
                 token_sender: ZmqTokenSender):
        """
        Constructor.

        :param end_point: Address of end point to bind to.
        :param token_table: Token lookup table (append only log).
        :param token_sender: Instance of the token sender (publisher).
        """
        self._receiver = zmq_comms.Puller(end_point, bind=True,
                                          callback=self.process)
        self.token_table = weakref.proxy(token_table)
        self.token_sender = weakref.proxy(token_sender)
        self._receiver.run()

    def _process_request_for_imsi(self, request: dict) -> (bytes, int):
        """
        Processes a request for a single IMSI.

        :return: Serialised IMSI record, number of outstanding records.
        """
        imsi = base64.standard_b64decode(request['imsi'])
        broadcast = False
        if not self.token_table.has_token(imsi):
            self.token_table.add(imsi)
            broadcast = True
        record_body = self.token_table.get_record(imsi=imsi).encode()
        if broadcast:
            # Shout out the new record to the world.
            broadcast_message = ManagedMessage(body=record_body)
            self.token_sender.publish(broadcast_message)
        requester_tip = request.get('requester_tip')
        if requester_tip and (self.token_table.tip - requester_tip > 1):
            return self._process_request_for_after(request)
        else:
            return record_body, 0

    def _process_request_for_after(self, request: dict) -> (bytes, int):
        """
        Processes a request for records after a given index.

        :return: Serialised IMSI records, number of outstanding records.
        """
        index = request['requester_tip']
        limit = request.get('limit', config.records_per_request_limit)
        start_t = time.time()
        records_message, num_records, more_records = (
            self.token_table.get_records_after(index=index, limit=limit))
        if records_message == '':
            logger.debug('No more records available after index {},'
                         ' own tip is {}.'
                         .format(index, self.token_table.tip))
        if num_records > self._after_timing_reporting_threshold:
            logger.info('Processing "records_after" request for {} records:'
                        ' {:2.2f} s'
                        .format(num_records, time.time() - start_t))
        return records_message.encode(), more_records

    def process(self, raw_message: bytes):
        """
        Processes an incoming token service request of a raw message received.

        :param raw_message: Raw binary message as received via the transport.
        """
        self.counter += 1
        message = ManagedMessage(raw=raw_message)
        request = json.loads(message.body.decode())
        sender = message.header['from']
        tx_id = message.header.get('tx')
        request_for = request['request_for']
        processor = None
        if request_for == 'imsi':
            processor = self._process_request_for_imsi
        elif request_for == 'records_after':
            processor = self._process_request_for_after
        else:
            logger.warning('Received an unknown request message that cannot'
                           ' be handled: "{}".'
                           .format(message))
            return
        body, more = processor(request)
        header = {'request_for': request_for, 'more_records': more}
        response = ManagedMessage(topic=sender.encode(),
                                  header=header, body=body)
        if tx_id:
            response.header['tx'] = tx_id
        self.token_sender.publish(response)
