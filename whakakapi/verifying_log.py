# -*- coding: utf-8 -*-
"""
Module implementing a self verifying log (append only) table data structure.
"""

# Created: 2017-04-11 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import base64
import gzip
import hashlib
import io
import json
import logging
import sys
import threading
import time
from typing import Iterable, Tuple, List, Union, Any, Optional

import nacl.utils

from whakakapi.synchronise import synchronized


TOKEN_LENGTH = 8
GENESIS_BLOCK = (
    '"If you want to build a ship, don\'t drum up the men to gather wood, '
    ' divide the work, and give orders. Instead, teach them to yearn for '
    ' the vast and endless sea."\n'
    '-- Antoine de Saint-Exupéry\n'
    '(µ-wisdom brought to you by Guy, the author of this code.)').encode()

logger = logging.getLogger(__name__)


def serialisable_value(value: Any) -> Any:
    """
    Converts a single value into a JSON serialisable format.

    :param record: Primitive data type to serialise.
    :return: Representation of value to be serialisable.
    """
    if isinstance(value, (str, bytes)):
        return base64.standard_b64encode(
            value.encode() if isinstance(value, str) else value).decode()
    else:
        return value


def int_to_bytes(value: int) -> bytes:
    """
    Converts an unsigned integer (internal tip counter) to a 32 bit byte
    representation (to be used in the hash-based integrity checking).

    :param value: The (unsigned) integer value to convert.
    :return: Byte representation.
    """
    return value.to_bytes(4, byteorder='big', signed=False)


def deserialisable_value(value: Any) -> Any:
    """
    Converts a single value into a JSON serialisable format.

    :param record: Primitive data type to serialise.
    :return: Representation of value to be serialisable.
    """
    if isinstance(value, str):
        return base64.standard_b64decode(value)
    else:
        return value


def serialise_record(record: Iterable) -> str:
    """
    Serialises a record for storage or transfer.

    :param record: Iterable object to serialise.
    :return: Serialised string representation of object.
    """
    return json.dumps([serialisable_value(x) for x in record])


def deserialise_record(record: str) -> List:
    """
    Deserialises a record from storage or transfer.

    :param record: String to deserialise.
    :return: Deserialised list representation of object.
    """
    return [deserialisable_value(x)
            for x in json.loads(record)]


def tip_to_integer(tip_value: Union[int, None]) -> int:
    """
    Converts a tip indication (where a None indicates no initialisation) to a
    (signed) integer for comparison. A None value is represented by a `-1`.

    :param tip_value: Tip value as used in the VerifyingLog structure.
    :return: The integer representation.
    """
    return tip_value if tip_value is not None else -1


class VerifyingLog(object):
    """
    A self verifying log (append only) table data structure.
    """

    imsis = None  # type: List
    """List of all the IMSIs."""
    imsis_map = None  # type: Dict
    """IMSIs mapping to indexes."""
    tokens = None  # type: List
    """List of all tokens."""
    hashes = None  # type: List
    """List of all record hashes."""
    timestamps = None  # type: List
    """List of all timestamps."""
    tip = None  # type: Union[int, None]
    """Current tip index of the log."""
    filename = None  # type: str
    """Name of log file."""
    _lock = None  # type: threading.RLock
    """Lock object for protecting concurrent operations."""

    def __init__(self, filename: str=None):
        """
        Constructor.

        :param filename: Name of log file.
        """
        self._lock = threading.RLock()
        self.imsis = []
        self.imsis_map = {}
        self.tokens = []
        self.hashes = []
        self.timestamps = []
        self.tip = None
        self.filename = filename
        if filename:
            logger.info('Initialising verifying log with file {}.'
                        .format(filename))
            try:
                logger.debug('Loading data from file {}.'.format(filename))
                with gzip.open(filename, 'rt') as source:
                    self.load(source)
            except FileNotFoundError:
                logger.warn('Log file {} not present: creating it.'
                            .format(filename))
        self.in_flight_requests = {}

    def _new_record_hash(self, tip: Union[int, None], imsi: bytes,
                         token: bytes) -> bytes:
        """
        Computes a hash value for the new log record.

        :param tip: Index of the new end of the log.
        :param imsi: IMSI to store in the log table.
        :param token: Assigned token to the IMSI.
        :return: Bytes of log record hash.
        """
        to_hash = GENESIS_BLOCK
        if tip > 0:
            to_hash = self.hashes[-1]
        to_hash += int_to_bytes(tip)
        to_hash += imsi
        to_hash += token
        return hashlib.sha256(to_hash).digest()

    @synchronized
    def add(self, imsi: bytes) -> Tuple:
        """
        Add an IMSI to the log.

        :param imsi: IMSI to store in the log table.
        :return: Tuple
            (`new_tip`, `imsi`, `token`, `record_hash`, `timestamp`).
            In case the IMSI is in the log already, `None` is returned.
        """
        if imsi in self.imsis_map:
            # Got it, just return.
            return

        # Determine values to store.
        new_tip = tip_to_integer(self.tip) + 1
        token = nacl.utils.random(TOKEN_LENGTH)
        while token in self.tokens:
            # To avoid collisions if the token length is too small.
            token = nacl.utils.random(TOKEN_LENGTH)
        record_hash = self._new_record_hash(new_tip, imsi, token)
        timestamp = int(time.time())

        # Store it all.
        self.imsis.append(imsi)
        self.imsis_map[imsi] = new_tip
        self.tokens.append(token)
        self.timestamps.append(timestamp)
        self.hashes.append(record_hash)
        self.tip = new_tip
        record = new_tip, imsi, token, record_hash, timestamp

        # Persist if we've got a log file.
        if self.filename:
            with gzip.open(self.filename, 'at') as log_file:
                log_file.write('{}\n'.format(serialise_record(record)))

        return record

    @synchronized
    def insert(self, new_tip: Union[int, None], imsi: bytes, token: bytes,
               record_hash: bytes, timestamp: Optional[int]=None) -> None:
        """
        Verifies a complete record and inserts it into the log.

        :param new_tip: Index of new tip (one ahead of current index).
        :param imsi: IMSI of the record.
        :param token: Token of the record.
        :param record_hash: Verification hash of the record.
        :param timestamp: Optional timestamp parameter (informational only,
            default: None).
        :raises: `ValueError` on verification errors.
        """
        if self.tip is None:
            if new_tip != 0:
                raise ValueError('New tip must be 0 on empty log, but it is {}'
                                 .format(new_tip))
        elif new_tip <= self.tip:
            logger.debug('Attempt to insert existing record at tip value {},'
                         ' current tip is {}'
                         .format(new_tip, self.tip))
            return
        else:
            if new_tip != self.tip + 1:
                raise ValueError('Illegal new tip value {}, current tip is {}'
                                 .format(new_tip, self.tip))
        if record_hash != self._new_record_hash(new_tip, imsi, token):
            raise ValueError('Mismatch of record hashes')

        # Store it all.
        self.imsis.append(imsi)
        self.imsis_map[imsi] = new_tip
        self.tokens.append(token)
        self.timestamps.append(timestamp)
        self.hashes.append(record_hash)
        self.tip = new_tip

    @synchronized
    def insert_many(self, to_add: Iterable) -> None:
        """
        Verifies and inserts a number of complete record into the log.

        Note: Records must be in order to be insertable. It is possible that
              the call will terminate with an exception part way through
              the process if an invalid record is detected.

        :param to_add: Iterable of record tuples of the format
            (`new_tip`, `imsi`, `token`, `record_hash`, `timestamp`).
            The last element (`timestamp`) is optional and may be missing.
        :raises: `ValueError` on verification errors.
        """
        for record in to_add:
            self.insert(*record)

    def _get_record(self, index: int) -> Tuple:
        """
        Gets a record at the given index.

        :param index: Index of record.
        :return: Iterable of record tuples of the format
            (`index`, `imsi`, `token`, `record_hash`, `timestamp`).
        """
        return (index, self.imsis[index], self.tokens[index],
                self.hashes[index], self.timestamps[index])

    @synchronized
    def has_token(self, imsi: bytes) -> bool:
        """
        Checks whether a token is available for an IMSI.

        :param imsi: IMSI to check for the token existence.
        :return: True if a token is mapped to the IMSI.
        """
        return imsi in self.imsis_map

    @synchronized
    def get_token(self, imsi: bytes) -> bytes:
        """
        Gets a token assigned to an IMSI.

        :param imsi: IMSI to get the token for.
        :return: Token assigned to the IMSI.
            available.
        """
        return self.tokens[self.imsis_map[imsi]]

    @synchronized
    def get_imsi(self, token: bytes) -> bytes:
        """
        Returns the IMSI corresponding to the given token.

        :param token: A token.
        :return: The corresponding IMSI.
        """
        index = self.tokens.index(token)
        return self.imsis[index]

    @synchronized
    def load(self, imsis: io.IOBase) -> None:
        """
        Loads a batch of IMSI records with tokens from a unicode string stream
        object into the local log. Loads can be performed through the text
        message format. If the log is populated, additional loads will allow
        for incremental population of records.

        :param imsis: Stream object containing (JSON) serialised record tuples
            of the format (`index`, `imsi`, `token`, `record_hash`,
            `timestamp`). The last element (`timestamp`) is optional and may be
            missing.
        """
        for line in imsis:
            if not line:
                # Skip empty line (like final new line in file).
                continue
            record = deserialise_record(line)
            self.insert(*record)

    @synchronized
    def get_record(self, index: int=None, imsi: bytes=None,
                   include_ts: bool=False) -> str:
        """
        Returns the serialised mapping records from the log after a particular
        position onwards.

        The position is either identified by the `index` or the `imsi`
        parameter (not both). If none is given, returning records will start
        from the first entry in the log. Output can be limited by the `limit`
        parameter.

        :param index: Index of the record in the log (default: None).
        :param imsi: IMSI of the record in the log following which to return
            records from (default: None).
        :param include_ts: True if time stamps are to be part of the returned
            records (default: False).
        :return: (JSON) serialised record tuples. The record tuples are of the
            format (`imsi`, `token`, `record_hash`, `timestamp`).
            The last element (`timestamp`) is optional and is missing if the
            `include_ts` is set to True.
        """
        if imsi and index:
            logger.error('Cannot provide `index` and `imsi`'
                         ' to VerifyingLog.get_record!')
            sys.exit(1)
        i = 0
        if index:
            i = index
        elif imsi:
            i = self.imsis_map[imsi]
        record = self._get_record(i)
        if not include_ts:
            record = record[:-1]
        return serialise_record(record)

    @synchronized
    def get_records_after(self, index: Optional[int]=None,
                          imsi: Optional[bytes]=None,
                          limit: Optional[int]=None,
                          include_ts: Optional[bool]=False) -> (str, int, int):
        """
        Returns the serialised mapping records from the log after a particular
        position onwards.

        The position is either identified by the `index` or the `imsi`
        parameter (not both). If none is given, returning records will start
        from the first entry in the log. Output can be limited by the `limit`
        parameter.

        :param index: Index of the record in the log (default: None).
        :param imsi: IMSI of the record in the log following which to return
            records from (default: None).
        :param limit: Number of records to return as an upper limit
            (default: None for all available).
        :param include_ts: True if time stamps are to be part of the returned
            records (default: False).
        :return: (JSON) serialised record tuples, a number indicating the
            number of records returned and the number of further records
            available. The record tuples are of the format (`imsi`, `token`,
            `record_hash`, `timestamp`). The last element (`timestamp`) is
            optional and is missing if the `include_ts` is set to True.
        """
        if imsi and index:
            logger.error('Cannot provide `index` and `imsi`'
                         ' to VerifyingLog.get_records_after!')
            sys.exit(1)
        if self.tip is None:
            # Nothing there, bail out.
            return '', 0, 0
        limit = limit or (self.tip + 1)
        result = io.StringIO()
        start_index = 0
        if index is not None:
            start_index = index + 1
        elif imsi:
            start_index = self.imsis_map[imsi] + 1
        i = start_index
        while (i <= self.tip) and (limit > 0):
            record = self.get_record(i, include_ts=include_ts)
            result.write('{}\n'.format(record))
            i += 1
            limit -= 1
        more_records = self.tip - i + 1
        if more_records < 0:
            # Happens if the index after passed was higher than the tip.
            more_records = 0
        return result.getvalue(), i - start_index, more_records
