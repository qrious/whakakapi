# -*- coding: utf-8 -*-
"""
Module providing a "ground truth" instance of a IMSI to token mapper. This
implementation communicates via a RESTful Web Service.
"""

# Created: 2017-11-09 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'


import base64
import io
import json
import logging
import time
from typing import Union
import weakref

from flask import request, Flask, Response

from whakakapi.config import config
from whakakapi.verifying_log import (VerifyingLog,
                                     tip_to_integer,
                                     serialise_record)

CONTENT_TYPE = 'application/vnd.qrious.whakakapi'

logger = logging.getLogger(__name__)


def _int_from_string(value: Union[str, bytes, None]) -> Union[int, None]:
    """
    Converts a received value from an HTTP request into an integer or None.

    :param value: Value to convert (a string, byte sequence or None).
    :return: Integer representation of the number or None.
    :raises: ValueError on unconvertible values.
    """
    if value == '' or value is None:
        return None
    if isinstance(value, (str, bytes)):
        return int(value)
    else:
        return value


class WsTokenService(object):
    """
    Token service instance processing incoming requests via a Flask-based
    RESTful Web Service.
    """
    counter = 0
    """Internal counter for number of processed requests."""
    token_table = None
    """Reference to token sending handler."""
    token_sender = None
    """Threshold number of records to report timing on request after."""
    _after_timing_reporting_threshold = 100
    """Report in the logs only after this number of records sent back."""
    _update_table_cache = None
    """Caching responses for priming workers on initial table updates."""

    def __init__(self, token_table: VerifyingLog):
        """
        Constructor.

        :param token_table: Token lookup table (append only log).
        """
        self.token_table = weakref.proxy(token_table)
        self._update_table_cache = {}

    def _make_response(self, message: Union[str, bytes, io.IOBase],
                       status: int=200, headers: dict=None):
        """
        Makes a Flask response object.

        :param message: Response content message.
        :param status: HTTP status (default: 200).
        :param headers: HTTP headers (default: Content-Type of this service).
        """
        _headers = {'Content-Type': CONTENT_TYPE}
        if headers:
            _headers.update(headers)
        return Response(message, status, _headers)

    def map_imsi(self, imsi) -> bytes:
        """
        Maps an IMSI to the corresponding token if it's available.

        For GET requests to `/mapping/<imsi>`.

        :param imsi: IMSI encoded in URL-safe base64 of binary
            representation.
        """
        imsi_bytes = base64.urlsafe_b64decode(imsi)
        if self.token_table.has_token(imsi_bytes):
            response = self._make_response(
                self.token_table.get_record(imsi=imsi_bytes))
        else:
            message = json.dumps({
                'title': 'IMSI not found.',
                'message': 'Requested IMSI {} not found.'.format(imsi),
                'code': 404001
            })
            response = self._make_response(message, 404)
        return response

    def map_or_create_imsi(self, imsi: bytes) -> bytes:
        """
        Maps or creates (if not available) a new IMSI mapping.

        For POST requests to `/mapping/<imsi>`.

        :param imsi: IMSI encoded in URL-safe base64 of binary
            representation.

        URL parameters:
        :param requester_tip: Indication of the requestor's tip at the point
            of time of the request. This way the service can respond with a
            batch of records bringing the requestor up to date with possibly
            missing records.
        :type requester_tip: int
        :param limit: Optionally the limit for the maximal batch size of
            records returned (used with the `requester_tip` parameter). If
            not given, the limit will be retrieved from the configuration.
        :type limit: int
        """
        imsi_bytes = base64.urlsafe_b64decode(imsi)
        requester_tip = None
        if 'requester_tip' in request.args:
            requester_tip = tip_to_integer(
                _int_from_string(request.args['requester_tip']))
        if 'rem' in request.args:
            logger.info('Requester remark: "{}".'.format(request.args['rem']))
        if self.token_table.has_token(imsi_bytes):
            record_str = self.token_table.get_record(imsi=imsi_bytes)
        else:
            record_str = serialise_record(self.token_table.add(imsi_bytes))
        if (requester_tip is not None
                and (self.token_table.tip - requester_tip > 1)):
            # Deferring to the records_after() method if off by more.
            return self.records_after()
        else:
            return self._make_response(record_str)

    def records_after(self):
        """
        Maps an IMSI to the corresponding token if it's available.

        For GET requests to `/mapping`.

        URL parameters:
        :param records_after: Record index after which to return a batch of
            records.
        :type records_after: int
        :param limit: Optionally the limit for the maximal batch size of
            records returned (used with the `records_after` parameter). If
            not given, the limit will be retrieved from the configuration.
        :type limit: int
        """
        # Use of `requester_tip` in addition if there was a relay from the
        # map_or_create_imsi() method.
        records_after = tip_to_integer(_int_from_string(
            request.args.get('records_after')
            or request.args.get('requester_tip')))
        limit = _int_from_string(
            request.args.get('limit', config.records_per_request_limit))
        start_t = time.time()
        if (records_after, limit) in self._update_table_cache:
            records_message, num_records = self._update_table_cache[
                (records_after, limit)]
            more_records = (self.token_table.tip - num_records -
                            records_after + 1)
        else:
            records_message, num_records, more_records = (
                self.token_table.get_records_after(index=records_after,
                                                   limit=limit))
            # Cache it, if it's a full batch.
            if not more_records:
                self._update_table_cache[(records_after, limit)] = (
                    records_message, num_records)
        response = self._make_response(records_message)
        if records_message == '':
            logger.debug('No more records available after index {},'
                         ' own tip is {}.'
                         .format(records_after, self.token_table.tip))
        if num_records > self._after_timing_reporting_threshold:
            logger.info('Processing "records_after" request for {} records:'
                        ' {:2.2f} s'
                        .format(num_records, time.time() - start_t))
        if more_records:
            response.headers['X-More-Records'] = more_records
        return response


def make_app(target: WsTokenService) -> Flask:
    """
    Makes the Flask service application.

    :param target: Instance of the target implementation class defining the
        operations.
    """
    app = Flask(__name__.split('.')[0])
    app.add_url_rule('/mapping/<imsi>', 'map_or_create_imsi',
                     methods=['POST'],
                     view_func=target.map_or_create_imsi)
    app.add_url_rule('/mapping/<imsi>', 'map_imsi',
                     methods=['GET'],
                     view_func=target.map_imsi)
    app.add_url_rule('/mapping', 'records_after',
                     methods=['GET'],
                     view_func=target.records_after)
    return app
