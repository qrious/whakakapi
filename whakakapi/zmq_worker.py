# -*- coding: utf-8 -*-
"""
Module providing a worker communicating via ZeroMQ to provide tokeniseation
services.
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging
import threading

from whakakapi import worker
from whakakapi.messages import ManagedMessage
from whakakapi.verifying_log import VerifyingLog
from whakakapi.worker import RecordsSink
from whakakapi.worker import TokenBase
import whakakapi.zmq_communicator as zmq_comms


TX_ID_LENGTH = 16
logger = logging.getLogger(__name__)


class ZmqRecordsSink(worker.RecordsSink):
    """
    Outgoing end of the worker pipeline stage using ZeroMQ.
    """
    _sink = None  # type: zmq_comms.Pusher
    """ZeroMQ sink for outgoing pipeline."""

    def __init__(self, end_point: str):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        """
        super().__init__()
        self._sink = zmq_comms.Pusher(end_point, bind=False)

    def request_impl(self, message: ManagedMessage):
        """
        To request (send) an outgoing message.

        :param message: Outgoing message.
        """
        self._sink.request(message.message)


class ZmqTokenBase(worker.TokenBase):
    """
    Proxy object to manage all interactions with the token base service using
    ZeroMQ.
    """

    _base = None  # type: zmq_comms.Pusher
    """ZeroMQ base service socket for outgoing requests."""

    def __init__(self, end_point: str,
                 token_table: VerifyingLog):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param token_table: Token lookup table (append only log).
        """
        super().__init__(token_table)
        self._base = zmq_comms.Pusher(end_point, bind=False)

    def request_impl_async(self, message: ManagedMessage):
        """
        To produce (send) an outgoing request message asynchronously.

        :param message: Outgoing message.
        """
        self._base.request(message.message)


class ZmqRecordsWorker(worker.RecordsWorker):
    """
    Worker that will receive requests, and forward tokenised records via the
    ZeroMQ.
    """

    _puller = None  # type: zmq_comms.Puller
    """ZeroMQ message (pull) receiver."""

    def __init__(self,
                 end_point: str,
                 token_table: VerifyingLog,
                 records_sink: RecordsSink,
                 token_base: TokenBase):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param token_table: Token lookup table (append only log).
        :param records_sink: Record sender (outbound).
        :param token_base: Proxy object to token base service.
        """
        super().__init__(token_table, records_sink, token_base)
        self._puller = zmq_comms.Puller(end_point, bind=False,
                                        callback=self.process)

    def run(self):
        """
        Starts running the subscriber in a thread.
        """
        self._puller_thread = threading.Thread(target=self._puller.run,
                                               args=())
        self._puller_thread.start()

    def join(self):
        """
        Joins the subscriber thread.
        """
        self._puller_thread.join()


class ZmqTokenUpdater(worker.TokenUpdater):
    """
    Listener on a ZeroMQ socket subscription to data updates from the token
    base service.
    """
    _subscriber = None  # type: zmq_comms.Subscriber
    """ZeroMQ message (subscription) receiver."""

    def __init__(self,
                 end_point: str,
                 worker_id: bytes,
                 token_table: VerifyingLog,
                 token_base: TokenBase):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param worker_id: ID of the local token base worker to subscribe to.
        :param token_table: Token lookup table (append only log).
        :param token_base: Proxy object to token base service.
        """
        super().__init__(token_table, token_base)
        self._subscriber = zmq_comms.Subscriber(end_point, bind=False,
                                                subscribe_to=worker_id,
                                                callback=self.process)

    def run(self):
        """
        Starts running the subscriber in a thread.
        """
        self._subscriber_thread = threading.Thread(target=self._subscriber.run,
                                                   args=())
        self._subscriber_thread.start()

    def join(self):
        """
        Joins the subscriber thread.
        """
        self._subscriber_thread.join()
