# -*- coding: utf-8 -*-
"""
Module providing a worker providing tokeniseation services.

Records are received and sent in a JSON encoded object, with the IMSI contained
in an attribute by the name specified in `config.imsi_record_name`. The IMSI
is stored in a signed 64 bit integer, converted to a base64 encoded big-endian
bytes for internal purposes.
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'


import base64
import collections
import json
import logging
import sys
import threading
import time
from typing import Dict, Callable, Union, Iterable, List
import weakref

import nacl.utils

from whakakapi.config import config
from whakakapi.messages import ManagedMessage
from whakakapi.messages import standard_b64decode
from whakakapi.synchronise import synchronized
from whakakapi.verifying_log import VerifyingLog
from whakakapi.verifying_log import deserialisable_value
from whakakapi.verifying_log import deserialise_record
from whakakapi.verifying_log import serialisable_value
from whakakapi.verifying_log import tip_to_integer


WORKER_ID_LENGTH = 16
TX_ID_LENGTH = 16
logger = logging.getLogger(__name__)


def generate_id(length: int) -> bytes:
    """
    Generate a new random ID of a given length in a base64 encoded
    representation.

    :param length: Length of ID in bytes.
    :return: The generated ID.
    """
    return base64.urlsafe_b64encode(nacl.utils.random(length)).rstrip(b'=')


class RecordsSink(object):
    """
    Abstract outgoing end of the worker pipeline stage.
    """
    counter = 0
    """Internal counter for number of sent records."""

    def produce(self, data: Dict):
        """
        Sends (produces) a message to be sent onwards in the pipeline.

        The sent message is an (unmanaged, plain) binary JSON object of a
        record.

        :param data: Dictionary of outgoing (JSON) message, of which the IMSI
            field has been tokenised.
        """
        self.counter += 1
        # Convert big-endian 64 bit base64 encoded bytes to a signed integer.
        imsi = standard_b64decode(data[config.imsi_record_name])
        imsi_decimal = int.from_bytes(imsi, byteorder='big', signed=True)
        data[config.imsi_record_name] = imsi_decimal
        message = json.dumps(data)
        logger.debug('Sending {}: {}'.format(self.counter, message))
        self.produce_impl(message.encode())

    def produce_impl(self, raw_message: bytes):
        """
        To produce (send) an outgoing message (not implemented).

        :param raw_message: Raw outgoing message.
        """
        raise NotImplementedError('This method needs to be overridden.')


class TokenBase(object):
    """
    Proxy object to manage all interactions with the token base service.
    """
    counter = 0
    """Internal counter for number of sent records."""
    token_table = None  # type: VerifyingLog
    """Local worker's token table."""
    worker_id = None  # type: bytes
    """Random byte sequence identifying this worker."""
    token_request_buffer = None  # type: collections.deque
    """
    Queue of token requests to process in order. Contains a tuple of the
    processing callable (callback) and the JSON body of request message as a
    calling parameter.
    """
    update_pending = False  # type: bool
    """Token table (batch) update is pending."""
    tx_in_flight = None  # type: Dict
    """Register of current unfinished transactions to the TokenBase service."""
    _lock = None  # type: threading.RLock
    """Lock object for protecting concurrent operations."""

    def __init__(self, token_table: VerifyingLog):
        """
        Constructor.

        :param token_table: Token lookup table (append only log).
        """
        self._lock = threading.RLock()
        self.token_table = token_table
        self.worker_id = generate_id(WORKER_ID_LENGTH)
        self.token_request_buffer = collections.deque()
        self.tx_in_flight = {}

    @synchronized
    def _is_request_in_flight(self, message_body: Dict) -> bool:
        """
        Checks whether a request for the given 'thing' is already in flight.

        :param message_body: Dictionary holding message body content.
        :return: `True` if it already is in flight, `False` otherwise. In
            cases of uncertainty (unknown operation), `None` is returned.
        """
        imsi = message_body.get('imsi')
        for in_flight in self.tx_in_flight.values():
            if in_flight['body']['request_for'] == 'imsi':
                imsi_serialised = serialisable_value(imsi)
                if (imsi and in_flight['body']['imsi'] == imsi_serialised):
                    # We've got a request for this IMSI.
                    return True
            elif in_flight['body']['request_for'] == 'records_after':
                if (tip_to_integer(in_flight['body']['requester_tip'])
                        <= tip_to_integer(message_body['requester_tip'])):
                    # We've got a request with this tip covered.
                    return True
            else:
                logger.warning("This shouldn't happen: A request for {}."
                               .format(in_flight['body']['request_for']))
                return None
        return False

    @synchronized
    def _request_async(self, message: ManagedMessage,
                       callback: Callable[[Dict], Union[bool, Dict]]=None,
                       cb_params=None):
        """
        Asynchronous request operation to a token base service.

        :param message: Message defining the token base service inquiry.
        :param callback: Callback function to execute upon receiving a response
            from the token base service (default: None).
        :param cb_params: Parameters for the callback call upon response
            (default: None).
        """
        # Proceed with request only if it is not in flight, yet.
        if self._is_request_in_flight(message.body) is False:
            tx_id = message.header['tx']
            self.tx_in_flight[tx_id] = {'body': message.body,
                                        'header': message.header,
                                        'ts': time.time()}
            if callback:
                self.tx_in_flight[tx_id]['callback'] = callback
                self.tx_in_flight[tx_id]['cb_params'] = cb_params
            self.request_impl_async(message)

    @synchronized
    def _request_sync(self, message: ManagedMessage) -> Union[bytes, Iterable]:
        """
        Synchronous request operation to a token base service. Records
        retrieved from the token base service are added to the local token
        table (due to the lack of a token updater).

        :param message: Message defining the token base service inquiry.
        :return: Token for the requested IMSI on IMSI requests, on
            'range after' requests an iterable of token table records.
        """
        return self.request_impl_sync(message)

    @synchronized
    def request(self, imsi: bytes=None, after: int=None, limit: int=None,
                callback: Callable[[Dict], Union[bool, Dict]]=None,
                cb_params=None,
                synchronous: bool=False) -> Union[Iterable[List], bytes, None]:
        """
        Requests for an IMSI token record(s) from the token base.

        All parameters by themselves are optional. But either `imsi` or
        `after` are required for operation.

        :param imsi: IMSI to request a token for (default: None).
        :param after: Request of records after a given index (default: None).
        :param limit: Maximum number of records to return (default: None). 0
            (zero) for all.
        :param callback: Callback function to execute upon receiving a response
            from the token base service (default: None).
        :param cb_params: Parameters for the callback call upon response
            (default: None).
        :param synchronous: Execute the operation in a synchronous (True) or
            asynchronous (False) fashion (default: False).
        :return: `None` in asynchronous mode, otherwise a token or an
            iterable of lists containing the tokenised records from the
            token base service.
        """
        self.counter += 1
        if imsi and after:
            logger.error('Cannot provide `imsi` and `after` parameters'
                         ' to TokenBase.request!')
            sys.exit(1)
        # Make the request message to the token base.
        message_body = None
        tx_id = generate_id(TX_ID_LENGTH).decode()
        if imsi:
            message_body = {'request_for': 'imsi',
                            'imsi': serialisable_value(imsi),
                            'requester_tip': self.token_table.tip}
        else:
            message_body = {'request_for': 'records_after',
                            'requester_tip': serialisable_value(after)}
            if limit:
                message_body['limit'] = limit
        message_header = {'from': self.worker_id.decode(),
                          'tx': tx_id}
        message = ManagedMessage(body=message_body,
                                 header=message_header,
                                 topic=config.token_base_topic)
        if synchronous:
            return self._request_sync(message)
        else:
            # Fire and forget, the callback takes care of it later.
            self._request_async(message, callback, cb_params)

    def request_impl_async(self, raw_message: bytes):
        """
        To request (send) an outgoing message asynchronously (not implemented).

        :param raw_message: Raw outgoing message.
        """
        raise NotImplementedError('This method needs to be overridden.')

    def request_impl_sync(self, raw_message: bytes):
        """
        To request (send) an outgoing message synchronously (not
        implemented).

        Note: This method should also already insert all retrieved records
              into the local token table.

        :param raw_message: Raw outgoing message.
        """
        raise NotImplementedError('This method needs to be overridden.')

    def update_table(self):
        """
        Request a (batch) of token table updates (up to `limit` records).
        """
        self.update_pending = True
        self.request(after=self.token_table.tip,
                     limit=config.records_per_request_limit)

    @synchronized
    def _process_head_item(self) -> bool:
        """
        Tries to process the head (first) item in the token base's request
        buffer.

        :return: None on empty token request buffer, True on success, False on
            failure.
        """
        if len(self.token_request_buffer) == 0:
            return None
        callback, data = self.token_request_buffer.popleft()
        success = callback(data)
        if not success:
            self.token_request_buffer.appendleft((callback, data))
        return success

    def catch_up_processing(self):
        """
        Try to replay buffered requests for IMSI tokenisation.
        """
        success = True
        while success:
            result = self._process_head_item()
            # Only `and` the result if it's a bool value. Not on None.
            success = result if result is None else success & result
        if success is False:
            logger.debug('Could not enter IMSI to local token table,'
                         ' need a table update (current tip {}).'
                         .format(self.token_table.tip))
            if not self.update_pending:
                self.update_table()

    @synchronized
    def cleanup_expired_transactions(self):
        """
        Weed through tx_in_flight list and purge past tips, too old
        transactions, etc.
        """
        current_tip = tip_to_integer(self.token_table.tip)
        for tx_id, transaction in list(self.tx_in_flight.items()):
            tx_tip = tip_to_integer(transaction['body']['requester_tip'])
            tx_type = transaction['body']['request_for']
            imsi = deserialisable_value(transaction['body'].get('imsi'))
            if tx_type == 'records_after' and tx_tip < current_tip:
                # We've covered this transaction already.
                del self.tx_in_flight[tx_id]
            elif tx_type == 'imsi' and self.token_table.has_token(imsi):
                # We've obtained a token for this IMSI in the mean time.
                del self.tx_in_flight[tx_id]
            elif (time.time() - transaction['ts']) > config.transaction_ttl:
                # Let's consider this transaction past it's use-by date.
                del self.tx_in_flight[tx_id]

    @synchronized
    def pop_transaction(self, tx_id: bytes) -> Dict:
        """
        Returns a transaction ID from the `tx_in_flight` register and removes
        it.

        :param tx_id: Transaction ID.
        :return: Dictionary containing information on the token base request
            in flight. Elements are `body`, `header` and `ts`; optionally also
            `callback` and `cb_params`.
        """
        return self.tx_in_flight.pop(tx_id, None)


class RecordsWorker(object):
    """
    Worker that will receive requests, and forward tokenised records via the
    RecordsSink if a token is available in the lookup table.
    """
    counter = 0
    """Internal counter for number of processed records."""
    token_table = None  # type: VerifyingLog
    """Local worker's token table."""
    records_sink = None  # type: RecordsSink
    """Reference to the outgoing pipeline message sink proxy object."""
    token_base = None  # type: TokenBase
    """Reference to the token base service proxy object."""
    _interval_cache_misses = 0
    """
    Local counter to track misses on token table misses per reporting interval.
    """
    _interval_start_time = None  # type: float
    """Time stamp of reporting interval start."""
    _interval_records_processed = 0
    """Number of IMSI records processed per reporting interval."""
    _interval_start_time = 0
    """Start time for the reporting interval."""
    _puller_thread = None  # type: threading.Thread
    """Thread running the puller."""
    synchronous = False
    """Operate the worker in a synchronous (True) fashion?"""

    def __init__(self,
                 token_table: VerifyingLog,
                 records_sink: RecordsSink,
                 token_base: TokenBase,
                 synchronous: bool=False):
        """
        Constructor.

        :param token_table: Token lookup table (append only log).
        :param records_sink: Record sender (outbound).
        :param token_base: Proxy object to token base service.
        :param synchronous: Operate the worker in a synchronous (True) fashion
            or asynchronous (False) fashion (default: False).
        """
        self.token_table = weakref.proxy(token_table)
        self.records_sink = weakref.proxy(records_sink)
        self.token_base = weakref.proxy(token_base)
        logger.info('Priming token table from token base service.')
        self.token_base.update_table()
        logger.info("Loading initial token entries, ready for rock'n'roll.")
        self._interval_start_time = time.time()
        self.synchronous = synchronous

    def run(self):
        """
        Starts running the subscriber in a thread.
        """
        self._puller_thread = threading.Thread(target=self._puller.run,
                                               args=())
        self._puller_thread.start()

    def join(self):
        """
        Joins the subscriber thread.
        """
        self._puller_thread.join()

    def _process_imsi(self, data: Dict) -> Union[bool, Dict]:
        """
        Processes (tokenises) the IMSI in a given data record.

        :param data: Dictionary of incoming (JSON) data, of which the field
            `imsi` is to be tokenised.
        :return: In asynchronous operation: True if the IMSI processing
            succeeded on the local token table, False otherwise. In synchronous
            operation the dictionary of the tokenised data record.
        """
        imsi = data[config.imsi_record_name]
        if self.token_table.has_token(imsi):
            token = base64.standard_b64encode(
                self.token_table.get_token(imsi)).decode().rstrip('=')
            data[config.imsi_record_name] = token
            self._interval_records_processed += 1
            # Do some reporting on the logs.
            time_elapsed = time.time() - self._interval_start_time
            if ((self._interval_records_processed
                    >= config.reporting_interval_records)
                    or (time_elapsed >= config.reporting_interval_time)):
                recs_per_sec = (self._interval_records_processed
                                / time_elapsed)
                cache_miss_rate = (self._interval_cache_misses
                                   / self._interval_records_processed)
                logger.info('Interval report: {} records processed,'
                            ' {:.3g} recs/s,'
                            ' {:2.2f} % cache misses ({}/{}).'
                            .format(self._interval_records_processed,
                                    recs_per_sec,
                                    100.0 * cache_miss_rate,
                                    self._interval_cache_misses,
                                    self._interval_records_processed))
                self._interval_cache_misses = 0
                self._interval_records_processed = 0
                self._interval_start_time = time.time()
            # Deliver the result.
            if self.synchronous:
                return data
            else:
                self.records_sink.produce(data)
                return True
        else:
            logger.debug('Unknown IMSI {}, requesting new token.'.format(imsi))
            self._interval_cache_misses += 1
            if self.synchronous:
                # Fetch data from token base.
                self.token_base.request(imsi=imsi, synchronous=True)
                # Call myself to return the now cached result.
                return self._process_imsi(data)
            else:
                self.token_base.request(imsi=imsi,
                                        callback=self._process_imsi,
                                        cb_params=data)
                # This operation on a deque is thread safe, no locking mum.
                self.token_base.token_request_buffer.append((self._process_imsi,
                                                             data))
                return False

    def process(self, message: Union[bytes, Dict]) -> Union[bytes, Dict]:
        """
        Processes an incoming message in plain JSON format. The IMSI in the
        message is a signed, 64-bit integer (in numeric form).

        :param message: Raw incoming message (as string or bytes), or
            structured message (as dictionary).
        :return: In asynchronous operation: True if the IMSI processing
            succeeded on the local token table, False otherwise. In synchronous
            operation the output message in same format as incoming message.
        """
        self.counter += 1
        logger.debug('Received {}: {}'.format(self.counter, message))
        # Convert incoming message to data dictionary.
        if isinstance(message, (str, bytes)):
            data = json.loads(message if isinstance(message, str)
                              else message.decode())
        elif isinstance(message, dict):
            data = message
        else:
            logger.warning('Unsupported message format: {}. Skip processing.'
                           .format(type(message)))
            return
        # Convert big-endian signed 64 bit integer to base64 encoded bytes.
        imsi = data[config.imsi_record_name]
        imsi_binary = imsi.to_bytes(8, byteorder='big', signed=True)
        data[config.imsi_record_name] = imsi_binary
        # Processing through the internal implementation.
        result = self._process_imsi(data)
        # Return result in appropriate format.
        if self.synchronous:
            if isinstance(message, str):
                result = json.dumps(result)
            elif isinstance(message, bytes):
                result = json.dumps(result).encode()
        return result


class TokenUpdater(object):
    """
    Listener on data update broadcasts from the token base service.
    """
    counter = 0
    """Internal counter for number updates received."""
    token_table = None  # type: VerifyingLog
    """Local worker's token table."""
    token_base = None  # type: TokenBase
    """Reference to the token base service proxy object."""
    _subscriber_thread = None  # type: threading.Thread
    """Thread running the ZeroMQ subscriber."""

    def __init__(self,
                 token_table: VerifyingLog,
                 token_base: TokenBase):
        """
        Constructor.

        :param token_table: Token lookup table (append only log).
        :param token_base: Proxy object to token base service.
        """
        self.token_table = weakref.proxy(token_table)
        self.token_base = weakref.proxy(token_base)

    def run(self):
        """
        Starts running the subscriber in a thread.
        """
        self._subscriber_thread = threading.Thread(target=self._subscriber.run,
                                                   args=())
        self._subscriber_thread.start()

    def join(self):
        """
        Joins the subscriber thread.
        """
        self._subscriber_thread.join()

    def process(self, raw_message: bytes):
        """
        Processes a message from the token base service on IMSI record(s).

        :param raw_message: Raw message coming in from the token base service.
        """
        self.counter += 1
        message = ManagedMessage(raw=raw_message)
        # See if we've got a response for a transaction to us.
        tx_id = message.header.get('tx')
        transaction = self.token_base.pop_transaction(tx_id)
        # Set up accounting for # of processed records.
        old_tip = tip_to_integer(self.token_table.tip)
        # There should be something I need to update.
        records = (x.decode() for x in message.body.split(b'\n') if x)
        try:
            # Insert the (many) records.
            self.token_table.insert_many(deserialise_record(x)
                                         for x in records)
            # Check if we need to fetch more.
            if message.header.get('more_records', 0) > 0:
                self.token_base.update_table()
            else:
                self.token_base.update_pending = False
            # Execute possible callback.
            if transaction and 'callback' in transaction:
                transaction['callback'](transaction['cb_params'])
        except ValueError:
            if not transaction:
                logger.info('Could not insert received record(s) into local'
                            ' token table, requesting update range from'
                            ' current tip {}.'
                            .format(self.token_table.tip))
                # Request from where we are currently.
                self.token_base.update_table()
            else:
                logger.warning('This should not happen. Got a response to a'
                               ' pending transaction I cannot service:'
                               ' own tip={}, transaction={}'
                               .format(self.token_table.tip, str(transaction)))

        # Set new_tip for accounting of # of processed records.
        num_added = tip_to_integer(self.token_table.tip) - old_tip
        if num_added > 1:
            logger.info('{} records added.'.format(num_added))
        if num_added:
            self.token_base.cleanup_expired_transactions()
            self.token_base.catch_up_processing()
