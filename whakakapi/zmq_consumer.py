# -*- coding: utf-8 -*-
"""
Module consuming tokenised records.
"""

# Created: 2017-05-01 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import logging

import whakakapi.zmq_communicator as zmq_comms


REPORTING_INTERVAL = 5000

logger = logging.getLogger(__name__)


class GetRecords(object):
    counter = 0
    """Internal counter for number of produced records."""
    _source = None  # type: zmq_comms.Puller
    """ZeroMQ source for incoming pipeline."""

    def __init__(self, end_point: str, bind: bool=False):
        """
        Constructor.

        :param end_point: Address of end point to connect to.
        :param bind: True for binding, otherwise it connects the socket
            (default: False).
        :param callback: Callback function to process a received message.
        """
        self._source = zmq_comms.Puller(end_point, bind=bind,
                                        callback=self.process)
        self._source.run()

    def process(self, raw_message):
        """
        Receive in incoming message from the tokeniser pipeline.

        :param raw_message: Incoming raw message.
        """
        self.counter += 1
        # FIXME: remove the following hard coded interval.
        if self.counter % REPORTING_INTERVAL == 0:
            logger.info('Received {}: {}'.format(self.counter, raw_message))
        else:
            logger.debug('Received {}: {}'.format(self.counter, raw_message))
