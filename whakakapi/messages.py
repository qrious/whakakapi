# -*- coding: utf-8 -*-
"""
Module providing base message structures for workers, base logs and
source/sink components in the tokenisation chain.
"""

# Created: 2017-04-19 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# (c) 2017 by Qrious Limited, Auckland, New Zealand
#     http://qrious.co.nz/
#     All rights reserved.

__author__ = 'Guy K. Kloss <guy.kloss@qrious.co.nz>'

import base64
import json
from typing import Dict, Union

from whakakapi.config import config


def standard_b64decode(value: Union[str, bytes]) -> bytes:
    """
    Applies base64 padding (where stripped) and decodes the value into a byte
    sequence.

    :param value: String or bytes value to be decoded.
    :return: Raw decoded byte sequence.
    """
    if isinstance(value, str):
        value = value.encode()
    delta = len(value) % 3
    delta = 3 - delta if delta else 0
    value += b'=' * delta
    return base64.standard_b64decode(value)


class ManagedMessage(object):
    """
    A wrapper for creating "managed messages". A managed message contains in
    the first line the format:

       topic: header
       body

    The `header` is a JSON object following the `topic` on the same line, and
    it is JSON parsed. The `body` is following a new line, and is accessible
    as a byte string in raw (so that the code can make sense of it later on).

    A managed message *must* contain a header line, and everything following is
    the body. Only the topic is optional.

    For the context of the tokeniser: The `topic` is b'update' (configurable)
    for update broadcasts, and the worker ID for (directed) responses to a
    worker. If the topic is intended to be left empty, it needs to be set to
    an empty string (or byte string) value.
    """
    body = None  # type: bytes
    header = None  # type: Dict
    topic = None  # type: bytes

    def __init__(self, body: bytes=None, header: Dict=None,
                 topic: bytes=None, raw: bytes=None):
        """
        A message object can be constructed in two ways: Either by its
        (received) binary body that is parsed, or via providing the
        individual body, header and topic components.

        :param body: Message body.
        :param header: Message header (to be translated to JSON).
        :param topic: (Optional) topic for pub/sub messages.
        :param raw: Binary message body to be parsed.
        """
        if raw:
            self._split_message(raw)
        else:
            self.body = body if body else b''
            self.header = header if header else {}
            self.topic = topic if topic is not None else config.update_topic

    def _split_message(self, raw: bytes):
        """
        Parses a binary message body and splits it into its components:
        topic, header and body.

        :param raw: Byte string of message body.
        """
        header_sep_index = raw.index(b'\n')
        self.body = raw[header_sep_index + 1:]
        header_raw = raw[:header_sep_index]
        if b':' in header_raw:
            index = header_raw.index(b':')
            self.topic = header_raw[:index]
            self.header = json.loads(header_raw[index + 2:].decode())
        else:
            self.topic = None
            self.header = header_raw

    @property
    def message(self) -> bytes:
        """
        Creates and returns a message readily prepared for the transfer.

        :return: Binary (wire) representation of the message.
        """
        message = b''
        if self.topic:
            message += self.topic + b': '
        if isinstance(self.body, str):
            body = self.body.encode()
        elif isinstance(self.body, dict):
            body = json.dumps(self.body).encode()
        elif isinstance(self.body, bytes):
            body = self.body
        else:
            raise ValueError('Cannot handle message body of type {}'
                             .format(type(self.body)))
        message += json.dumps(self.header).encode() + b'\n' + body
        return message

    def __str__(self):
        return self.message.decode()

    def __repr__(self):
        return '{}(body={}, header={}, topic={})'.format(
            self.__class__.__name__, repr(self.body)[:200],
            repr(self.header), repr(self.topic))
