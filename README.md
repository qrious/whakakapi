Whakakapi Tokeniser for Data Anonymisation
==========================================

A simple Python implementation for a randomised data anonymiser.


## Deployment

### Prerequisites

- Python >= 3.4
- `pyNaCl` - Python bindings to `libsodium`.
- `appdirs` - To determine appropriate platform-specific directories.
- `pyzmq` - To enable ZeroMQ event driven communication.
- `pyaml` - To enable YAML file-based configuration.


### Install

If a copy exists (e.g. via `git clone`) in a directory, do the following:

    pip install whakakapi/

Or to upgrade:

    pip install --upgrade whakakapi/

If this should fail to produce the installed library usable for all
users. If executed by `root` it is possible that the `umask` is set
too restrictively (e.g. as on CentOS with a restrictive setup like
SELinux), just set the `umask` first:

    umask 0022
    pip install whakakapi/


### Setting up a Virtual Environment

Set up and activate for Python 3:

    virtualenv env3 --system-site-packages --python=/usr/bin/python3
    source env3/bin/activate

Install required packages:

    pip install -e .

Install packages required for a particular token base and working
style:

    pip install -e .[wsworker,wsbase]

Working styles are as follows:

- `ws` -- Synchronous operation using a Flask RESTful Web Service as a
  token base.
- `zmq` -- Asynchronous pipeline operation using ZeroMQ messages for
  all communication.
- `postgresql` -- Synchronous operation using a PostgreSQL DB as a
  token base. The PostgreSQL DB *does not* support concurrent or
  multi-threaded operation.

For installing the additional development, testing or documentation
dependencies, add a qualifier with one or more of these commands:

    pip install -e .[dev]           # Development dependencies
    pip install -e .[test]          # Testing dependencies
    pip install -e .[doc]           # Documentation dependencies
    pip install -e .[dev,test,doc]  # All dependencies together


## Run the Code

To run the code, it needs to be configured, and individual components run.


### Configure

The default configuration is located in `whakakapi.config._Settings`. Every
directive can be overridden via a YAML configuration file within the executing
user's configuration directory. Under Linux, this is commonly
`$HOME/.config/whakakapi.conf`.

Such a file could for example look like this (reconfiguring to a UNIX socket
end point on the worker-service communication, and changing the reporting
interval):

    # service_queue: tcp://127.0.0.1:6002
    service_queue: ipc:///tmp/whakakapi_service
    # update_queue: tcp://127.0.0.1:6003
    update_queue: ipc:///tmp/whakakapi_updates
    
    reporting_interval: 10000


### Execution of the Components

The tokeniser contains of a number of components to execute in the
`scripts` directory. Please refer to
`docs/source/quick_start_deploy.rst` for further information on these.


## Project Name

*Whakakapi* is the Māori word for "make full" or "occupy", but also in the
sense of the verb/noun "substitute" or "replace"/"replacement".

whakakapi:

1. (verb) (-a) to fill up (a space), occupy, replace, stand as a substitute.
2. (verb) (-a, -ngia) to conclude, complete, close, finish (speeches,
   writing, lectures) - especially in the sense of tying together the
   ideas that have been said or written earlier or by other speakers.
3. (noun) cover, substitute, successor, replacement.


## Licence

Copyright 2017-2018 by Qrious Limited, Auckland, New Zealand

This work is licensed under the Apache 2.0 open source licence.
Terms and conditions apply.
